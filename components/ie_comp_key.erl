-module(ie_comp_key).

-include("interactive_erlang.hrl").
-include("idiagnostic.hrl").
-include("idraw.hrl").
-include("imoderator.hrl").

-behaviour(ie_component).

%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, stop/1, event/2]).

-record(key_event, {key_code,
                    type}).

component_info() -> #component_info{iworld = #type_info{init_info = [ie_comp_moderator, ie_comp_draw]},
                                    iuniverse = #type_info{init_info = undefined}}.

describe(Options) ->
  case ?OptionGetValue(key, Options) of
    undefined -> undefined;
    Callback  -> {"key", Callback}
  end.

init(#iState{options = Options} = IState) ->
  init(IState, ?OptionGetValue(key, Options)).

init(_IState, undefined) -> undefined;
init(#iState{extra_data = #{window := Window,
                            canvases := Canvases}}, CallbackFun) ->
  Send = fun(Key, Type) -> ?SendCallbackEvent({{Key, Type},
                                               #callback_event{event_object = #key_event{key_code = Key,
                                                                                         type = Type},
                                                             callback = CallbackFun}}) end,
  CallBack1 = fun(#wx{event = EventRecord}, _EventObject) ->
                      Converted = convert_key(EventRecord),
                      case Converted of
                        f10 -> ie_settings:add_setting(flush); %% remove, convert functionality to button in command console
                        f11 -> ?Send(ie_comp_tick, switch);
                        f12 -> ?SendToDiagnostic(switchDraw),
                               [?Send(Canvas#canvas.pid, switch) || Canvas <- Canvases];
                        _   -> Send(Converted, key_down)
                      end
              end,
  CallBack2 = fun(#wx{event = EventRecord}, _EventObject) -> Send(convert_key(EventRecord), key_up) end,
  wxEvtHandler:connect(Window, key_down, [{callback, CallBack1}]),
  wxEvtHandler:connect(Window, key_up, [{callback, CallBack2}]),
  ok.

stop(IState) -> IState.

event(#iState{state = State} = IState,
      #callback_event{event_object = #key_event{key_code = Key,
                                                type     = Type},
                      callback = Callback}) ->
  IState#iState{state = ?Dispatch(Callback(State, Key, Type))}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%Translates the keycodes to easier-to-understand atoms. (Code, shift-depressed)
convert_key(#wxKey{keyCode = Key, shiftDown = Shift}) ->
  convert_key(Key, Shift).

convert_key(8,  _) -> backspace;
convert_key(9,  _) -> tab;
convert_key(13, _) -> enter;
convert_key(27, _) -> escape;
convert_key(32, _) -> " ";

%% Letters
convert_key(N, true)  when 65 =< N andalso N =< 90 -> [N];      %% Uppercase
convert_key(N, false) when 65 =< N andalso N =< 90 -> [N + 32]; %% Lowercase

%% Numbers
convert_key(N, false) when 48 =< N andalso N =< 57 -> [N];

%Number bar
convert_key(96,  false) -> "`";
convert_key(45,  false) -> "-";
convert_key(61,  false) -> "=";

convert_key(96,  true)  -> "~";
convert_key(49,  true)  -> "!";
convert_key(50,  true)  -> "@";
convert_key(51,  true)  -> "#";
convert_key(52,  true)  -> "$";
convert_key(53,  true)  -> "%";
convert_key(54,  true)  -> "^";
convert_key(55,  true)  -> "&";
convert_key(56,  true)  -> "*";
convert_key(57,  true)  -> "(";
convert_key(48,  true)  -> ")";
convert_key(45,  true)  -> "_";
convert_key(61,  true)  -> "+";

%% First row symbols
convert_key(N, false) when 91 =< N andalso N =< 93 -> [N];
convert_key(N, true)  when 91 =< N andalso N =< 93 -> [N + 32];

%Second row symbols
convert_key(59,  false) -> ";";
convert_key(39,  false) -> "'";
convert_key(59,  true)  -> ":";
convert_key(39,  true)  -> "\"";

%Third row symbols
convert_key(44,  false) -> ",";
convert_key(46,  false) -> ".";
convert_key(47,  false) -> "/";

convert_key(44,  true)  -> "<";
convert_key(46,  true)  -> ">";
convert_key(47,  true)  -> "?";
%Command keys
convert_key(127,  _) -> delete;
convert_key(306,  _) -> shift;
convert_key(307,  _) -> lcontrol;
convert_key(308,  _) -> lalt;
convert_key(312,  _) -> 'end';
convert_key(313,  _) -> home;
convert_key(314,  _) -> left;
convert_key(315,  _) -> up;
convert_key(316,  _) -> right;
convert_key(317,  _) -> down;
convert_key(322,  _) -> insert;
convert_key(325,  _) -> num1;
convert_key(326,  _) -> num2;
convert_key(327,  _) -> num3;
convert_key(328,  _) -> num4;
convert_key(329,  _) -> num5;
convert_key(330,  _) -> num6;
convert_key(331,  _) -> num7;
convert_key(332,  _) -> num8;
convert_key(333,  _) -> num9;
convert_key(340,  _) -> f1;
convert_key(341,  _) -> f2;
convert_key(342,  _) -> f3;
convert_key(343,  _) -> f4;
convert_key(344,  _) -> f5;
convert_key(345,  _) -> f6;
convert_key(346,  _) -> f7;
convert_key(347,  _) -> f8;
convert_key(348,  _) -> f9;
convert_key(349,  _) -> f10;
convert_key(350,  _) -> f11;
convert_key(351,  _) -> f12;
convert_key(366,  _) -> page_up;
convert_key(367,  _) -> page_down;
convert_key(384,  _) -> num0;
convert_key(_,    _) -> undefined.
