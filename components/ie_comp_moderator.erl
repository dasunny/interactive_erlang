-module(ie_comp_moderator).

-include("interactive_erlang.hrl").

-behaviour(ie_component).

%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, stop/1, event/2]).
-export([start_process/1]).

-record(state, {keys = [],
                queue = [],
                rate,
                timer_ref,
                stay_alive = true}).

component_info() -> #component_info{iworld = #type_info{init_info = [ie_comp_diagnostic]},
                                    iuniverse = #type_info{init_info = []}}.

describe(_Options) -> undefined.

init(#iState{options = Options} = _IState) ->
  Rate = ?OptionGetValue3(clock_rate, Options, 100),
  spawn(?MODULE, start_process, [#state{rate = trunc(1000/Rate)}]),
  ok.

stop(IState) ->
  ?Unregister(?MODULE),
  IState.

event(IState, _CallbackEvent) -> IState.

%% ====================================================================
%% Internal functions
%% ====================================================================

start_process(#state{rate = Rate} = State) ->
  register(?MODULE, self()),
  {ok, TRef} = timer:send_interval(Rate, self(), reset),
  loop(State#state{timer_ref = TRef}).

loop(#state{keys = Keys, queue = Queue, stay_alive = true} = State) ->
  Reset = fun() -> lists:foreach(fun(Elem) -> ?SendCallbackEvent(Elem) end, lists:reverse(Queue)),
                                   {[], []} end,
  {NewKeys, NewQueue} = receive
                          reset -> Reset();
                          Event -> add_to_map(Event, Keys, Queue)
                        after
                            100 -> Reset()
                        end,
  loop(State#state{keys = NewKeys, queue = NewQueue, stay_alive = ?IsParentAlive});

loop(#state{timer_ref = TRef, stay_alive = false}) ->
  timer:cancel(TRef),
  ok.

add_to_map(#callback_event{component = Key} = CallbackEvent, Keys, Queue) ->
  add_to_map(Key, CallbackEvent, Keys, Queue);
add_to_map({Key, CallbackEvent}, Keys, Queue) ->
  add_to_map(Key, CallbackEvent, Keys, Queue).

add_to_map(Key, CallbackEvent, Keys, Queue) ->
  case lists:member(Key, Keys) of
    true  -> {Keys, Queue};
    false -> {[Key | Keys], [CallbackEvent | Queue]}
  end.
