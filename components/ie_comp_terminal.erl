-module(ie_comp_terminal).

-include("interactive_erlang.hrl").

-include_lib("wx/include/wx.hrl").

-behaviour(ie_component).

%% ====================================================================
%% API functions
%% ====================================================================
-compile({no_auto_import,[put/2]}).
-export([component_info/0, describe/1, init/1, stop/1, event/2]).
-export([start_process/1]).

-record(state, {stay_alive = true,
                callback,
                wx_env,
                frame,
                textbox,
                input}).

component_info() -> #component_info{iworld = #type_info{init_info = undefined},
                                    iuniverse = #type_info{init_info = [ie_comp_wx_init]}}.%%TODO add support for world

describe(Options) ->
  case ?OptionGetValue(terminal, Options) of
    undefined -> undefined;
    Callback  -> {"terminal", Callback}
  end.

init(#iState{options = Options,
             extra_data = #{wx := Wx, wx_env := Env} = ExtraData,
             type = Type} = IState) ->
  Callback = ?OptionGetValue(terminal, Options),
  Title = "Interactive Erlang Terminal - " ++ ie_utils_common:descriptor(Type),
  Frame = wxFrame:new(Wx, 2, Title, [{size, {800, 600}}]),
  CB = fun(_,_) -> ?SendClose("Terminal closed") end,
  wxEvtHandler:connect(Frame, close_window, [{callback, CB}]),

  spawn(?MODULE, start_process, [#state{callback = Callback, wx_env = Env, frame = Frame}]),

  {ok, IState#iState{extra_data = ExtraData#{terminal_window => Frame}}}.

stop(IState) ->
  ?Send(?MODULE, stop),
  ?Unregister(?MODULE),
  IState.

event(IState, #callback_event{callback = undefined}) ->
  put("No Callback defined!"),
  IState;
event(#iState{state = State} = IState, #callback_event{event_object = EventObject, callback = Callback}) ->
  IState#iState{state = ?Dispatch(Callback(State, EventObject))}.

%% ====================================================================
%% Internal functions
%% ====================================================================

start_process(#state{frame = Frame, wx_env = WX} = State) ->
  register(?MODULE, self()),
  wx:set_env(WX),
  wxFrame:setClientSize(Frame, 800, 600),
  Panel = wxPanel:new(Frame, []),
  MainSizer   = wxBoxSizer:new(?wxVERTICAL),
  TextSizer   = wxBoxSizer:new(?wxHORIZONTAL),
  InputSizer  = wxBoxSizer:new(?wxHORIZONTAL),
  TextBox = wxTextCtrl:new(Panel, ?wxID_ANY, [{style, ?wxDEFAULT
                                                 bor ?wxTE_RICH
                                                 bor ?wxTE_MULTILINE
                                                 bor ?wxHSCROLL
                                                 bor ?wxVSCROLL
                                                 bor ?wxTE_READONLY}]),
  Input = wxTextCtrl:new(Panel, ?wxID_ANY, [{style, ?wxTE_PROCESS_ENTER bor ?wxTE_RICH}]),
  Stop    = wxButton:new(Panel, ?wxID_ANY, [{label, "Stop"}]),

  wxPanel:setSizer(Panel, MainSizer, []),

  wxSizer:add(InputSizer, Input,      [{flag, ?wxEXPAND}, {proportion, 1}]),
  wxSizer:add(InputSizer, Stop),
  wxSizer:add(TextSizer,  TextBox,    [{flag, ?wxEXPAND}, {proportion, 1}]),
  wxSizer:add(MainSizer,  TextSizer,  [{flag, ?wxEXPAND}, {proportion, 1}]),
  wxSizer:add(MainSizer,  InputSizer, [{flag, ?wxEXPAND}]),

  Attr = wxTextAttr:new(),
  Font = wxFont:new(),
  wxFont:setFamily(Font, ?wxFONTFAMILY_MODERN),
  wxTextAttr:setFont(Attr, Font),
  wxTextCtrl:setDefaultStyle(TextBox, Attr),
  wxTextCtrl:setDefaultStyle(Input, Attr),

  CB1 = fun(_,_) -> ?SendClose("Server Stopped") end,
  CB2 = fun(_,_) -> Text = wxTextCtrl:getValue(Input),
                    ?Send(?MODULE, {stdin, Text}),
                    wxTextCtrl:setValue(Input, "") end,
  CB3 = fun(_,_) -> wxWindow:setFocus(Input) end,

  wxButton:connect(Stop,    command_button_clicked, [{callback, CB1}]),
  wxTextCtrl:connect(Input, command_text_enter,     [{callback, CB2}]),
  wxTextCtrl:connect(TextBox, child_focus, [{callback, CB3}]),

  wxFrame:show(Frame),
  put("Starting iUniverse"),
  wxWindow:setFocus(Input),
  loop(State#state{textbox = TextBox, input = Input}).

loop(#state{stay_alive = true, callback = Callback, textbox = TextBox, input = Input} = State) ->
  Put = fun(Text) -> put({TextBox, Input}, Text) end,
  StayAlive = receive
                {stdin, In} -> Put("> " ++ In),
                               ?SendCallbackEvent(#callback_event{callback = Callback,
                                                                  event_object = In}),
                               true;
                {put,   In} -> Put(In),
                               true;
                stop        -> false
              end,
  loop(State#state{stay_alive = StayAlive andalso ?IsParentAlive});

loop(#state{stay_alive = false, frame = Frame}) ->
  wxFrame:hide(Frame),
  wxFrame:destroy(Frame),
  ok.

put(Text) ->
  ?Send(?MODULE, {put, Text}).

put(Format, Args) when is_list(Format), is_list(Args) ->
  ?Send(?MODULE, {put, io_lib:format(Format, Args)});
put({TextBox, Input}, Text) ->
  wxWindow:setFocus(TextBox),
  wxTextCtrl:appendText(TextBox, Text ++ "\n"),
  wxWindow:setFocus(Input).
