-module(ie_comp_resize).
 
-include("interactive_erlang.hrl").
-include("idraw.hrl").
-include("imoderator.hrl").
 
-behaviour(ie_component).
 
%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, stop/1, event/2]).
 
component_info() -> #component_info{iworld = #type_info{init_info = [ie_comp_moderator, ie_comp_draw]},
                                    iuniverse = #type_info{init_info = undefined}}.
 
describe(Options) ->
  Resize = case ?OptionGetValue(resize, Options) of
             undefined -> undefined;
             Callback1 -> {"resize", Callback1}
           end,
  Pane = case ?OptionGetValue(resize_pane, Options) of
           undefined -> undefined;
           Callback2 -> {"resize_pane", Callback2}
         end,
  [Resize, Pane].
 
init(#iState{options = Options} = IState) ->
  init(IState, ?OptionGetValue(resize, Options)).
 
init(#iState{extra_data = #{window := Window}}, undefined) ->
  Size = wxFrame:getSize(Window),
  wxFrame:setMinSize(Window, Size),
  wxFrame:setMaxSize(Window, Size),
  ok;
 
init(#iState{extra_data = #{window := Parent}} = IState, CallbackFun) ->
  CallBack = fun(_EventRecord, _EventObject) ->
                     ?SendCallbackEvent({resize, #callback_event{event_object = {size, undefined},
                                                                 callback = CallbackFun}}) end,
  wxEvtHandler:connect(Parent, size, [{callback, CallBack}]),
  {ok, IState}.
 
stop(IState) -> IState.
 
event(#iState{extra_data = #{window := Window} = ExtraData,
              state = State} = IState,
      #callback_event{event_object = {size, _},
                      callback = Callback}) ->
  Size = wxWindow:getClientSize(Window),
  NewState = Callback(State, Size),
  UpdatedCanvases = update_canvases(IState),
  IState#iState{state = ?Dispatch(NewState),
                extra_data = ExtraData#{canvases => UpdatedCanvases}}.
 
%% ====================================================================
%% Internal functions
%% ====================================================================
 
update_canvases(#iState{options = Options} = IState) ->
  update_canvases(IState, ?OptionGetValue(resize_pane, Options)).
 
update_canvases(#iState{extra_data = #{canvases := [Canvas],
                                       window   := Window}}, _Callback) ->
  #canvas{panel = Panel, pid = Pid, simple_canvas = Simple} = Canvas,
  {Width, Height} = wxWindow:getClientSize(Window),
  wxPanel:setSize(Panel, Width, Height),
  ?Send(Pid, {resize, Width, Height}),
  [Canvas#canvas{width = Width,
                 height = Height,
                 simple_canvas = Simple#simple_canvas{width = Width,
                                                      height = Height}}];
update_canvases(#iState{extra_data = #{canvases := Canvases}}, undefined) ->
  Canvases;
update_canvases(#iState{state = State,
                        extra_data = #{canvases := Canvases,
                                       window   := Window}}, Callback) ->
  NewSize = wxWindow:getClientSize(Window),
  Fun = fun(#canvas{name = Name, panel = Panel, pid = Pid, simple_canvas = Simple} = Canvas) ->
                {Width, Height, X, Y} = Callback(State, NewSize, Name),
                wxPanel:setSize(Panel, {X, Y, Width, Height}),
                ?Send(Pid, {resize, Width, Height}),
                Canvas#canvas{width = Width, height = Height,
                 simple_canvas = Simple#simple_canvas{width = Width,
                                                      height = Height}} end,
  [Fun(Canvas) || Canvas <- Canvases].