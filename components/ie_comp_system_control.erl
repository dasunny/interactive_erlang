-module(ie_comp_system_control).

-include("interactive_erlang.hrl").

-behaviour(ie_component).

%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, stop/1, event/2]).
%% -export([start_process/1]).
%% -export([do_first/1, set_settings/0, get_setting/1, add_setting/1]).

%% -record(state, {stay_alive = true}).
-define(DoFirst, ie_do_first).

component_info() -> #component_info{iworld = #type_info{init_info = ?INIT_LAST,
                                                        exec_info = ?EXEC_FIRST},
                                    iuniverse = #type_info{init_info = ?INIT_LAST,
                                                           exec_info = ?EXEC_FIRST}}. %%TODO implement for universe

describe(_Options) -> undefined.

%% init(#iState{extra_data = ExtraData} = IState) ->
%%   Tid = ets:new(?MODULE, [named_table, public]),
%%   {ok, IState#iState{extra_data = ExtraData#{system_ets => Tid}}}. %% TODO add table tid to extra_data
init(_IState) ->
  ok.

%% stop(#iState{extra_data = #{system_ets := Tid}} = IState) ->
%%   ets:delete(Tid),
stop(IState) ->
  ?Unregister(?MODULE),
  IState.

event(IState, _CallbackEvent) ->
  lists:foreach(fun do_first/1, get_do_firsts([])),
  
  
  IState.

do_first(flush) ->
  ie_utils_common:flush();
do_first(_) ->
  ok.

%% set_settings() ->
%%   ets:insert(?MODULE, {draw, true}),
%%   ets:insert(?MODULE, {doFirst, []}).
%% 
%% get_setting(doFirst) ->
%%   {_, List} = hd(ets:lookup(?MODULE, doFirst)),
%%   ets:insert(?MODULE, {doFirst, []}),
%%   List;
%% get_setting(Key) ->
%%   {_, Value} = hd(ets:lookup(?MODULE, Key)),
%%   Value.
%% 
%% add_setting(New) ->
%%   [{_, List} | _] = ets:lookup(?MODULE, doFirst),
%%   ets:insert(?MODULE, {doFirst, List++[New]}).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% start_process(State) ->
%%   register(?MODULE, self()),
%%   loop(State).
%% 
%% loop(State) ->
%%   loop(State#state{stay_alive = ?IsParentAlive}).

get_do_firsts(Acc) ->
  receive
    {?DoFirst, Any} -> get_do_firsts([Any | Acc])
  after 0 -> lists:reverse(Acc)
  end.
