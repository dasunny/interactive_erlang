-module(ie_comp_wx_init).

-include("interactive_erlang.hrl").

-behaviour(ie_component).

%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, stop/1, event/2]).

component_info() -> #component_info{iworld = #type_info{init_info = ?INIT_FIRST},
                                    iuniverse = #type_info{init_info = ?INIT_FIRST}}.

describe(_Options) ->
  undefined.

init(#iState{extra_data = ExtraData} = IState) ->
  Wx  = wx:new(),
  Env = wx:get_env(),
  {ok, IState#iState{extra_data = ExtraData#{wx     => Wx,
                                             wx_env => Env}}}.

stop(IState) ->
  wx:destroy(),
  IState.

event(IState, _Event) ->
  IState.

%% ====================================================================
%% Internal functions
%% ====================================================================
