-module(ie_comp_call_once).

-include("interactive_erlang.hrl").

-behaviour(ie_component).

%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, stop/1, event/2]).

component_info() -> #component_info{iworld = #type_info{init_info = ?INIT_LAST},
                                    iuniverse = #type_info{init_info = ?INIT_LAST}}.

describe(Options) ->
  case ?OptionGetValue(call_once, Options) of
    undefined -> undefined;
    Callback  -> {"call_once", Callback}
  end.

init(#iState{options = Options}) ->
  case ?OptionGetValue(call_once, Options) of
    undefined -> undefined;
    Callback  -> Callback(),
                 ok
  end.

stop(IState) -> IState.

event(IState, _CallbackEvent) ->
  IState.

%% ====================================================================
%% Internal functions
%% ====================================================================
