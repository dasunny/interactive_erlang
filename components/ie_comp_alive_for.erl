-module(ie_comp_alive_for).

-include("interactive_erlang.hrl").

-behaviour(ie_component).

%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, stop/1, event/2]).
-export([start_process/1]).

-record(state, {time}).

component_info() -> #component_info{iworld = #type_info{init_info = ?INIT_LAST},
                                    iuniverse = #type_info{init_info = ?INIT_LAST}}.

describe(Options) ->
  case ?OptionGetValue(alive_for, Options) of
    undefined -> ok;
    Time      -> {"alive_for", "This world will automatically shut itself down after ~p seconds.", [Time]}
  end.

init(#iState{options = Options}) ->
  case ?OptionGetValue(alive_for, Options) of
    undefined -> undefined;
    Time      -> spawn(?MODULE, start_process, [#state{time = Time}]),
                 ok
  end.

stop(IState) ->
  ?Send(?MODULE, stop),
  ?Unregister(?MODULE),
  IState.

event(IState, _CallbackEvent) ->
  IState.

%% ====================================================================
%% Internal functions
%% ====================================================================

start_process(#state{time = Time}) ->
  register(?MODULE, self()),
  receive
    stop -> ok
  after
      Time*1000 -> io:format("Gracefully closing processes\n"),
                   ?SendClose("Alive_for timer expired"),
                   timer:sleep(2000),
                   case ?IsParentAlive of
                     true -> io:format("Forcefully Killing processes\n", []),
                             exit(?PARENT_PROCESS, 'Force Closing');
                     false -> ok
                   end
  end.
