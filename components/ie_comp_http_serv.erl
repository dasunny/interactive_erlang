-module(ie_comp_http_serv).

-include("interactive_erlang.hrl").
-include("idiagnostic.hrl").
-include("ihttp.hrl").

-behaviour(ie_component).

%% ====================================================================
%% API functions
%% ====================================================================
-export([init/1, stop/1, describe/1, event/2, component_info/0]).
-export([start_process/1]).

-record(state, {socket,
                callback,
                type,
                stay_alive = true}).

-define(TIMEOUT, ie_utils_net:timeout()).
-define(LISTEN_TYPE, listen).
-define(CLIENT_TYPE, client).

component_info() -> #component_info{iworld = #type_info{init_info = undefined}}.

describe(Options) ->
  Connect = case ?OptionGetValue(http_connect, Options) of
              undefined -> undefined;
              Callback1 -> {"http_connect", Callback1}
            end,
  Disconnect = case ?OptionGetValue(http_disconnect, Options) of
                 undefined -> undefined;
                 Callback2 -> {"http_disconnect", Callback2}
               end,
  Receive = case ?OptionGetValue(http_receive, Options) of
              undefined -> undefined;
              Callback3 -> {"http_receive", Callback3}
            end,
  [Connect, Disconnect, Receive].

init(#iState{options = Options}) ->
  spawn(?MODULE, start_process, [#state{callback = {?OptionGetValue(http_connect,    Options),
                                                    ?OptionGetValue(http_disconnect, Options),
                                                    ?OptionGetValue(http_receive,    Options)},
                                        type = ?LISTEN_TYPE}]),
  ok.

stop(IState) ->
  ?Send(?MODULE, stop),
  ?Unregister(?MODULE),
  IState.

event(#iState{state = State} = IState,
      #callback_event{callback = Callback,
                      event_object = EventObject}) ->
  {Method, _, Pid} = EventObject,
  IsGet = Method == get,
  Dispatch = case EventObject of
               {get,  Path, Pid}  -> Callback(State, get, Path, <<>>);
               {post, Path, Data} -> Callback(State, post, Path, Data)
             end,
  NewState = case ?Dispatch(Dispatch) of
               {http_response, State2, Send, Type} when IsGet -> ?Send(Pid, {response, Send, Type}),
                                                                 State2;
               State2 -> State2
             end,
  IState#iState{state = NewState}.

%% ====================================================================
%% Internal functions
%% ====================================================================

start_process(#state{type = ?LISTEN_TYPE} = State) ->
  register(?MODULE, self()),
  {ok, Socket} = gen_tcp:listen(80, [binary]),
  loop(State#state{socket = Socket});

start_process(#state{type = ?CLIENT_TYPE} = State) ->
  loop(State).

loop(#state{socket = Socket, callback = Callback, type = ?LISTEN_TYPE, stay_alive = true} = State) ->
  StayAlive = receive stop -> false after 0 -> true end,
  case gen_tcp:accept(Socket, ?TIMEOUT) of
    {ok, Connection} -> handle_new_connection(Connection, Callback);
    {error, timeout} -> ok
  end,
  loop(State#state{stay_alive = StayAlive andalso ?IsParentAlive});

loop(#state{type = ?CLIENT_TYPE, stay_alive = true} = State) ->
  Msg = receive Any -> Any end,
  NewState = case handle(State, Msg) of
               #state{} = NS -> NS;
               _             -> State
             end,
  loop(NewState#state{stay_alive = NewState#state.stay_alive andalso ?IsParentAlive});

loop(#state{socket = Socket, type = ?LISTEN_TYPE, stay_alive = false}) ->
  gen_tcp:close(Socket).

handle_new_connection(Connection, Callback) ->
  io:format("new connection!\n\n"), %%TODO
  Pid = spawn(?MODULE, start_process, [#state{socket = Connection, callback = Callback, type = ?CLIENT_TYPE}]),
  gen_tcp:controlling_process(Connection, Pid).

%% handle_connection(Socket, Callback) ->
%%   receive
%%     {tcp, _Connection, Data} -> io:format("got data:\n~s\n\n",[Data]), %%TODO
%%                                 handle_http(Socket, Data, Callback)
%%   after ?TIMEOUT -> ok
%%   end.

handle(#state{socket = Socket, callback = Callback} = State, {tcp, _Connection, Data}) ->
  handle_http(Socket, Data, Callback)




handle_http(Socket, <<"POST ", Rest/binary>>, Callback) ->
  {Path, Body} = strip_headers(Rest),
  io:format("headers\n~s\n~s\n\n", [Path, Body]), %%TODO
  gen_tcp:send(Socket, <<"HTTP/1.0 200 OK">>),
  ?SendCallbackEvent(#callback_event{event_object = {post, Path, Body},
                                     callback = Callback});
handle_http(Socket, <<"GET ", Rest/binary>>, Callback) ->
  {Path, _Body} = strip_headers(Rest),
  io:format("headers\n~s\n\n", [Path]), %%TODO
  ?SendCallbackEvent(#callback_event{event_object = {get, Path, self()},
                                     callback = Callback}),
  receive
    {response,
     Data, Type} -> PackageData = package_data(Data, Type),
                    gen_tcp:send(Socket, PackageData)
  end.

strip_headers(Rest) ->
  io:format("stripping:\n~s\n\n",[Rest]), %%TODO
  [RequestHead, Headers] = binary:split(Rest, [<<"\n">>, <<"\r\n">>]),
  io:format("requesthead: ~p\nheaders: ~p\n\n", [RequestHead, Headers]), %%TODO
  [Path | _] = binary:split(RequestHead, <<" ">>),
  io:format("path: ~p\n\n", [Path]), %%TODO
  [_, Body] = binary:split(Headers, [<<"\n\n">>, <<"\r\n\r\n">>]),
  io:format("body: ~p\n\n", [Body]), %%TODO
  {Path, Body}.

package_data(Data, Type) ->
  Length = integer_to_binary(byte_size(Data)),
  <<"HTTP/1.0 200 OK\r\n",
    "Content-Type: ", Type/binary, "\r\n",
    "Connection: Keep-Alive\r\n",
    "Content-Length: ", Length/binary, "\r\n\r\n",
    Data/binary>>.

