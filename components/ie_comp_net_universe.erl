-module(ie_comp_net_universe).

-include("interactive_erlang.hrl").
-include("idiagnostic.hrl").
-include("iterminal.hrl").
-include("iuniverse.hrl").

-behaviour(ie_component).

-record(state, {iworlds = [],
                callbacks,
                stay_alive = true}).

-define(Shutdown,   <<"shutdown">>).
-define(Disconnect, <<"disconnect">>).

%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, stop/1, event/2]).
-export([start_process/1]).

component_info() -> #component_info{iworld = #type_info{init_info = undefined},
                                    iuniverse = #type_info{init_info = [ie_comp_net_lobby,
                                                                        ie_comp_diagnostic,
                                                                        ie_comp_terminal]}}.

describe(Options) ->
  Connect = case ?OptionGetValue(connect, Options) of
              undefined -> undefined;
              Callback1 -> {"connect", Callback1}
            end,
  Disconnect = case ?OptionGetValue(disconnect, Options) of
                 undefined -> undefined;
                 Callback2 -> {"disconnect", Callback2}
               end,
  Receive = case ?OptionGetValue('receive', Options) of
              undefined -> undefined;
              Callback3 -> {"receive", Callback3}
            end,
  [Connect, Disconnect, Receive].

init(#iState{options = Options, extra_data = ExtraData} = IState) ->
  try
    Connect = ?OptionGetValue(connect, Options),
    Disconnect = ?OptionGetValue(disconnect, Options),
    Receive = ?OptionGetValue('receive', Options),
    Connect == undefined andalso throw(connect),
    Receive == undefined andalso throw('receive'),
    spawn(?MODULE, start_process, [#state{callbacks = {Connect, Disconnect, Receive}}]),
    {ok, IState#iState{extra_data = ExtraData#{iworlds => []}}}
  catch
    _:Key -> {error, {?MISSING_CALLBACK, Key}}
  end.

stop(IState) ->
  ?Send(?MODULE, stop),
  ?Unregister(?MODULE),
  IState.

event(#iState{state = State, extra_data = #{iworlds := IWorlds} = ExtraData} = IState,
      #callback_event{callback = Callback,
                      event_object = {new, IWorld}}) ->
  ?Put("New world connected: ~p", [IWorld#iworld.name]),
  Dispatch = Callback(State, IWorld),
  IState#iState{state = ?Dispatch(Dispatch),
                extra_data = ExtraData#{iworlds => [IWorld | IWorlds]}};
event(#iState{state = State, extra_data = #{iworlds := IWorlds} = ExtraData} = IState,
      #callback_event{callback = Callback,
                      event_object = {leave, IWorld}}) ->
  ?Put("World disconnected: ~p", [IWorld#iworld.name]),
  Dispatch = Callback(State, IWorld),
  IState#iState{state = ?Dispatch(Dispatch),
                extra_data = ExtraData#{iworlds => lists:delete(IWorld, IWorlds)}};
event(#iState{state = State} = IState,
      #callback_event{callback = Callback,
                      event_object = {message, IWorld, Message}}) ->
  Dispatch = Callback(State, IWorld, Message),
  IState#iState{state = ?Dispatch(Dispatch)}.

%% ====================================================================
%% Internal functions
%% ====================================================================

start_process(State) ->
  register(?MODULE, self()),
  loop(State).

loop(#state{stay_alive = true} = State) ->
  Msg = receive Any -> Any end,
  NewState = case handle(State, Msg) of
               #state{} = NS -> NS;
               _             -> State
             end,
  loop(NewState#state{stay_alive = NewState#state.stay_alive andalso ?IsParentAlive});

loop(#state{iworlds = IWorlds, stay_alive = false}) ->
  lists:foreach(fun shutdown/1, [IWorld#iworld.socket || IWorld <- IWorlds]),
  ok.

%% New
handle(#state{iworlds = IWorlds, callbacks = {Connect, _, _}} = State, {add, IWorld}) ->
  ?SendCallbackEvent(#callback_event{callback = Connect,
                                     event_object = {new, IWorld}}),
  State#state{iworlds = [IWorld | IWorlds]};
%% Outgoing
handle(#state{iworlds = IWorlds,
              callbacks = {_, Disconnect, _}}, {send, Socket, Message}) ->
  case gen_tcp:send(Socket, ie_utils_net:encode(Message)) of
    ok              -> ?SendToDiagnostic(sendB);
    {error, closed} -> IWorld = find_iworld(Socket, IWorlds),
                       ?SendToDiagnostic(connDown),
                       ?SendToDiagnostic({sendA, -1}),
                       ?SendToDiagnostic(#callback_event{callback = Disconnect,
                                                         event_object = {leave, IWorld}}),
                       ok;
    {error, Reason} -> io:format("Message failed to send!\nMessage:\n~p\nError: ~p\n", [Message, Reason])
  end;
%% Incoming
handle(#state{iworlds = IWorlds,
              callbacks = {_, Disconnect, Receive}}, {tcp, Socket, Data}) ->
  IWorld = find_iworld(Socket, IWorlds),
  case Data of
    ?Disconnect -> ?SendToDiagnostic(connDown),
                   ?SendCallbackEvent(#callback_event{callback = Disconnect,
                                                      event_object = {leave, IWorld}}),
                   disconnect(Socket),
                   ok;
    _Bytes      -> ?SendToDiagnostic(recv),
                   ?SendCallbackEvent(#callback_event{callback = Receive,
                                                      event_object = {message, IWorld,
                                                                      ie_utils_net:decode(Data)}})
  end;
%% Disconnections
handle(#state{iworlds   = IWorlds,
              callbacks = {_, Disconnect, _}}, {Action, Socket}) when Action == tcp_closed orelse
                                                                        Action == disconnect ->
  IWorld = find_iworld(Socket, IWorlds),
  case Action of
    tcp_closed -> ?SendCallbackEvent(#callback_event{callback = Disconnect,
                                                     event_object = {leave, IWorld}});
    disconnect -> disconnect(Socket)
  end,
  ?SendToDiagnostic(connDown),
  ok;
handle(State, stop) ->
  State#state{stay_alive = false};
handle(State, _Other) -> State.

find_iworld(Socket, IWorlds) ->
  lists:keyfind(Socket, #iworld.socket, IWorlds).

shutdown(Socket) ->
  close_connection(Socket, ?Shutdown).
disconnect(Socket) ->
  close_connection(Socket, ?Disconnect).

close_connection(Socket, Reason) ->
  gen_tcp:send(Socket, Reason),
  timer:sleep(1000),
  gen_tcp:close(Socket).
