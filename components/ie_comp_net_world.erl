-module(ie_comp_net_world).

-include("interactive_erlang.hrl").
-include("idiagnostic.hrl").

-behaviour(ie_component).

%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, stop/1, event/2]).
-export([start_process/1]).

-record(state, {address,
                decodedAddress,
                iworld,
                name,
                diagnostic,
                socket,
                callback,
                sent = 0,
                received = 0,
                link_down = false,
                stay_alive = true}).

-define(CONNECTATTEMPTS, ie_utils_net:connect_attempts()).
-define(PORT,            ie_utils_net:port()).
-define(SOCKET_OPTS,     ie_utils_net:socket_opts()).
-define(TIMEOUT,         ie_utils_net:timeout()).

component_info() -> #component_info{iworld = #type_info{init_info = [ie_comp_diagnostic]},
                                    iuniverse = #type_info{init_info = undefined}}.

describe(Options) ->
  Name = case ?OptionGetValue(name, Options) of
           undefined -> undefined;
           N         -> {"name", "This world will be aliased as ~p by the Universe.", [N]}
         end,
  Receive = case ?OptionGetValue('receive', Options) of
              undefined -> undefined;
              Callback2 -> {"receive", Callback2}
            end,
  Register = case ?OptionGetValue(register, Options) of
               undefined -> undefined;
               Address   -> {"receive", "This world will connect to the server at ~p.", [Address]}
             end,
  [Name, Receive, Register].

init(#iState{options = Options} = IState) ->
  init(IState, ?OptionGetValue(register, Options)).

init(_IState, undefined) -> undefined;
init(#iState{options = Options}, Register) ->
  Receive = ?OptionGetValue('receive', Options),
  case {Register =/= undefined, Receive} of
    {true, undefined} -> {error, {?MISSING_CALLBACK, 'receive'}};
    _ -> spawn(?MODULE, start_process,
               [#state{address = Register,
                       decodedAddress = ie_utils_net:decode_address(Register),
                       callback = Receive,
                       name = ?OptionGetValue3(name, Options, io_lib:format("IWorld~p",[random:uniform(99)]))}]),
         receive
           {?MODULE, Any} -> Any %% ok or {error, Error}
         end
  end.

stop(IState) ->
  ?Send(?MODULE, stop),
  ?Unregister(?MODULE),
  IState.

event(IState, #callback_event{callback = undefined}) -> IState;
event(IState, #callback_event{event_object = decode_error}) -> IState;
event(#iState{state = State} = IState, #callback_event{event_object = EventObject,
                                                       callback = Callback}) ->
  NewState = ?Dispatch(Callback(State, EventObject)),
  IState#iState{state = NewState}.

%% ====================================================================
%% Internal functions
%% ====================================================================


%Starts a separate process to send and receive messages from a Universe server.
start_process(#state{} = State) ->
  register(?MODULE, self()),
  case connect(State, ?CONNECTATTEMPTS) of
    {ok, Socket} -> ?Send(?PARENT_PROCESS, {?MODULE, ok}),
                    loop(State#state{socket = Socket});
    Error -> ?Send(?PARENT_PROCESS, {?MODULE, Error})
  end.

loop(#state{stay_alive = true} = State) ->
  Msg = receive Any -> Any end,
  NewState = case handle(State, Msg) of
               #state{} = NS -> NS;
               _             -> State
             end,
  loop(NewState#state{stay_alive = NewState#state.stay_alive andalso ?IsParentAlive});

loop(#state{socket = Socket, stay_alive = false}) ->
  gen_tcp:send(Socket, <<"disconnect">>),
  timer:sleep(1000),
  gen_tcp:close(Socket),
  ok.

handle(#state{socket = Socket, link_down = false} = State, {send, Message}) ->
  case gen_tcp:send(Socket, ie_utils_net:encode(Message)) of
    ok              -> ?SendToDiagnostic(sendB);
    {error, closed} -> ?SendClose("TCP Socket closed"),
                       State#state{link_down = true};
    {error, Reason} -> io:format("Message failed to send! ~nMessage:~n~p~nError: ~p~n", [Message, Reason])
  end;
handle(#state{callback = Callback} = State, {tcp, _, Data}) ->
  case Data of
    <<"disconnect">> -> ?SendClose("Universe disconnected"),
                        State#state{link_down = true};
    <<"shutdown">>   -> ?SendClose("Universe shut down"),
                        State#state{link_down = true};
    _Bytes           -> case ie_utils_net:decode(Data) of
                          {?IE_DISPATCH, Dispatch} -> ?Dispatch(ie_utils_common:wrap_dispatch(ok, Dispatch));
                          Got -> ?SendToDiagnostic(recv),
                                 ?SendCallbackEvent(#callback_event{event_object =  Got,
                                                                    callback = Callback})
                        end
  end;
handle(State, {tcp_closed, _}) ->
  ?SendClose("Universe disconnected"),
  State#state{link_down = true};
handle(State, stop) ->
  State#state{stay_alive = false};
handle(State, _Other) -> State.

connect(#state{address = Address}, 0) ->
  io:format("Failed to connect to ~p after ~p attempts!", [Address, ?CONNECTATTEMPTS]),
  {error, connection_timeout};
connect(#state{address = Address, decodedAddress = DecodedAddress, name = Name} = State, N) ->
  io:format("Attempting to connect to ~p ... ", [Address]),
  case gen_tcp:connect(DecodedAddress, ?PORT, ?SOCKET_OPTS, ?TIMEOUT) of
    {ok, Socket} -> io:format("SUCCEEDED!\n", []), send_name(Socket, Name);

    {error, timeout}  -> io:format("FAILED! (Attempt ~p of ~p)\n", [?CONNECTATTEMPTS - N + 1,
                                                                    ?CONNECTATTEMPTS]),
                         connect(State, N - 1);
    {error,
     econnrefused}    -> {error, could_not_find_universe};
    {error, Error}    -> {error, Error};
    {tcp_error, _Socket, Error} -> io:format("An unknown error has occured! Aborting. Error: ~p~n", [Error]),
                                   {error, Error}
  end.

send_name(Socket, Name) ->
  case gen_tcp:send(Socket, term_to_binary(Name)) of
    ok -> {ok, Socket};
    {error, Reason} -> io:format("Connection setup failed!\nName: ~p\nError: ~p", [Name, Reason]),
                       {error, {name_error, Reason}}
  end.
