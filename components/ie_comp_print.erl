-module(ie_comp_print).

-include("interactive_erlang.hrl").
-include("iuniverse.hrl").

-include_lib("wx/include/wx.hrl").

-behaviour(ie_component).

%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, stop/1, event/2]).

component_info() -> #component_info{iworld = #type_info{exec_info = ?EXEC_LAST},
                                    iuniverse = #type_info{exec_info = ?EXEC_LAST}}.

describe(Options) ->
  case ?OptionGetValue(print, Options) of
    undefined -> undefined;
    Callback  -> {"print", Callback}
  end.

init(#iState{options = Options}) ->
  case ?OptionGetValue(print, Options) of
    undefined -> undefined;
    _         -> ok
  end.

stop(IState) -> IState.

event(#iState{options = Options} = IState, CallbackEvent) ->
  event(IState, CallbackEvent, ?OptionGetValue(print, Options)).

event(IState, undefined, _Callback) ->
  IState;
event(IState, _CallbackEvent, undefined) ->
  IState;
event(#iState{state = State} = IState,
      CallbackEvent, Callback) ->
  io:format("~p: ", [header(CallbackEvent)]),
  case Callback(State) of
    {Fmt, Args} -> io:format(Fmt, Args);
    Text        -> io:format("~s\n", [Text])
  end,
  IState.

%% ====================================================================
%% Internal functions
%% ====================================================================

header(tick)                            -> "Tick";
header(#wx{event = #wxKey{}})           -> "Key";
header(#wx{event = #wxMouse{}})         -> "Mouse";
header({Name, #wx{event = #wxMouse{}}}) -> "Mouse/" ++ atom_to_list(Name);
header(#wx{event = #wxCommand{}})       -> "Menu";
header(#wx{event = #wxSize{}})          -> "Resize";
header({close, {error, Reason}})        -> "INTERNAL ERROR: " ++ Reason;
header({close, Reason})                 -> Reason;
header({new, _Pid, Name})               -> "New/" ++ Name;
header({leave, #iworld{name = Name}})   -> "Leave/" ++ Name;
header({message, _From, _Message})      -> "Message";
header({'receive', _Message})           -> "Receive";
header(_)                               -> "".
