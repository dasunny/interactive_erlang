-module(ie_comp_diagnostic).

-include("interactive_erlang.hrl").
-include("idiagnostic.hrl").

-include_lib("wx/include/wx.hrl").

-behaviour(ie_component).

%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, stop/1, event/2]).
-export([start_process/1]).

-define(DIAG_GAME, "GAME").

-record(page, {panel,
               sizer}).

-record(kv_pair, {key   = "",
                  value = 0,
                  sort  = 0,
                  note}).
-record(kv_section, {title = "",
                     kvs   = [],
                     sort  = 0}).

-record(state, {canvas,
                textctrl,
                kvs = [],
                wx,
                stay_alive = true,
                sections = [#kv_section{title = ?DIAG_SYSTEM,     sort = 0},
                            #kv_section{title = ?DIAG_RENDER,     sort = 1},
                            #kv_section{title = ?DIAG_CONNECTION, sort = 2},
                            #kv_section{title = ?DIAG_GAME,       sort = 3}],
                time = 0,
                tickTime = 0,
                sentA = 0, %Reported Sent
                sentB = 0, %Actual Sent
                received = 0,
                connections = 0,
                canvasNames = [],
                canvasMap,
                title,
                doTick = true,
                doDraw = true,
                type,
                wx_env}).

component_info() -> #component_info{iworld = #type_info{init_info = [ie_comp_wx_init],
                                                        exec_info = ?EXEC_LAST},
                                    iuniverse = #type_info{init_info = [ie_comp_wx_init],
                                                           exec_info = ?EXEC_LAST}}.

describe(Options) ->
  case ?OptionGetValue(diagnostic, Options) of
    undefined -> undefined;
    Callback  -> {"diagnostic", Callback}
  end.

init(#iState{type = Type,
             extra_data = #{wx     := Wx,
                            wx_env := Env},
             options = Options}) ->
  Title = ?OptionGetValue3(title, Options, "Interactive Erlang IWorld"),
  case ?OptionGetValue(diagnostic, Options) of
    undefined -> undefined;
    _         -> %spawn(?MODULE, start_process, [#state{title = Title, type = Type,
%%                                                        wx = Wx, wx_env = Env}]),
                 ok
  end.

stop(IState) ->
  ?Unregister(?MODULE),
  IState. %%TODO implement

event(IState, _) -> IState; %%TODO
event(#iState{state = State,
              options = Options} = IState, _CallbackEvent) ->
  case ?OptionGetValue(diagnostic, Options) of
    undefined -> IState;
    Callback  -> KVs = Callback(State),
                 ?Send(?MODULE, {?DIAG_GAME, kvs, set, KVs}),
                 IState
  end.

%% ====================================================================
%% Internal functions
%% ====================================================================

start_process(#state{title = Title, wx = Wx, wx_env = Env} = State) ->
  register(?MODULE, self()),
  wx:set_env(Env),
  Width = 800,
  Height = 600,
  Frame = wxFrame:new(Wx, ?wxID_ANY, "Diagnostics for " ++ Title, [{size, {Width, Height}},
                                                                             {style, ?wxMINIMIZE_BOX
                                                                                bor ?wxRESIZE_BORDER
                                                                                bor ?wxSYSTEM_MENU
                                                                                bor ?wxCAPTION
                                                                                bor ?wxCLOSE_BOX
                                                                                bor ?wxCLIP_CHILDREN}]),
  wxWindow:setClientSize(Frame, Width, Height),
  Panel = wxPanel:new(Frame),

  MainSizer   = wxBoxSizer:new(?wxVERTICAL),
  wxPanel:setSizer(Panel, MainSizer, []),
%%   wxSizer:add(MainSizer, TextBox, [{flag, ?wxEXPAND}, {proportion, 1}]),

  Notebook = wxNotebook:new(Panel, ?wxID_ANY, [{style, ?wxNB_LEFT bor ?wxBK_DEFAULT}]),
  wxSizer:add(MainSizer, Notebook, [{flag, ?wxEXPAND}, {proportion, 1}]),

  Page1 = wxPanel:new(Notebook, []),
  wxNotebook:addPage(Notebook, Page1, "page1", []),
  wxPanel:setBackgroundColour(Page1, ?wxRED),

  Page2 = wxPanel:new(Notebook, []),
  wxNotebook:addPage(Notebook, Page2, "page2", []),
  wxPanel:setBackgroundColour(Page2, ?wxBLUE),

  Page3 = wxPanel:new(Notebook, []),
  wxNotebook:addPage(Notebook, Page3, "page3", []),
  wxPanel:setBackgroundColour(Page3, ?wxBLACK),

  TextBox = wxTextCtrl:new(Page1, ?wxID_ANY, [{style, ?wxDEFAULT
                                                 bor ?wxTE_MULTILINE
                                                 bor ?wxHSCROLL
                                                 bor ?wxVSCROLL
                                                 bor ?wxTE_READONLY}]),

  Sizer1   = wxBoxSizer:new(?wxVERTICAL),
  wxPanel:setSizer(Page1, Sizer1, []),
  wxSizer:add(MainSizer, TextBox, [{flag, ?wxEXPAND}, {proportion, 1}]),
%%   wxFrame:show(Frame, []),

  CB = fun(_,_) -> ?SendClose("Scene closed") end,
  wxEvtHandler:connect(Frame, close_window, [{callback, CB}]),

  loop(State#state{canvas = Panel, canvasMap = maps:new(), textctrl = TextBox}).

loop(#state{stay_alive = true, sections = Sections, sentA = SentA, received = Recv,
            sentB = SentB, connections = Conns, doTick = DoTick, doDraw = DoDraw,
            canvasNames = Names, canvasMap = Map} = State) ->
  draw(State),
  Received = receive
               Any -> Any
             after
               500 -> undefined
             end,
  NewState = case Received of
               {SectionTitle, Key, Op, Value} ->
                 MySection = case lists:keyfind(SectionTitle, #kv_section.title, Sections) of
                               false    -> #kv_section{title = SectionTitle, sort = length(Sections+1)};
                               ASection -> ASection
                             end,
                 MySectionKVs = MySection#kv_section.kvs,
                 KV = case lists:keyfind(Key, #kv_pair.key, MySectionKVs) of
                        false -> #kv_pair{key = Key, sort = length(MySectionKVs)+1};
                        AKV   -> AKV
                      end,
                 UpdatedKV = updateKV(KV, Op, Value),
                 UpdatedSectionKVs = lists:keystore(Key, #kv_pair.key, MySectionKVs, UpdatedKV),
                 UpdatedSections = lists:keystore(SectionTitle, #kv_section.title, Sections,
                                                  MySection#kv_section{kvs = UpdatedSectionKVs}),
                 State#state{sections = UpdatedSections};
               _ -> State
             end,




%%                {kvs,  KVs}         -> State#state{kvs         = KVs};
%%                {tick, Time}        -> State#state{time        = Time};
%%                {tickTime, Time}    -> State#state{tickTime    = Time};
%%                sendA               -> State#state{sentA       = SentA + 1};
%%                sendB               -> State#state{sentB       = SentB + 1};
%%                recv                -> State#state{received    = Recv  + 1};
%%                {sendA, N}          -> State#state{sentA       = SentA + N};
%%                {sendB, N}          -> State#state{sentB       = SentB + N};
%%                {recv, N}           -> State#state{received    = Recv  + N};
%%                connUp              -> State#state{connections = Conns + 1};
%%                connDown            -> State#state{connections = Conns - 1};
%%                switchTick          -> State#state{doTick      = (not DoTick)};
%%                switchDraw          -> State#state{doDraw      = (not DoDraw)};
%%                {register, Name}    -> State#state{canvasMap   = maps:put(Name, 0, Map),
%%                                                   canvasNames = lists:sort([Name | Names])};
%%                {time, _Name, skip} -> State;
%%                {time, Name, Time}  -> State#state{canvasMap = maps:put(Name, Time, Map)}
%%              after
%%                  500               -> State
%%              end,
  loop(NewState#state{stay_alive = ?IsParentAlive});

loop(#state{canvas = Canvas, stay_alive = false}) ->
  wxPanel:destroy(Canvas),
  ok.

draw(#state{canvas = Canvas, textctrl = TextBox, kvs = KVs, sections = Sections,
            time = Time, tickTime = TickTime, type = Type,
            sentA = SentA, sentB = SentB, received = Recv, connections = Conns,
            doTick = DoTick, doDraw = DoDraw, canvasNames = CanvasNames, canvasMap = Map}) ->
%%   Fun = fun(Elem, {Acc, N}) -> drawKVs(Acc, Elem, N) end,
%%   {Width,Height} = wxWindow:getClientSize(Canvas),
  Type2 = case Type of
            iworld    -> "IWORLD";
            iuniverse -> "IUNIVERSE"
          end,
  {_, QueueLength} = process_info(whereis(?PARENT_PROCESS), message_queue_len),
  AllKVs = lists:flatten([{io_lib:format("-----~s DIAGNOSTICS-----", [Type2])},
                          {"Event queue length", integer_to_list(QueueLength)},
                          {"    (Press F10 to flush queue)"},
                          {"World Tick",           integer_to_list(Time)},
                          {"Tick time",            integer_to_list(TickTime)},
                          {"Ticking",              atom_to_list(DoTick)},
                          {"    (Press F11 to pause the Ticker)"},
                          {"-----RENDER DIAGNOSTICS-----"},
                          {"Drawing",              atom_to_list(DoDraw)},
                          {"    (Press F12 to pause Drawing)"},
                          timeKVs(CanvasNames, Map),
                          {"-----CONNECTION DIAGNOSTICS-----"},
                          ignoreIf0("Connections", Conns),
                          ignoreIf0("Messages Sent", SentA, SentB),
                          ignoreIf0("Messages Received", Recv),
                          {"-----GAME DIAGNOSTICS------"},
                          KVs]),
  HorPos = wxTextCtrl:getScrollPos(TextBox, ?wxHORIZONTAL),
  VerPos = wxTextCtrl:getScrollPos(TextBox, ?wxVERTICAL),
  wxTextCtrl:setValue(TextBox, toSingleString(AllKVs)),
  wxTextCtrl:setScrollPos(TextBox, ?wxHORIZONTAL, HorPos),
  wxTextCtrl:setScrollPos(TextBox, ?wxVERTICAL, VerPos).

updateKV(#kv_pair{value = Value} = KV, add,     NewValue) when
  is_integer(Value) orelse is_float(Value)                -> KV#kv_pair{value = Value + NewValue};
updateKV(#kv_pair{value = Value} = KV, add,     NewValue) when
  is_list(Value)                                          -> KV#kv_pair{value = Value ++ [NewValue]};
updateKV(#kv_pair{value = Value} = KV, switch, _NewValue) when
  is_boolean(Value)                                       -> KV#kv_pair{value = not Value};
updateKV(KV,                           new,     NewValue) -> KV#kv_pair{value = NewValue};
updateKV(KV,                           set,     NewValue) -> KV#kv_pair{value = NewValue};
updateKV(KV,                           _Op,    _NewValue) -> KV.

ignoreIf0(_Key, 0) -> [];
ignoreIf0(Key, N) ->
  String = integer_to_list(N),
  {Key, String}.

ignoreIf0(_Key, 0, 0) -> [];
ignoreIf0(Key, Na, 0) ->
  ignoreIf0(Key, Na);
ignoreIf0(Key, Na, Nb) ->
  String = io_lib:format("~p/~p - Difference ~p", [Na, Nb, abs(Na-Nb)]),
  {Key, String}.

timeKVs(CanvasNames, Map) ->
  Fun = fun(Elem) -> String = integer_to_list(maps:get(Elem, Map)),
                     Key    = Elem ++ " frame time",
                     {Key, String} end,
  [Fun(Name) || Name <- CanvasNames].

flattenSection(#kv_section{title = Title, kvs = KVs}) ->
  SortedKVs = lists:keysort(#kv_pair.sort, KVs),
  FlattenedKVs = [flattenKV(KV) || KV <- SortedKVs],
  io_lib:format("~s\n~s", [Title, FlattenedKVs]).

flattenKV(#kv_pair{key = Key, value = Value, note = undefined}) -> io_lib:format("~s:~p\n", [Key, Value]);
flattenKV(#kv_pair{key = Key, value = Value, note = Note})      -> io_lib:format("~s:~p\n~s\n", [Key, Value, Note]).


toSingleString(KVs) ->
  Fun = fun({Elem})       -> Elem ++ "\n";
           ({Key, Value}) -> io_lib:format("~s:~s\n", [Key, Value]) end,
  lists:flatten(lists:map(Fun, KVs)).
