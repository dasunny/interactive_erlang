-module(ie_comp_debug).

-include("interactive_erlang.hrl").

-behaviour(ie_component).

%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, stop/1, event/2]).

component_info() -> #component_info{iworld = #type_info{init_info = ?INIT_FIRST},
                                    iuniverse = #type_info{init_info = ?INIT_FIRST}}.

describe(Options) ->
  case lists:member(debug, Options) of
    true -> {"debug"};
    _    -> undefined
  end.

init(#iState{options = Options} = IState) ->
  init(IState, lists:member(debug, Options)).

init(_IState, false) -> undefined;
init(#iState{options       = Options,
             ie_components = Components}, true) ->
  RawDescribes = [Component:describe(Options) || Component <- Components],
  Formatted = lists:map(fun format_describe/1, lists:flatten(RawDescribes)),
  lists:foreach(fun(Elem) -> io:format("~s", [Elem]) end, lists:sort(Formatted)),
  ok.

stop(IState) -> IState.

event(IState, _CallbackEvent) ->
  IState.

%% ====================================================================
%% Internal functions
%% ====================================================================

key(Key) -> io_lib:format("Option '~s' matched.", [Key]).

format_describe(Description) ->
  case Description of
    undefined        -> [];
    {Key}            -> key(Key) ++ "\n";
    {Key, Function}  -> {module, Module} = erlang:fun_info(Function, module),
                        {name,   Name}   = erlang:fun_info(Function, name),
                        {arity,  Arity}  = erlang:fun_info(Function, arity),
                        io_lib:format("Option '~s' matched and bound to ~p:~p/~p.\n", [Key, Module, Name, Arity]);
    {Key, Fmt, Args} -> io_lib:format("~s " ++ Fmt ++ "\n", [key(Key)] ++ Args)
  end.
