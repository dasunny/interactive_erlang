-module(ie_comp_net_lobby).

-include("interactive_erlang.hrl").
-include("idiagnostic.hrl").
-include("iuniverse.hrl").

-behaviour(ie_component).

%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, stop/1, event/2]).
-export([start_process/1]).

-record(state, {socket,
                stay_alive = true}).

-define(CONNECTATTEMPTS, ie_utils_net:connect_attempts()).
-define(PORT,            ie_utils_net:port()).
-define(SOCKET_OPTS,     ie_utils_net:socket_opts()).
-define(TIMEOUT,         ie_utils_net:timeout()).
-define(NET_UNIVERSE,    ie_comp_net_universe).

component_info() -> #component_info{iworld = #type_info{init_info = undefined}}.

describe(_Options) -> undefined.

init(_IState) ->
  spawn(?MODULE, start_process, [#state{}]),
  ok.

stop(IState) ->
  ?Send(?MODULE, stop),
  ?Unregister(?MODULE),
  IState.

event(IState, _CallbackEvent) -> IState.

%% ====================================================================
%% Internal functions
%% ====================================================================

start_process(State) ->
  register(?MODULE, self()),
  {ok, Socket} = gen_tcp:listen(?PORT, ?SOCKET_OPTS),
  loop(State#state{socket = Socket}).

loop(#state{socket = Socket, stay_alive = true} = State) ->
  StayAlive = receive stop -> false after 0 -> true end,
  case gen_tcp:accept(Socket, ?TIMEOUT) of
    {ok, Connection} -> handle_new_connection(Connection);
    {error, timeout} -> ok
  end,
  loop(State#state{stay_alive = StayAlive andalso ?IsParentAlive});

loop(#state{socket = Socket, stay_alive = false}) ->
  gen_tcp:close(Socket).

handle_new_connection(Connection) ->
  receive
    {tcp, _Connection, <<"ping">>}  -> respond_to_ping(Connection);
    {tcp, _Connection, EncodedName} ->
      Name = binary_to_term(EncodedName),
      FormattedName = ie_utils_net:format_name(Name, Connection),
      IWorld = #iworld{socket = Connection,
                       name = FormattedName},
      ?Send(?NET_UNIVERSE, {add, IWorld}),
      ?SendToDiagnostic(connUp),
      gen_tcp:controlling_process(Connection, whereis(?NET_UNIVERSE))
  end.

respond_to_ping(Socket) ->
  gen_tcp:send(Socket, <<"pong">>),
  gen_tcp:close(Socket).
