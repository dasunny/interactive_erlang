-module(ie_comp_tick).

-include("interactive_erlang.hrl").
-include("idiagnostic.hrl").
-include("imoderator.hrl").

-behaviour(ie_component).

%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, stop/1, event/2]).
-export([start_process/1]).

-record(state, {callback,
                rate,
                stay_alive = true,
                count = 0,
                do_tick = true}).

component_info() -> #component_info{iworld = #type_info{init_info = [ie_comp_moderator]}}.

describe(Options) ->
  Tick = case ?OptionGetValue(tick, Options) of
           undefined -> undefined;
           Callback  -> {"tick", Callback}
         end,
  Rate = case ?OptionGetValue(clock_rate, Options) of
           undefined -> undefined;
           R         -> {"clock_rate", "This world will tick at ~p times per second.", [R]}
         end,
  [Tick, Rate].

init(#iState{options = Options,
             type = Type}) ->
  Callback = ?OptionGetValue(tick, Options),
  case {Type, Callback} of
    {iuniverse, undefined} -> {error, {?MISSING_CALLBACK, tick}};
    {_,         undefined} -> ok;
    _                      -> spawn(?MODULE, start_process, [#state{callback = Callback,
                                                                    rate = ?OptionGetValue3(clock_rate, Options, 10)}]),
                              ok
  end.

stop(IState) ->
  ?Send(?MODULE, stop),
  ?Unregister(?MODULE),
  IState.

event(IState, #callback_event{callback = undefined}) ->
  IState;
event(#iState{state = State} = IState,
      #callback_event{callback = Callback}) ->
  {Time, NewState} = timer:tc(Callback, [State]),
  ?SendToDiagnostic({tickTime, Time div 1000}),
  IState#iState{state = ?Dispatch(NewState)}.

%% ====================================================================
%% Internal functions
%% ====================================================================

start_process(State) ->
  register(?MODULE, self()),
  loop(State).

loop(#state{callback = Callback, rate = Rate, stay_alive = true, count = Count, do_tick = DoTick} = State) ->
  Got = receive Any -> Any
        after 0 -> undefined end,

  {StayAlive, Switch} =
    case Got of
      undefined -> DoTick andalso
                     begin
                       timer:sleep(trunc(1000/Rate)),
                       ?SendCallbackEvent(#callback_event{callback = Callback,
                                                          event_object = tick}),
                       remainder(Count, Rate) == 0 andalso ?SendToDiagnostic({tick, divide(Count, Rate)})
                     end,
                   {true, false};
      stop      -> {false, false};
      switch    -> %%TODO ?SendToDiagnostic(switchTick)
                   {true, true}
    end,

  loop(State#state{stay_alive = StayAlive andalso ?IsParentAlive,
                   count = Count + 1, do_tick = DoTick xor Switch});

loop(#state{stay_alive = false}) ->
  ok.

remainder(Count, Rate) when is_integer(Rate) ->
  Count rem Rate;
remainder(Count, Rate) when is_float(Rate) ->
  Num = Count/Rate,
  Rem = Num - trunc(Num),
  Rem*Rate.

divide(Count, Rate) when is_integer(Rate) ->
  Count div Rate;
divide(Count, Rate) when is_float(Rate) ->
  trunc(Count/Rate).