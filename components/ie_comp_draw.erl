-module(ie_comp_draw).

-include("interactive_erlang.hrl").
-include("idiagnostic.hrl").
-include("idraw.hrl").

-behaviour(ie_component).

%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, stop/1, event/2]).
-export([start_process/1, draw_permanent/1, reset_canvas/1, get_simple_canvas/1]).

-define(HORIZONTAL, horizontal).
-define(VERTICAL,   vertical).
-define(GENERATIONLIMIT, 0).
-define(OVERTIMELIMIT, 25).
-define(sqrt, fun math:sqrt/1).

-record(state, {stay_alive = true,
                do_draw = true,
                canvas,
                callback,
                refresh_rate,
                wx_env,
                last_frame_time = 0}).

component_info() -> #component_info{iworld = #type_info{init_info = [ie_comp_diagnostic],
                                                        exec_info = ?EXEC_LAST},
                                    iuniverse = #type_info{init_info = undefined}}.

describe(Options) ->
  Draw = case ?OptionGetValue(draw, Options) of
           undefined -> undefined;
           Callback  -> {"draw", Callback}
         end,
  Panes = case ?OptionGetValue(panes, Options) of
            undefined -> undefined;
            P         -> {"panes", "~p panes found.", [length(P)]}
          end,
  Title = case ?OptionGetValue(title, Options) of
            undefined -> undefined;
            T         -> {"title", "This window will be called ~p.", [T]}
          end,
  Size = case ?OptionGetValue(size, Options) of
           undefined -> undefined;
           {W, H}    -> {"size", "This window will have a size of ~px~p.", [W,H]}
         end,
  Icon = case ?OptionGetValue(icon, Options) of
           undefined -> undefined;
           Path      -> {"icon", "This window will use the icon located at ~s.", [Path]}
         end,
  [Draw, Panes, Title, Size, Icon].

init(#iState{options = Options} = IState) ->
  init(IState, ?OptionGetValue(draw, Options)).

init(_IState, undefined) -> {error, {?MISSING_CALLBACK, draw}};
init(#iState{options = Options,
             extra_data = #{wx := Wx,
                            wx_env := Env} = ExtraData} = IState, Callback) ->
  Size  = ?OptionGetValue3(size, Options, {500,500}),
  Title = ?OptionGetValue3(title, Options, "Interactive Erlang IWorld"),
  %%   Icon = ?OptionGetValue(icon, Options) %%TODO implement see bottom
  ParentWindow = create_window(Wx, Size, Title),
  Fun = fun(Elem) -> Pid = spawn(?MODULE, start_process,
                                 [#state{canvas     = Elem,
                                         callback   = Callback,
                                         refresh_rate = trunc(1000/?OptionGetValue3(clock_rate, Options, 10)),
                                         wx_env = Env}]),
                     Elem#canvas{pid = Pid} end,
  
  Canvases0 = create_canvases(ParentWindow, ?OptionGetValue3(panes, Options, [])),
  Canvases1 = [Fun(Canvas) || Canvas <- Canvases0],
  wxFrame:show(ParentWindow),
  {ok, IState#iState{extra_data = ExtraData#{canvases => Canvases1,
                                             window   => ParentWindow}}}.

stop(#iState{extra_data = #{canvases := Canvases,
                            window := Window}} = IState) ->
  wxFrame:hide(Window),
  lists:foreach(fun(#canvas{name = Name,
                            pid = Pid}) -> ?Unregister(Name),
                                           ?Send(Pid, stop) end, Canvases),
  wxFrame:destroyChildren(Window),
  wxFrame:destroy(Window),
  IState.

event(#iState{state = WorldState,
              options = Options,
              extra_data = #{canvases := Canvases}} = IState,
      _CallbackEvent) ->
  Callback = ?OptionGetValue(draw, Options),
  Now = now_milli(),
  Fun = fun(#canvas{pid = Pid,
                    simple_canvas = Simple}) -> Draws = get_draws(WorldState, Callback, Simple),
                                                {_, TrimmedDraws} = trim_draws(Draws),
                                                ?Send(Pid, {draw, TrimmedDraws, Now}) end,
  lists:foreach(Fun, Canvases),
  IState.

draw_permanent(Draws) ->
  {#simple_canvas{name = Name},
   TrimmedDraws} = trim_draws(Draws),
  ?Send(Name, {perm, TrimmedDraws}).

reset_canvas(Name) ->
  ?Send(Name, reset).

get_simple_canvas(Name) ->
  Ref = make_ref(),
  ?Send(Name, {get_simple, Ref, self()}),
  receive
    {Ref, SimpleCanvas} -> SimpleCanvas
  end.

%% ====================================================================
%% Internal functions
%% ====================================================================

start_process(#state{wx_env = Env, canvas = #canvas{name = Name,
                                                    memoryDC1 = MemoryDC1}} = State) ->
  put(canvas, MemoryDC1),
  try
    unregister(Name)
  catch _:_ -> ok end,
  register(Name, self()),
  ?SendToDiagnostic({register, Name}),
  wx:set_env(Env),
  loop(State).

loop(#state{stay_alive = true, do_draw = DoDraw,
            canvas = #canvas{name = Name,
                             memoryDC1 = PermCanvas,
                             simple_canvas = Simple} = Canvas} = State) ->
  Got = receive Any -> Any
        after 500 -> redraw(Canvas) end,
  case Got of
    {draw, Draws, Then}
      when DoDraw     -> Time = draw(State, Draws, now_milli() - Then),
                         ?SendToDiagnostic({time, Name, Time});
    {perm, PermDraws} -> draw_permanent(PermCanvas, PermDraws);
    reset             -> wxDC:clear(PermCanvas);
    {get_simple, Ref, Pid} -> ?Send(Pid, {Ref, Simple});
    _                 -> ok
  end,
  NewState = case Got of
               switch                  -> ?SendToDiagnostic({time, Name, -1}), %%TODO implement properly
                                          State#state{do_draw = not DoDraw};
               {resize, Width, Height} -> State#state{canvas = resize_canvas(Canvas#canvas{width = Width,
                                                                                           height = Height})};
               stop                    -> State#state{stay_alive = false};
               _                       -> State
             end,
  loop(NewState#state{stay_alive = NewState#state.stay_alive and ?IsParentAlive});

loop(#state{stay_alive = false}) ->
  ok.

create_window(Wx, Size, Title) ->
  Frame = wxFrame:new(Wx, 1, Title, [{size, Size}]),
  wxFrame:setClientSize(Frame, Size),
  CB = fun(_,_) -> ?SendClose("Window closed") end,
  wxEvtHandler:connect(Frame, close_window, [{callback, CB}]),
  Frame.

create_canvases(Parent, []) ->
  {Width, Height} = wxWindow:getClientSize(Parent),
  create_canvases(Parent, [{?DEFAULT_CANVAS, Width, Height, 0, 0}]);

create_canvases(Parent, Panes) ->
  CB = fun(_,_) -> wxWindow:setFocus(Parent) end,
  Fun = fun({Name, Width, Height, X, Y}) ->
                Panel = wxPanel:new(Parent, X, Y, Width, Height),
                wxEvtHandler:connect(Panel, child_focus, [{callback, CB}]),
                Bitmap1 = wxBitmap:new(Width,Height),
                Bitmap2 = wxBitmap:new(Width,Height),
                MemoryDC1 = wxMemoryDC:new(Bitmap1),
                MemoryDC2 = wxMemoryDC:new(Bitmap2),
                ClientDC = wxClientDC:new(Panel),
                wxMemoryDC:selectObjectAsSource(MemoryDC1, Bitmap1),
                wxBitmap:destroy(Bitmap2),
                put(canvas, MemoryDC1),
                #canvas{name      = Name,
                        width     = Width,
                        height    = Height,
                        bitmap    = Bitmap1,
                        memoryDC1 = MemoryDC1,
                        memoryDC2 = MemoryDC2,
                        clientDC  = ClientDC,
                        panel     = Panel,
                        simple_canvas = #simple_canvas{name   = Name,
                                                       width  = Width,
                                                       height = Height}} end,
  [Fun(Pane) || Pane <- Panes].

resize_canvas(#canvas{width  = Width,
                      height = Height,
                      bitmap = Bitmap1,
                      simple_canvas = Simple} = Canvas) ->
  Bitmap2 = wxBitmap:new(Width, Height),
  wxBitmap:setWidth(Bitmap1, Width),
  wxBitmap:setHeight(Bitmap1, Height),
  MemoryDC1 = wxMemoryDC:new(Bitmap1),
  MemoryDC2 = wxMemoryDC:new(Bitmap2),
  put(canvas, MemoryDC1),
  Canvas#canvas{memoryDC1     = MemoryDC1,
                memoryDC2     = MemoryDC2,
                simple_canvas = Simple#simple_canvas{width  = Width,
                                                     height = Height}}.

now_milli() ->
  {Mega, Seconds, Micro} = erlang:now(),
  ((Mega*1000000 + Seconds)*1000000 + Micro) div 1000.

draw(#state{refresh_rate = Rate} = State, _Draws, Diff) when Diff >= Rate ->
  % io:format("Skipped Draw. Diff: ~p. Required: ~p\n", [Diff, Rate]),
  State;

draw(#state{canvas = #canvas{width = Width, height = Height,
                             memoryDC1 = MemoryDC1, memoryDC2 = MemoryDC2, clientDC = ClientDC} = Canvas},
     Draws, _Diff) ->
  Pred = fun(Elem) -> within(Canvas, Elem) end,
  Start = now_milli(),
  wxDC:clear(MemoryDC2),
  FilteredDraws = lists:filter(Pred, Draws),
  wxDC:blit(MemoryDC2,       %destination canvas
            {0,0},           %destination point
            {Width, Height}, %width/height of copy area
            MemoryDC1,       %source canvas
            {0,0}),          %source point
  lists:foreach(fun(Elem) -> ie_utils_draw:draw(MemoryDC2, Elem) end, FilteredDraws),
  wxDC:blit(ClientDC,        %destination canvas
            {0,0},           %destination point
            {Width, Height}, %width/height of copy area
            MemoryDC2,       %source canvas
            {0,0}),          %source point
  Stop = now_milli(),
  Time = Stop - Start,
  Time.

draw_permanent(PermCanvas, PermDraws) ->
  lists:foreach(fun(Elem) -> ie_utils_draw:draw(PermCanvas, Elem) end, PermDraws).

redraw(#canvas{width = Width, height = Height,
               memoryDC2 = MemoryDC2, clientDC = ClientDC}) ->
  wxDC:blit(ClientDC,        %destination canvas
            {0,0},           %destination point
            {Width, Height}, %width/height of copy area
            MemoryDC2,       %source canvas
            {0,0}),          %source point
  ok. %% Must return ok (or at least anything that would interfere with the loop case statements)

get_draws(WorldState, Callback, #simple_canvas{name = ?DEFAULT_CANVAS} = Canvas) ->
  Callback(WorldState, [Canvas]);
get_draws(WorldState, Callback, #simple_canvas{name = Name} = Canvas) ->
  Callback(WorldState, [Canvas], Name).

%% Line
within(Canvas, #line{point1 = P1, point2 = P2}) ->
  #posn{x = Dx, y = Dy} = ?Difference(P2, P1),
  within(Canvas, #rectangle{top_left = P1, width = Dx, height = Dy});
%% Circle
within(Canvas, #circle{center = Center, radius = Radius}) ->
  within(Canvas, #rectangle{top_left = ?Translate(Center, -Radius, -Radius), width = Radius*2, height = Radius*2});
%% Rectangle
within(Canvas, #rectangle{top_left = TopLeft, width = Width, height = Height}) ->
  {#posn{x = P1x, y = P1y}, _, _, #posn{x = P2x, y = P2y}} = corners(Canvas),
  {#posn{x = P3x, y = P3y}, _, _, #posn{x = P4x, y = P4y}} = corners(TopLeft, Width, Height),
  not ((P2y < P3y) orelse (P1y > P4y) orelse (P2x < P3x) orelse (P1x > P4x));
%% Text
within(Canvas, #text{top_left = TopLeft, text = Text}) ->
  Width = ie_utils_draw:text_width(Text),
  Height = ie_utils_draw:text_height(Text),
  within(Canvas, #rectangle{top_left = TopLeft, width = Width, height = Height});
%% Image
within(Canvas, #image{top_left = TopLeft, path = Path, options = Options}) ->
  {Width, Height} = case lists:keyfind(resize, 1, Options) of
                      {resize, Size} -> Size;
                      false          -> {ie_utils_draw:image_width(Path),
                                         ie_utils_draw:image_height(Path)}
                    end,
  {MaxWidth, MaxHeight} = case lists:keyfind(rotate, 1, Options) of
                            false         -> {Width, Height};
                            {rotate, _}   -> Max = max(Width, Height),
                                             {Max, Max};
                            {rotate, _,_} -> Max = max(Width, Height),
                                             {Max, Max}
                          end,
  within(Canvas, #rectangle{top_left = TopLeft, width = MaxWidth, height = MaxHeight});
within(Canvas, #composite_canvas{top_left = TopLeft, canvas = Composite, options = Options}) ->
  {composite_size, Width, Height} = lists:last(Composite),
  {AdjWidth, AdjHeight} = case lists:keyfind(rotate, 1, Options) of
                            false         -> {Width, Height};
                            {rotate, _}   -> Max = max(Width, Height),
                                             {Max, Max};
                            {rotate, _,_} -> Max = max(Width, Height),
                                             {Max, Max}
                          end,
  within(Canvas, #rectangle{top_left = TopLeft, width = AdjWidth, height = AdjHeight}).



corners(#canvas{width = Width, height = Height}) ->
  corners(#posn{x = 0, y = 0}, Width, Height).
corners(TopLeft, Width, Height) ->
  TopRight = ?Translate(TopLeft, Width, 0),
  BottomLeft = ?Translate(TopLeft, 0, Height),
  BottomRight = ?Translate(TopLeft, Width, Height),
  {TopLeft, TopRight, BottomLeft, BottomRight}.

trim_draws(Draws) ->
  {lists:last(Draws),
   lists:reverse(lists:droplast(Draws))}.

%  case Icon of
%    undefined -> wxTopLevelWindow:setIcon(Scene, wxIcon:new(ie_utils_draw:curDir()++"/icons/world.bmp"));
%    _       -> wxTopLevelWindow:setIcon(Scene, wxIcon:new(Icon))
%  end,
