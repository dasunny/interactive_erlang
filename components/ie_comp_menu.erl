-module(ie_comp_menu).

-include("interactive_erlang.hrl").
-include("imenu.hrl").

-include_lib("wx/include/wx.hrl").

-behaviour(ie_component).

%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, stop/1, event/2]).

component_info() -> #component_info{iworld = #type_info{init_info = [ie_comp_draw]},
                                    iuniverse = #type_info{init_info = [ie_comp_terminal]}}.

describe(Options) ->
  Menu = case ?OptionGetValue(menu, Options) of
           undefined -> undefined;
           Callback1 -> {"menu", Callback1}
         end,
  Event = case ?OptionGetValue(menu_event, Options) of
            undefined -> undefined;
            Callback2 -> {"menu_event", Callback2}
          end,
  [Menu, Event].

init(#iState{options = Options} = IState) ->
  init(IState, ?OptionGetValue(menu, Options)).

init(_IState, undefined) -> undefined;
init(#iState{options = Options,
             type = Type,
             extra_data = ED}, Callback) ->
  Key = case Type of
          ?IWORLD    -> window;
          ?IUNIVERSE -> terminal_window
        end,
  Window = maps:get(Key, ED),
  CallBackFun = fun(#wx{id = ID}, _EventObject) ->
                        ?SendCallbackEvent(#callback_event{event_object = ID,
                                                           callback = ?OptionGetValue(menu_event, Options)}) end,
  wxFrame:hide(Window),
  MenuTree = Callback(),
  build_menu(MenuTree, Window),
  {Width, Height} = ?OptionGetValue3(size, Options, {500,500}),
  wxFrame:setClientSize(Window, Width, Height),
  wxFrame:connect(Window, command_menu_selected, [{callback, CallBackFun}]),
  wxFrame:show(Window),
  ok.

stop(IState) -> IState.

event(IState, #callback_event{callback = undefined}) -> IState;
event(#iState{state = State} = IState,
      #callback_event{event_object = ID,
                      callback = Callback}) ->
  IState#iState{state = ?Dispatch(Callback(State, ID))}.

%% ====================================================================
%% Internal functions
%% ====================================================================

build_menu(?MenuBar(Menus), Parent) ->
  MenuBar = wxMenuBar:new(),
  lists:foreach(fun(?Menu(_,_) = Elem) -> build_menus(Elem, MenuBar);
                   (_)                   -> {error, bad_menu_structure} end, Menus),
  wxFrame:setMenuBar(Parent, MenuBar),
  ok;
build_menu(_, _) -> {error, bad_menu_structure}.

build_menus(?Menu(Title, Items), Parent) ->
  Menu = wxMenu:new(),
  wxMenuBar:append(Parent, Menu, Title),
  lists:foreach(fun(Elem) -> build_menu_items(Elem, Menu) end, Items).

build_menu_items(?MenuSubMenuItem(Title, ID, Items), Parent) ->
  SubMenu = wxMenu:new(),
  Item = wxMenuItem:new([{id, id(ID)}, {text, Title}, {subMenu, SubMenu}]),
  lists:foreach(fun(Elem) -> build_menu_items(Elem, SubMenu) end, Items),
  wxMenu:append(Parent, Item);
build_menu_items(?MenuButtonItem(Title, ID), Parent) ->
  Item = wxMenuItem:new([{id, id(ID)}, {text, Title}]),
  wxMenu:append(Parent, Item);
build_menu_items(?MenuRadioItem(Title, ID), Parent) ->
  Item = wxMenuItem:new([{id, id(ID)}, {text, Title}, {kind, ?wxITEM_RADIO}]),
  wxMenu:append(Parent, Item);
build_menu_items(?MenuCheckItem(Title, ID), Parent) ->
  Item = wxMenuItem:new([{id, id(ID)}, {text, Title}, {kind, ?wxITEM_CHECK}]),
  wxMenu:append(Parent, Item);
build_menu_items(?MenuSeparatorItem, Parent) ->
  wxMenu:appendSeparator(Parent);
build_menu_items(_,_) ->
  ok.

id(?NoID) -> ?wxID_ANY;
id(Id)    -> Id.
