-module(ie_comp_mouse).

-include("interactive_erlang.hrl").
-include("idraw.hrl").
-include("imoderator.hrl").

-behaviour(ie_component).

-define(LIST, [left_down, left_up, right_down, right_up, motion]).

%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, stop/1, event/2]).

-record(mouse_event, {x,
                      y,
                      type,
                      canvas_name}).

component_info() -> #component_info{iworld = #type_info{init_info = [ie_comp_moderator, ie_comp_draw]},
                                    iuniverse = #type_info{init_info = undefined}}.

describe(Options) ->
  case ?OptionGetValue(mouse, Options) of
    undefined -> undefined;
    Callback  -> {"mouse", Callback}
  end.

init(#iState{options = Options, extra_data = #{canvases := Canvases}}) ->
  case ?OptionGetValue(mouse, Options) of
    undefined -> lists:foreach(fun null_bind/1, Canvases),
                 undefined;
    Callback  -> lists:foreach(fun(Elem) -> full_bind(Elem, Callback) end, Canvases),
                 ok
  end.

stop(IState) -> IState.

event(#iState{state = State} = IState,
      #callback_event{event_object = #mouse_event{x = X, y = Y, type = Type, canvas_name = Name},
                      callback = Callback}) ->
  NewState = case Name of
               ?DEFAULT_CANVAS -> Callback(State, X, Y, Type);
               _               -> Callback(State, X, Y, Type, Name)
             end,
  IState#iState{state = ?Dispatch(NewState)}.

%% ====================================================================
%% Internal functions
%% ====================================================================

null_bind(#canvas{panel = Parent}) ->
  CallBack = fun(_EventRecord, _Object) -> ok end,
  Fun = fun(Elem) -> wxEvtHandler:connect(Parent, Elem, [{callback, CallBack}]) end,
  lists:foreach(Fun, ?LIST).

full_bind(#canvas{name = Name, panel = Parent}, CallbackFun) ->
  Fun = fun(Elem) ->
                CallBack = fun(#wx{event = EventRecord}, _EventObject) ->
                                   ?SendCallbackEvent({{Name, Elem},
                                                       #callback_event{event_object = convert(EventRecord, Name),
                                                                       callback = CallbackFun}}) end,
                wxEvtHandler:connect(Parent, Elem, [{callback, CallBack}]) end,
  lists:foreach(Fun, ?LIST).

convert(#wxMouse{x = X, y = Y, type = Type}, Name) ->
  #mouse_event{x = X, y = Y, type = Type, canvas_name = Name}.
