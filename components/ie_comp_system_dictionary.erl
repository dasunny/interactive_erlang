-module(ie_comp_system_dictionary).

-include("interactive_erlang.hrl").

-behaviour(ie_component).

%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, stop/1, event/2]).
-export([set/2, get/2]).


component_info() -> #component_info{iworld = #type_info{init_info = ?INIT_FIRST},
                                    iuniverse = #type_info{init_info = ?INIT_FIRST}}.

describe(_Options) -> undefined.

init(#iState{extra_data = ExtraData} = IState) ->
  Tid = ets:new(?MODULE, [named_table, public, set]),
  {ok, IState#iState{extra_data = ExtraData#{system_ets => Tid}}}.

stop(#iState{extra_data = #{system_ets := Tid}} = IState) ->
  ?Unregister(?MODULE),
  ets:delete(Tid),
  IState.

event(IState, _CallbackEvent) ->
  IState.

set(Key, Value) ->
  ets:insert(?MODULE, {Key, Value}).

get(Key, Default) ->
  case ets:lookup(?MODULE, Key) of
    [{Key, Value}] -> Value;
    _              -> Default
  end.

%% ====================================================================
%% Internal functions
%% ====================================================================
