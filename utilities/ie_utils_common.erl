-module(ie_utils_common).

-include("interactive_erlang.hrl").

-record(result_dispatch, {result,
                          dispatch}).

%% ====================================================================
%% API functions
%% ====================================================================
-export([safe_send/2, is_parent_alive/0, flush/0, descriptor/1,
         wrap_dispatch/2, dispatch_callback_results/1]).

safe_send(To, Message) when is_atom(To) ->
  case whereis(To) of
    undefined -> ok;
    Pid       -> Pid ! Message
  end;
safe_send(To, Message) when is_pid(To) ->
  try
    To ! Message
  catch
    C:E -> io:format("Failed to safesend. ~p:~p\nTo:~p\nMessage:~p", [C, E, To, Message])
  end.

is_parent_alive() ->
  case whereis(?PARENT_PROCESS) of
    undefined -> false;
    Pid       -> is_process_alive(Pid)
  end.

flush() ->
  receive
    _ -> flush()
  after
    0 -> ok
  end.

descriptor(?IWORLD)    -> "IWorld";
descriptor(?IUNIVERSE) -> "IUniverse".

wrap_dispatch(Result, Dispatches) ->
  #result_dispatch{result = Result,
                   dispatch = Dispatches}.

dispatch_callback_results(#result_dispatch{result = Result, dispatch = Dispatch}) ->
  Fun = fun({?IE_DISPATCH, Dest, Message}) -> safe_send(Dest, Message) end,
  lists:foreach(Fun, Dispatch),
  Result;
dispatch_callback_results(Result) ->
  Result.

%% ====================================================================
%% Internal functions
%% ====================================================================
