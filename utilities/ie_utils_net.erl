-module(ie_utils_net).

-include("interactive_erlang.hrl").
-include("iworld.hrl").
-include("iuniverse.hrl").

-include_lib("wx/include/wx.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([make_package/2, make_bundle/3, server_chooser/0, my_ip/0, ping_universe/1]). %% Public
-export([decode_address/1, encode/1, decode/1, format_name/2, socket_opts/0]). %% Private
-export([connect_attempts/0, port/0, timeout/0]).

make_package(State, Message) ->
  ie_utils_common:wrap_dispatch(State, [{?IE_DISPATCH, ie_comp_net_world, {send, Message}}]).

make_bundle(State, Mail, Disconnects) ->
  WrappedMail = [{?IE_DISPATCH,
                  ie_comp_net_universe,
                  {send, Socket, Message}} || #mail{to      = #iworld{socket = Socket},
                                                    message = Message} <- Mail],
  WrappedDisconnects = [{?IE_DISPATCH,
                         ie_comp_net_universe,
                         {disconnect, Socket}} || #iworld{socket = Socket} <- Disconnects],
  ie_utils_common:wrap_dispatch(State, WrappedMail ++ WrappedDisconnects).

format_name(undefined, Socket) ->
  {ok, {{A,B,C,D}, _Port}} = inet:peername(Socket),
  io_lib:format("~p.~p.~p.~p", [A,B,C,D]);
format_name(Name, _) -> Name.

decode_address(Address) ->
  try
    decode_address2(Address)
  catch
    _:E -> io:format("Invalid address! Defaulting to ~p\nAddress: ~p\nError: ~p\n", ["127.0.0.1", Address, E]),
           decode_address("127.0.0.1")
  end.

decode_address2(localhost) ->
  decode_address2("localhost");
decode_address2("localhost") ->
  {ok, Address} = inet:getaddr("localhost", inet),
  Address;
decode_address2(Address) when is_tuple(Address), size(Address) == 4 ->
  Address;
decode_address2(Address) when is_list(Address) ->
  {ok, Decoded} = inet:parse_address(Address),
  Decoded.

my_ip() ->
  {ok, Host}     = inet:gethostname(),
  {ok, Address} = inet:getaddr(Host, inet),
  Address.

ping_universe(Address) ->
  DecodedAddress = decode_address(Address),
  case connect(DecodedAddress, connect_attempts()) of
    {ok, Socket} -> Result = send_ping(Socket),
                    gen_tcp:close(Socket),
                    Result;
    Error        -> {error, Error}
  end.

connect(_Address, 0) ->
  {error, timeout};
connect(Address, N) ->
  case gen_tcp:connect(Address, port(), socket_opts(), timeout()) of
    {ok, Socket}     -> {ok, Socket};
    {error, timeout} -> connect(Address, N - 1);
    {error, Reason}  -> {error, {connect_error, Reason}};
    {tcp_error, _Socket, Error} -> {tcp_error, Error}
  end.

send_ping(Socket) ->
  case gen_tcp:send(Socket, <<"ping">>) of
    ok              -> get_response(10);
    {error, Reason} -> {error, {send_error, Reason}};
    {tcp_error, _Socket, Error} -> {error, {tcp_error, Error}}
  end.

get_response(0) -> {error, no_response};
get_response(N) ->
  receive
    {tcp, _Connection, <<"pong">>} -> ok;
    _                              -> get_response(N)
  after
      1000 -> get_response(N-1)
  end.

encode(Term) ->
  try
    term_to_binary(Term)
  catch
    _:_ -> term_to_binary(encode_error)
  end.

decode(Binary) ->
  try
    binary_to_term(Binary)
  catch
    _:_ -> decode_error
  end.

socket_opts() -> [binary, {packet, 4}].
port() -> 8321.
timeout() -> 5000.
connect_attempts() -> 3.


server_chooser() ->
  Wx = wx:new(),
  Red    = make_bitmap({255,  0,0}),
  Yellow = make_bitmap({255,255,0}),
  Green  = make_bitmap({0,  255,0}),

  Frame = wxFrame:new(Wx, ?wxID_ANY, "Server Chooser", [{style, ?wxSYSTEM_MENU
                                                           bor ?wxCAPTION
                                                           bor ?wxCLOSE_BOX}]),
  %wxFrame:setClientSize(Frame, 250, 150),
  Panel = wxPanel:new(Frame),
  MainSizer   = wxBoxSizer:new(?wxVERTICAL),
  ServerSizer = wxStaticBoxSizer:new(?wxHORIZONTAL, Panel, [{label, "Server Address"}]),
  NameSizer   = wxStaticBoxSizer:new(?wxHORIZONTAL, Panel, [{label, "Name"}]),
  ButtonSizer = wxBoxSizer:new(?wxHORIZONTAL),
  Server  = wxTextCtrl:new(Panel, ?wxID_ANY, [{value, "localhost"}]),
  Name    = wxTextCtrl:new(Panel, ?wxID_ANY, [{value, "Player"}]),
  Check   = wxBitmapButton:new(Panel, ?wxID_ANY, Yellow),
  Connect = wxButton:new(Panel, ?wxID_ANY, [{label, "Connect"}]),
  Cancel  = wxButton:new(Panel, ?wxID_ANY, [{label, "Cancel"}]),

  wxButton:disable(Connect),

  wxPanel:setSizer(Panel, MainSizer),

  wxSizer:add(ServerSizer, Server,      [{flag, ?wxEXPAND}, {proportion, 1}]),
  wxSizer:add(ServerSizer, Check,       [{flag, ?wxALIGN_RIGHT}]),
  wxSizer:add(NameSizer,   Name,        [{flag, ?wxEXPAND}, {proportion, 1}]),
  wxSizer:add(ButtonSizer, Connect,     [{flag, ?wxALIGN_RIGHT}]),
  wxSizer:add(ButtonSizer, Cancel,      [{flag, ?wxALIGN_RIGHT}]),
  wxSizer:add(MainSizer,   ServerSizer, [{flag, ?wxEXPAND}, {proportion, 1}]),
  wxSizer:add(MainSizer,   NameSizer,   [{flag, ?wxEXPAND}, {proportion, 1}]),
  wxSizer:add(MainSizer,   ButtonSizer, [{flag, ?wxALIGN_RIGHT}]),

  Path = ".last",
  case file:read_file(Path) of
    {ok, Binary} -> case binary_to_term(Binary) of
                      {ServerV, NameV} -> wxTextCtrl:setValue(Server, ServerV),
                                          wxTextCtrl:setValue(Name, NameV);
                      _                -> ok
                    end;
    _            -> ok
  end,

  {_, MinY} = wxSizer:getMinSize(MainSizer),
  wxFrame:setClientSize(Frame, 250, MinY),

  Self = self(),
  CB1 = fun(_,_) -> case ping_universe(wxTextCtrl:getValue(Server)) of
                      ok -> wxBitmapButton:setBitmapLabel(Check, Green),
                            wxButton:enable(Connect);
                      _  -> wxBitmapButton:setBitmapLabel(Check, Red),
                            wxButton:disable(Connect)
                    end end,
  CB2 = fun(_,_) -> Self ! error,
                    wxFrame:hide(Frame),
                    wxFrame:destroy(Frame) end,
  CB3 = fun(_,_) -> Self ! {wxTextCtrl:getValue(Server), wxTextCtrl:getValue(Name)},
                    wxFrame:hide(Frame),
                    wxFrame:destroy(Frame) end,

  wxButton:connect(Check, command_button_clicked, [{callback, CB1}]),
  wxButton:connect(Cancel, command_button_clicked, [{callback, CB2}]),
  wxFrame:connect(Frame, close_window, [{callback, CB2}]),
  wxButton:connect(Connect, command_button_clicked, [{callback, CB3}]),

  wxFrame:show(Frame),

  receive
    error -> error;
    Any   -> file:write_file(Path, term_to_binary(Any)), Any
  end.

make_bitmap(Color) ->
  {W, H} = wxButton:getDefaultSize(),
  Bitmap = wxBitmap:new(W-10, H-10),
  DC = wxMemoryDC:new(),
  wxMemoryDC:selectObject(DC, Bitmap),
  wxDC:setBackground(DC, wxBrush:new(Color)),
  wxDC:clear(DC),
  wxMemoryDC:destroy(DC),
  Bitmap.
