-module(ie_utils_menu).

-include("interactive_erlang.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([add_status_bar/1, change_status_bar/1, revert_status_bar/0]). %Status bar related
-export([spawn_dialog_box/1]). %Menubar related

%Adds a Status Bar with the supplied text to the supplied Window Object.
add_status_bar(Text) ->
  Frame = get_frame(),
  wxFrame:createStatusBar(Frame),
  wxFrame:setStatusText(Frame, fmt_text(Text)).

%Edits the status bar of the window object to the new text.
change_status_bar(Text) ->
  Frame = get_frame(),
  SB = wxFrame:getStatusBar(Frame),
  wxStatusBar:pushStatusText(SB, fmt_text(Text)).

%Resets the status bar text to it's default.
revert_status_bar() ->
  Frame = get_frame(),
  SB = wxFrame:getStatusBar(Frame),
  wxStatusBar:popStatusText(SB).

%% %Adds a Menu Bar to the supplied Window Object.
%% add_menu() ->
%%   Window = wxWindow:findWindowById(1),
%%   Temp = wxMenuBar:new(),
%%   wxFrame:setMenuBar(Window, Temp),
%%   Temp.
%% 
%% %Adds an Item to the supplied Menu Bar.
%% add_menu_item(Menu, Name) ->
%%   Temp = wxMenu:new(),
%%   wxMenuBar:append(Menu, Temp, Name),
%%   Temp.
%% 
%% %Adds a subitem to the supplied Menu Item.
%% add_sub_menu_item(Menu, Name, ID) ->
%%   Temp = wxMenuItem:new([{id,ID}, {text,Name}]),
%%   wxMenu:append(Menu, Temp),
%%   Temp.

%Generates a dialog box with the supplied text.
spawn_dialog_box(Text) ->
  Window = wxWindow:findWindowById(1),
  Temp = wxMessageDialog:new(Window, fmt_text(Text)),
  wxMessageDialog:showModal(Temp).

get_frame() ->
  Window = wxWindow:findWindowById(1),
  wx:typeCast(Window, wxFrame).

fmt_text({Fmt, Args}) ->
  io_lib:format(Fmt, Args);
fmt_text(Text) -> Text.
