-module(ie_utils_draw).

-include("interactive_erlang.hrl").
-include("idraw.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([new_canvas/2]). %Canvas related
-export([draw/2]). %Generic drawing related
-export([current_directory/0, image_width/1, image_height/1,
         preload_image/1, text_width/1, text_height/1, make_saved_image/1, make_saved_image/2]). %Utility related

-record(image_opts, {resize,
                     mask,
                     rotate_angle_rad,
                     rotate_angle_deg,
                     rotate_point,
                     opacity,
                     save = true,
                     no_op = true}).

-define(Pen,    wxPen).
-define(Brush,  wxBrush).
-define(Bitmap(ImageOptions), {wxBitmap, ImageOptions}).
-define(Bitmap, ?Bitmap(#image_opts{})).
-define(CompositeCanvas(ImageOptions), {composite, ImageOptions}).
-define(CompositeCanvas, ?CompositeCanvas(undefined)).

-define(P2T(Posn), {trunc(Posn#posn.x), trunc(Posn#posn.y)}).
-define(C2T(Color), {Color#color.r, Color#color.g, Color#color.b}).

-define(IsDef(Term), Term /= undefined).

new_canvas(Width, Height) ->
  Frame = wxFrame:new(wx:new(), -1, "Canvas", [{size, {Width, Height}}]),
  wxWindow:setClientSize(Frame, Width, Height),
  Canvas = wxPanel:new(Frame, 0, 0, Width, Height),
  DC = wxWindowDC:new(Canvas),
  wxFrame:show(Frame),
  DC.

draw(DC, #line{point1 = Point1, point2 = Point2,
               color = Color, thickness = Thickness}) ->
  Pen = get(?Pen, Color),
  wxPen:setWidth(Pen, Thickness),
  wxDC:setPen(DC, Pen),
  wxDC:drawLine(DC,?P2T(Point1), ?P2T(Point2)),
  wxPen:setWidth(Pen, 1);

draw(DC, #circle{center = Center, radius = Radius, color = Color}) ->
  Pen = get(?Pen, ?BLACK),
  wxDC:setPen(DC, Pen),
  Brush = get(?Brush, Color),
  wxDC:setBrush(DC, Brush),
  wxDC:drawCircle(DC, ?P2T(Center), Radius);

draw(DC, #rectangle{top_left = ?Posn(X, Y), width = Width, height = Height, color = Color}) ->
  Pen = get(?Pen, ?BLACK),
  wxDC:setPen(DC, Pen),
  Brush = get(?Brush, Color),
  wxDC:setBrush(DC, Brush),
  wxDC:drawRectangle(DC, {trunc(X), trunc(Y), Width, Height});

draw(DC, #text{top_left = TopLeft, text = Text, color = Color}) ->
  wxDC:setTextForeground(DC, ?C2T(Color)),
  wxDC:drawText(DC, fmt_text(Text), ?P2T(TopLeft));

draw(DC, #image{top_left = TopLeft, path = Path, options = Options}) ->
  ImageOptions = parse_bitmap_options(Options),
  Bitmap = get(?Bitmap(ImageOptions), Path),
  draw_bitmap(DC, Bitmap, TopLeft, {image_width(Path), image_height(Path)}, ImageOptions),
  ImageOptions#image_opts.save orelse wxBitmap:destroy(Bitmap),
  ok;

draw(DC, #composite_canvas{top_left = TopLeft, canvas = Canvas, options = Options}) ->
  ImageOptions = parse_bitmap_options(Options),
  Bitmap = get(?CompositeCanvas(ImageOptions), Canvas),

  {composite_size, Width, Height} = lists:last(Canvas),

  draw_bitmap(DC, Bitmap, TopLeft, {Width, Height}, ImageOptions),
  ImageOptions#image_opts.save orelse wxBitmap:destroy(Bitmap),
  ok.

%Returns the image width dimension.
image_width(#saved_image{width = Width}) ->
  Width;
image_width(Path) ->
  Bitmap = get(?Bitmap, Path),
  wxBitmap:getWidth(Bitmap).

%Returns the image height dimension.
image_height(#saved_image{height = Height}) ->
  Height;
image_height(Path) ->
  Bitmap = get(?Bitmap, Path),
  wxBitmap:getHeight(Bitmap).

preload_image(Path) ->
  get(?Bitmap, Path),
  ok.

%Returns the width of the string.
text_width(Text) ->
  DC = get(canvas),
  {Width, _Height} = wxDC:getTextExtent(DC, fmt_text(Text)),
  Width.

%Returns the height of the string.
text_height(Text) ->
  DC = get(canvas),
  {_Width, Height} = wxDC:getTextExtent(DC, fmt_text(Text)),
  Height.

%Returns the directory of the .beam file being executed
current_directory() ->
  hd(code:get_path()).

make_saved_image(ImagePath) ->
  {_, Image} = make_image(ImagePath),
  #saved_image{path = ImagePath,
               width = wxImage:getWidth(Image),
               height = wxImage:getHeight(Image),
               data = wxImage:getData(Image)}.
make_saved_image(Name, Image) ->
  #saved_image{path = Name,
               width = wxImage:getWidth(Image),
               height = wxImage:getHeight(Image),
               data = wxImage:getData(Image)}.

fmt_text({Fmt, Args}) ->
  io_lib:format(Fmt, Args);
fmt_text(Text) -> Text.

%% ====================================================================
%% Internal functions
%% ====================================================================

parse_bitmap_options(Options) ->
  Size = case lists:keyfind(resize, 1, Options) of
           {resize, {Sx, Sy}} -> {max(1, Sx), max(1, Sy)};
           false      -> undefined
         end,
  MaskColor = case lists:keyfind(mask, 1, Options) of
                {mask, Color} -> Color;
                false         -> undefined
              end,
  {AngleRad, AngleDeg, RotatePoint} = case lists:keyfind(rotate, 1, Options) of
                                        {rotate, Angle, Cent} -> {Angle*math:pi()/180, Angle, Cent};
                                        {rotate, Angle}       -> {Angle*math:pi()/180, Angle, ?Posn(0,0)};
                                        _                     -> {undefined, undefined, undefined}
                                      end,
  Opacity = case lists:keyfind(opacity, 1, Options) of
                 {opacity, Percent} -> round(Percent*255);
                 false             -> 255
               end,
  Save = case lists:keyfind(save, 1, Options) of
           {save, Sa} -> Sa;
           false -> true
         end,
  #image_opts{resize           = Size,
              mask             = MaskColor,
              rotate_angle_rad = AngleRad,
              rotate_angle_deg = AngleDeg,
              rotate_point     = RotatePoint,
              opacity          = Opacity,
              save             = Save,
              no_op            = false}.

draw_bitmap(DC, Bitmap, TopLeft, BitmapSize, ImageOptions) ->
  #image_opts{mask             = MaskColor,
              rotate_angle_rad = AngleRad,
              rotate_point     = RotatePoint} = ImageOptions,
  OldOffset = case AngleRad of
                undefined -> undefined;
                _Angle    -> {BitmapWidth, BitmapHeight} = BitmapSize,
                             ?Posn(Dx, Dy) = ?Difference(RotatePoint, ?Posn(BitmapWidth div 2,
                                                                            BitmapHeight div 2)),
                             ?Posn(Dy*math:sin(AngleRad) + Dx*math:cos(AngleRad),
                                   Dy*math:cos(AngleRad) - Dx*math:sin(AngleRad))
              end,
  % Bitmap = get(?Bitmap(ImageOptions), Path),
  DrawPoint = case OldOffset of
                undefined -> TopLeft;
                _Offset   -> NewWidthCenter  = wxBitmap:getWidth(Bitmap) div 2,
                             NewHeightCenter = wxBitmap:getHeight(Bitmap) div 2,
                             NewOffset = ?Translate(?Posn(NewWidthCenter,
                                                          NewHeightCenter), OldOffset),
                             ?Difference(TopLeft, NewOffset)
              end,
  wxDC:drawBitmap(DC, Bitmap, ?P2T(DrawPoint), [{useMask, ?IsDef(MaskColor)}]),
  ok.

make(Type, Color) when Type == ?Pen; Type == ?Brush ->
  Item = Type:new(?C2T(Color)),
  put({Type, Color}, Item),
  Item;
make(?Bitmap(ImageOptions), Input) ->
  {Path, Image} = make_image(Input),

  FinalImage = manipulate_image(Image, ImageOptions),

  Bitmap = convert_and_delete(FinalImage),
  ImageOptions#image_opts.save andalso put({?Bitmap(ImageOptions), Path}, Bitmap),
  Bitmap;
make(?CompositeCanvas, Canvas) ->
  {{composite_size, Width, Height}, Draws} = {lists:last(Canvas),
                                              lists:reverse(lists:droplast(Canvas))},
  EmptyBinary = << <<255, 255, 255>> || _ <- lists:seq(1, Width*Height) >>,
  EmptyImage = wxImage:new(Width, Height, EmptyBinary),
  Bitmap = wxBitmap:new(EmptyImage),
  wxImage:destroy(EmptyImage),
  MemoryDC = wxMemoryDC:new(Bitmap),
  lists:foreach(fun(Draw) -> draw(MemoryDC, Draw) end, Draws),
  put({?CompositeCanvas, Canvas}, Bitmap),
  Bitmap;
make(?CompositeCanvas(ImageOptions), Canvas) ->
  Bitmap = get(?CompositeCanvas, Canvas),

  AsImage = wxBitmap:convertToImage(Bitmap),
  FinalImage = manipulate_image(AsImage, ImageOptions),
  FinalBitmap = convert_and_delete(FinalImage),

  ImageOptions#image_opts.save andalso put({?CompositeCanvas(ImageOptions), Canvas}, FinalBitmap),
  FinalBitmap.

%% get(Type, #saved_image{path = Path} = Saved) -> get(Type, {Path, Saved});
get(Type, Key) ->
  case get({Type, Key}) of
    undefined -> make(Type, Key);
    Item      -> Item
  end.

make_image(#saved_image{path   = Path,
                        width  = Width,
                        height = Height,
                        data   = Data}) ->
  {Path, wxImage:new(Width, Height, Data)};
make_image(Path) ->
  {Path, wxImage:new(Path)}.

manipulate_image(Image, #image_opts{no_op = true}) ->
  Image;
manipulate_image(Image, #image_opts{resize           = Resize,
                                    mask             = MaskColor,
                                    rotate_angle_rad = RotAngle,
                                    opacity          = Opacity} = ImageOptions) ->
  ScaledImage = case Resize of
                  undefined   -> Image;
                  {NewWidth,
                   NewHeight} -> ScaImage = wxImage:scale(Image, NewWidth, NewHeight),
                                 wxImage:destroy(Image),
                                 ScaImage
                end,
  RotatedImage = case RotAngle of
                   undefined -> ScaledImage;
                   _AngleRad -> case MaskColor of
                                  undefined -> ok;
                                  ?COLOR(R,G,B) -> wxImage:setMaskColour(ScaledImage, R, G, B)
                                end,
%%                                 set_alpha(ScaledImage, MaskColor, Opacity),
                                rotate_image(ScaledImage, ImageOptions)
                 end,
  set_alpha(RotatedImage, MaskColor, Opacity),
  RotatedImage.

convert_and_delete(Image) ->
  Bitmap = wxBitmap:new(Image),
  wxImage:destroy(Image),
  Bitmap.

rotate_image(Image, #image_opts{rotate_angle_deg = undefined}) -> Image;
rotate_image(Image, #image_opts{rotate_angle_rad = AngleRad,
                                rotate_angle_deg = AngleDeg,
                                mask = Mask}) ->
  Rotate90 = fun(ToRotate) -> New = wxImage:rotate90(ToRotate),
                              wxImage:destroy(ToRotate),
                              New end,
  MaskImage = fun(Im) -> {R,G,B} = case Mask of
                                     undefined -> {255,255,255};
                                     Color -> ?C2T(Color)
                                   end,
                         wxImage:setMaskColour(Im,R,G,B),
                         wxImage:setMask(Im, [{mask, true}])
              end,
  case AngleDeg rem 360 of
    0   -> Image;
    90  -> Rotate90(Rotate90(Rotate90(Image)));
    180 -> Rotate90(Rotate90(Image));
    270 -> Rotate90(Image);
    _   -> MaskImage(Image),
           New = wxImage:rotate(Image, AngleRad, {0,0}, [{interpolating, true}]),
           wxImage:destroy(Image),
           New
  end.

set_alpha(_Image, undefined, 255) -> ok;
set_alpha(_Image, undefined, undefined) -> ok;
set_alpha(Image, undefined, Opacity) ->
  wxImage:hasAlpha(Image) orelse wxImage:initAlpha(Image),
  Data = wxImage:getData(Image),
  AlphaData = << <<(Opacity)>> || _ <- lists:seq(1, byte_size(Data) div 3) >>,
  wxImage:setAlpha(Image, AlphaData);
set_alpha(Image, ?COLOR(MaskRed, MaskGreen, MaskBlue), Opacity) ->
  wxImage:hasAlpha(Image) orelse wxImage:initAlpha(Image),
  Data = wxImage:getData(Image),
  NoiseMargin = 10,
  Fun = fun(Red, Green, Blue) when Red   == MaskRed
                           andalso Green == MaskGreen
                           andalso Blue  == MaskBlue -> 0;
%%            (Color, Color, Color) when Color > 223 -> Opacity - Color;
           (Color, Color, Color) when Color > 223 -> 0;
           (Red, Green, Blue) when abs(Red - MaskRed)     < NoiseMargin
                           andalso abs(Green - MaskGreen) < NoiseMargin
                           andalso abs(Blue - MaskBlue)   < NoiseMargin -> 0;
           (_Red, _Green, _Blue) -> Opacity end,
  AlphaData = << <<(Fun(X,Y,Z))>> || <<X, Y, Z>> <= Data >>,
  wxImage:setAlpha(Image, AlphaData).


