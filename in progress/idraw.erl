%% @author Danny
%% @doc Written with the goal to simplify the creation and managment of wx windows and frames.


-module(idraw).

%% ====================================================================
%% API functions
%% ====================================================================
-export([newCanvas/2]). %Canvas related
-export([addStatusBar/2, changeStatusBar/2, revertStatusBar/1]). %Status bar related
-export([addMenu/1, addMenuItem/2, addSubMenuItem/3, spawnDialogBox/2]). %Menubar related
-export([drawLine/5, drawRect/5, drawCircle/4, drawText/3, drawText/4]). %Generic drawing related
-export([drawLinePerm/4, drawRectPerm/4, drawCirclePerm/3, drawTextPerm/3]). %Permanent generic drawing related
-export([drawLinePerm/5, drawRectPerm/5, drawCirclePerm/4, drawTextPerm/4]).
-export([drawImage/3, drawScaledImage/5, drawTransImage/4]). %Image drawing related
-export([drawImagePerm/2, drawScaledImagePerm/4, drawTransImagePerm/3]). %Permanent image drawing related
-export([drawImagePerm/3, drawScaledImagePerm/5, drawTransImagePerm/4]).
-export([imageWidth/1, imageHeight/1, textWidth/2, textHeight/2]). %Utility related
-export([importImage/1, curDir/0, resetCanvas/0, resetCanvas/1]). %More utility related

-include_lib("wx/include/wx.hrl").

newCanvas(W, H) ->
  Frame = wxFrame:new(wx:new(), -1, "Canvas", [{size, {W, H}}]),
  wxWindow:setClientSize(Frame, W, H),
  Canvas = wxPanel:new(Frame, 0, 0, W, H),
  DC = wxWindowDC:new(Canvas),
  wxFrame:show(Frame),
  DC.

%Adds a Status Bar with the supplied text to the supplied Window Object.
addStatusBar(Window, Text) ->
  Temp = wxFrame:createStatusBar(Window),
  wxFrame:setStatusText(Window, Text),
  Temp.

%Edits the status bar of the window object to the new text.
changeStatusBar(Window, Text) ->
  SB = wxFrame:getStatusBar(Window),
  wxStatusBar:pushStatusText(SB, Text).

%Resets the status bar text to it's default.
revertStatusBar(Window) ->
  SB = wxFrame:getStatusBar(Window),
  wxStatusBar:popStatusText(SB).

%Adds a Menu Bar to the supplied Window Object.
addMenu(Window) ->
  Temp = wxMenuBar:new(),
  wxFrame:setMenuBar(Window, Temp),
  Temp.

%Adds an Item to the supplied Menu Bar.
addMenuItem(Menu, Name) ->
  Temp = wxMenu:new(),
  wxMenuBar:append(Menu, Temp, Name),
  Temp.

%Adds a subitem to the supplied Menu Item.
addSubMenuItem(Menu, Name, ID) ->
  Temp = wxMenuItem:new([{id,ID},{text,Name}]),
  wxMenu:append(Menu, Temp),
  Temp.

%Generates a dialog box with the supplied text.
spawnDialogBox(Window, Text) ->
  Temp = wxMessageDialog:new(Window, Text),
  wxMessageDialog:showModal(Temp).

getPermDC(Name) ->
  element(2, lists:nth(1, ets:lookup(permcanvastable, Name))).

resetCanvas() -> resetCanvas(undefined).
resetCanvas(Name) -> 
  DC = getPermDC(Name),
  wxDC:clear(DC),
  ok.

%Draws a line on the provided canvas.
%Canvas, {Point x1, point y1}, {Point x2, point y2}, {R, G, B, [A]}, Thickness -> Canvas. 
drawLine(Canvas, Point1, Point2, Color, Thick) ->
  drawLineInt(Canvas, Point1, Point2, Color, Thick).

%Draws a line permanently on the provided canvas.
%{Point x1, point y1}, {Point x2, point y2}, {R, G, B, [A]}, Thickness -> ok. 
drawLinePerm(Point1, Point2, Color, Thick) ->
  Canvas = getPermDC(undefined),
  drawLineInt(Canvas, Point1, Point2, Color, Thick).

%Draws a line permanently on the provided canvas.
%Use this if the 'pane' option was invoked during setup.
%Pane name, {Point x1, point y1}, {Point x2, point y2}, {R, G, B, [A]}, Thickness -> ok. 
drawLinePerm(Name, Point1, Point2, Color, Thick) ->
  Canvas = getPermDC(Name),
  drawLineInt(Canvas, Point1, Point2, Color, Thick).

drawLineInt(Canvas, {X1,Y1}, {X2,Y2}, Color, Thick) ->
  {Pen, Destroy} = getColor(Color, pen),
  OldWidth = wxPen:getWidth(Pen),
  wxPen:setWidth(Pen, Thick),
  wxDC:setPen(Canvas, Pen),
  wxDC:drawLine(Canvas,{X1,Y1}, {X2,Y2}),
  wxPen:setWidth(Pen, OldWidth),
  destroy(pen, Pen, Destroy),
  Canvas.

%Draws a circle on the provided canvas. The provided point will be the center of the circle.
%Canvas, {Point x, point y}, radius, {R, G, B, [A]} -> Canvas.
drawCircle(Canvas, Point, Rad, Color) ->
  drawCircleInt(Canvas, Point, Rad, Color).

%Draws a circle permanently on the provided canvas. The provided point will be the center of the circle.
%Use this if the 'pane' option was invoked during setup.
%{Point x, point y}, radius, {R, G, B, [A]} -> ok.
drawCirclePerm(Point, Rad, Color) ->
  Canvas = getPermDC(undefined),
  drawCircleInt(Canvas, Point, Rad, Color).

%Draws a circle permanently on the provided canvas. The provided point will be the center of the circle.
%Pane name, {Point x, point y}, radius, {R, G, B, [A]} -> ok.
drawCirclePerm(Name, Point, Rad, Color) ->
  Canvas = getPermDC(Name),
  drawCircleInt(Canvas, Point, Rad, Color).

drawCircleInt(Canvas, {X,Y}, Rad, Color) ->
  {Brush, Destroy} = getColor(Color, brush),
  wxDC:setBrush(Canvas, Brush),
  wxDC:drawCircle(Canvas, {X,Y}, Rad),
  destroy(brush, Brush, Destroy),
  Canvas.

%Draws a rectangle on the provided canvas. The provided point will be the top-left of the rectangle.
%Canvas, {Point x, point y}, width, height, {R, G, B, [A]} -> Canvas.
drawRect(Canvas, Point, Width, Height, Color) ->
  drawRectInt(Canvas, Point, Width, Height, Color).

%Draws a rectangle permanently on the provided canvas. The provided point will be the top-left of the rectangle.
%{Point x, point y}, width, height, {R, G, B, [A]} -> ok.
drawRectPerm(Point, Width, Height, Color) ->
  Canvas = getPermDC(undefined),
  drawRectInt(Canvas, Point, Width, Height, Color).

%Draws a rectangle permanently on the provided canvas. The provided point will be the top-left of the rectangle.
%Use this if the 'pane' option was invoked during setup.
%Pane name, {Point x, point y}, width, height, {R, G, B, [A]} -> ok.
drawRectPerm(Name, Point, Width, Height, Color) ->
  Canvas = getPermDC(Name),
  drawRectInt(Canvas, Point, Width, Height, Color).

drawRectInt(Canvas, {X,Y}, Width, Height, Color) ->
  {Brush, Destroy} = getColor(Color, brush),
  wxDC:setBrush(Canvas, Brush),
  wxDC:drawRectangle(Canvas, {X, Y, Width, Height}),
  destroy(brush, Brush, Destroy),
  Canvas.

drawText(Canvas, Point, Text) -> drawText(Canvas, Point, Text, black). %TODO LEGACY
%Draws a line of text on the provided canvas. The provided point will be the top-left of the text.
%Canvas, {Point x, point y}, Text, Color -> Canvas.
drawText(Canvas, Point, Text, Color) ->
  drawTextInt(Canvas, Point, Text, Color).

%Draws a line of text permanently on the provided canvas. The provided point will be the top-left of the text.
%Use this if the 'pane' option was invoked during setup.
%{Point x, point y}, Text, Color -> ok.
drawTextPerm(Point, Text, Color) ->
  Canvas = getPermDC(undefined),
  drawTextInt(Canvas, Point, Text, Color).

%Draws a line of text permanently on the provided canvas. The provided point will be the top-left of the text.
%Pane name, {Point x, point y}, Text, Color -> ok.
drawTextPerm(Name, Point, Text, Color) ->
  Canvas = getPermDC(Name),
  drawTextInt(Canvas, Point, Text, Color).

drawTextInt(Canvas, {X,Y}, Text, Color) ->
  {Pen, Destroy} = getColor(Color, pen),
  wxPen:setWidth(Pen, 1),
  wxDC:setPen(Canvas, Pen),
  wxDC:drawText(Canvas,Text, {X,Y}),
  destroy(pen, Pen, Destroy),
  Canvas.

%Draws an image on the provided canvas.
%Canvas, {Point x, point y}, [Image path(string) | Bitmap()] -> Canvas.
drawImage(Canvas, Point, Path) ->
  drawImageInt(Canvas, Point, Path).

%Draws an image permanently on the provided canvas.
%{Point x, point y}, [Image path(string) | Bitmap()] -> ok.
drawImagePerm(Point, Path) ->
  Canvas = getPermDC(undefined),
  drawImageInt(Canvas, Point, Path).

%Draws an image permanently on the provided canvas.
%Use this if the 'pane' option was invoked during setup.
%Pane name, {Point x, point y}, [Image path(string) | Bitmap()] -> ok.
drawImagePerm(Name, Point, Path) ->
  Canvas = getPermDC(Name),
  drawImageInt(Canvas, Point, Path).

drawImageInt(Canvas, {X,Y}, Path) when is_list(Path)->
  Bitmap = getBitmap(Path),
  wxDC:drawBitmap(Canvas, Bitmap, {X,Y}),
  Canvas;

drawImageInt(Canvas, {X,Y}, ImageBM) ->
  wxDC:drawBitmap(Canvas, ImageBM, {X,Y}),
  Canvas.

%Draws a scaled image on the provided canvas.
%Canvas, {Point x, point y}, Image path -> Canvas.
drawScaledImage(Canvas, Point, Path, XScale, YScale) ->
  drawScaledImageInt(Canvas, Point, Path, XScale, YScale).

%Draws a scaled image permanently on the provided canvas.
%{Point x, point y}, Image path -> ok.
drawScaledImagePerm(Point, Path, XScale, YScale) ->
  Canvas = getPermDC(undefined),
  drawScaledImageInt(Canvas, Point, Path, XScale, YScale).

%Draws a scaled image permanently on the provided canvas.
%Use this if the 'pane' option was invoked during setup.
%Pane name, {Point x, point y}, Image path -> ok.
drawScaledImagePerm(Name, Point, Path, XScale, YScale) ->
  Canvas = getPermDC(Name),
  drawScaledImageInt(Canvas, Point, Path, XScale, YScale).

drawScaledImageInt(Canvas, {X,Y}, Path, XScale, YScale) ->
  Image = getImage(Path),
  ScaledImage = wxImage:scale(Image, wxImage:getWidth(Image)*XScale, wxImage:getHeight(Image)*YScale),
  Bitmap = wxBitmap:new(ScaledImage),
  wxImage:destroy(ScaledImage),
  wxDC:drawBitmap(Canvas, Bitmap, {X,Y}),
  wxBitmap:destroy(Bitmap),
  Canvas.

%Draws an image on the provided canvas.
%Canvas, {Point x, point y}, {Mask R, Mask G, Mask B}, Image path -> Canvas.
drawTransImage(Canvas, Point, MaskColor, Path) ->
  drawTransImageInt(Canvas, Point, MaskColor, Path).

%Draws an image permanently on the provided canvas.
%{Point x, point y}, {Mask R, Mask G, Mask B}, Image path -> ok.
drawTransImagePerm(Point, MaskColor, Path) ->
  Canvas = getPermDC(undefined),
  drawTransImageInt(Canvas, Point, MaskColor, Path).

%Draws an image permanently on the provided canvas.
%Use this if the 'pane' option was invoked during setup.
%Pane name, {Point x, point y}, {Mask R, Mask G, Mask B}, Image path -> ok.
drawTransImagePerm(Name, Point, MaskColor, Path) ->
  Canvas = getPermDC(Name),
  drawTransImageInt(Canvas, Point, MaskColor, Path).

drawTransImageInt(Canvas, {X,Y}, MaskColor, Path) ->
  Image = getImage(Path),
  Bitmap = wxBitmap:new(Image),
  Mask = wxMask:new(Bitmap, MaskColor),
  wxBitmap:setMask(Bitmap, Mask),
  wxDC:drawBitmap(Canvas, Bitmap, {X,Y}, [{useMask, true}]),
  wxBitmap:destroy(Bitmap),
  Canvas.

%Returns the image width dimension.
imageWidth(Path) ->
  Image = getImage(Path),
  X = wxImage:getWidth(Image),
  X.

%Returns the image height dimension.
imageHeight(Path) ->
  Image = getImage(Path),
  Y = wxImage:getHeight(Image),
  Y.

%Returns the width of the string.
textWidth(Canvas, Text) ->
  {W, _} = wxDC:getTextExtent(Canvas, Text),
  W.

%Returns the height of the string.
textHeight(Canvas, Text) ->
  {_, H} = wxDC:getTextExtent(Canvas, Text),
  H.

%Imports and saves an image that will be used frequently. Returns Bitmap() data type.
importImage(Path) ->
  Image = wxImage:new(Path),
  Bitmap = wxBitmap:new(Image),
  wxImage:destroy(Image),
  Bitmap.

%Returns the directory of the .beam file being executed
curDir() ->
  [Return | _] = code:get_path(), 
  Return.

%% ====================================================================
%% Internal functions
%% ====================================================================

%Checks the process's ets table to see if ths provided picture aleady exists.
checkTable(Path, Table) ->
  length(ets:lookup(Table, Path)) > 0.

%Adds a picture to the process's ets table for later retrieval.
addToTable(Path, bitmap) ->
  Image = wxImage:new(Path),
  Bitmap = wxBitmap:new(Image),
  Suc = ets:insert(bitmaptable, {Path, Bitmap}),
  wxImage:destroy(Image),
  case Suc of
    true -> ok;
    false -> iworld:quit({error, "Adding <<" ++ Path ++ ">> to bitmap picture set failed!"})
  end;

addToTable(Path, image) ->
  Image = wxImage:new(Path),
  Suc = ets:insert(imagetable, {Path, Image}),
  case Suc of
    true -> ok;
    false -> iworld:quit({error, "Adding <<" ++ Path ++ ">> to image picture set failed!"})
  end.

%Retrieves the image from the process's ets table.
getImage(Path) ->
  case checkTable(Path, imagetable) of
    true -> element(2, lists:nth(1, ets:lookup(imagetable, Path)));
    false -> addToTable(Path, image), getImage(Path)
  end.

getBitmap(Path) ->
  case checkTable(Path, bitmaptable) of
    true -> element(2, lists:nth(1, ets:lookup(bitmaptable, Path)));
    false -> addToTable(Path, bitmap), getBitmap(Path)
  end.

%Decodes the user-provided color and translates it to something WX can understand.
getColor(black,       pen) -> {?wxBLACK_PEN, false};
getColor(red,         pen) -> {?wxRED_PEN, false};
getColor(blue,        pen) -> {wxPen:new({0,0,255}), true};
getColor(green,       pen) -> {?wxGREEN_PEN, false};
getColor(white,       pen) -> {?wxWHITE_PEN, false};
getColor(grey,        pen) -> {?wxGREY_PEN, false};
getColor(light_grey,  pen) -> {?wxLIGHT_GREY_PEN, false};
getColor(cyan,        pen) -> {?wxCYAN_PEN, false};
getColor(medium_grey, pen) -> {?wxMEDIUM_GREY_PEN, false};
getColor(Color, pen) 
  when is_tuple(Color)     -> {wxPen:new(Color),true};

getColor(black,       brush) -> {?wxBLACK_BRUSH, false};
getColor(red,         brush) -> {?wxRED_BRUSH, false};
getColor(blue,        brush) -> {?wxBLUE_BRUSH, false};
getColor(green,       brush) -> {?wxGREEN_BRUSH, false};
getColor(white,       brush) -> {?wxWHITE_BRUSH, false};
getColor(grey,        brush) -> {?wxGREY_BRUSH, false};
getColor(light_grey,  brush) -> {?wxLIGHT_GREY_BRUSH, false};
getColor(cyan,        brush) -> {?wxCYAN_BRUSH, false};
getColor(medium_grey, brush) -> {?wxMEDIUM_GREY_BRUSH, false};
getColor(Color, brush) 
  when is_tuple(Color)       -> {wxBrush:new(Color), true}.

destroy(brush, Brush,   true)  -> wxBrush:destroy(Brush);
destroy(pen,   Pen,     true)  -> wxPen:destroy(Pen);
destroy(_Type, _Object, false) -> ok.
