-include("interactive_erlang.hrl").

-define(DrawLine(Canvas, Point1, Point2, Color), idraw:drawLine(Canvas, Point1, Point2, Color, 1)).
-define(DrawLine(Canvas, Point1, Point2, Color, Thickness), idraw:drawLine(Canvas, Point1, Point2, Color, Thickness)).
-define(DrawCircle(Canvas, Point, Radius, Color), idraw:drawCircle(Canvas, Point, Radius Color)).
-define(DrawRectangle(Canvas, Point, Width, Height, Color), idraw:drawRect(Canvas, Point, Width, Height, Color)).
-define(DrawText(Canvas, Point, Text), idraw:drawText(Canvas, Point, Text, black)).
-define(DrawText(Canvas, Point, Text, Color), idraw:drawText(Canvas, Point, Text, Color)).
-define(DrawImage(Canvas, Point, Path), idraw:drawImage(Canvas, Point, Path)).
-define(DrawScaledImage(Canvas, Point, Path, XScale, YScale), idraw:drawScaledImage(Canvas, Point, Path, XScale, YScale)).
-define(DrawTransImage(Canvas, Point, Path, MaskColor), idraw:drawTransImage(Canvas, Point, MaskColor, Path)).
-define(ImageWidth(Path), idraw:imageWidth(Path)).
-define(ImageHeight(Path), idraw:imageHeight(Path)).
-define(ImageWidth(Canvas, Text), idraw:textWidth(Canvas, Text)).
-define(ImageHeight(Canvas, Text), idraw:textHeight(Canvas, Text)).

-define(ResetCanvas(), idraw:resetCanvas()).
-define(ResetCanvas(Name), idraw:resetCanvas(Name)).

-define(CurrentDirectory, idraw:curDir()).

-record(posn, {x, y}).

-define(Posn(X, Y), #posn{x = X, y = Y}).
-define(Translate(Posn, X, Y), Posn#posn{x = Posn#posn.x + X, y = Posn#posn.y + Y}).
-define(Difference(Posn1, Posn2), #posn{x = Posn1#posn.x - Posn2#posn.x, y = Posn1#posn.y - Posn2#posn.y}).