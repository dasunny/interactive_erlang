-module(iuniverse).

%% ====================================================================
%% API functions
%% ====================================================================

-export([]).

-include("interactive_erlang.hrl").

%Creates a world. Function options are: 
%tick: Function to call on tick events. 
%		Arguments passed are: (WorldState(), 
%							   WindowObject())
%		Must return: WorldState()
%
%alive_for: How long to keep the World going before terminating. (integer())
%
%tickRate: Defines how often to tick the WorldState, in Ticks-per-second. (integer())
%
%print: Function to call on print events.
%		Arguments passed are: (WorldState())
%		Must return: String().
%
%matchDebug: Defines whether or not to print successful function matches to the console. (any())
%
%TODO finish this
%callOnce: big_bang will call this function once before it starts running. It calls with arity 0 and any value returned
%   will be ignored. Intended use is to perform one-time setups that the worldstate might use, but big_bang does not 
%   have access to, such as ets tables, or to initialize background canvas drawings using the draw[shape]Perm functions.

%% universe(InitialState, Options) ->
%%   register(?PARENT_PROCESS, self()),
%%   IState = #iState{state   = InitialState,
%%                    options = Options},
%%   ie_common:init(IState).

%% ====================================================================
%% Internal functions
%% ====================================================================
