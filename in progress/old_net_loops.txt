Universe:
%% TODO cleanup after verification
%% loop(#state{iworld = IWorld, socket = Socket, callbacks = {Disconnect, Receive}, stay_alive = true} = State) ->
%%   StayAlive =
%%     receive
%%       stop       -> false;
%%       disconnect -> ?SendToDiagnostic(connDown),
%%                     false;
%%       {send, Message} ->
%%         case gen_tcp:send(Socket, ie_utils_net:encode(Message)) of
%%           ok -> ?SendToDiagnostic(sendB),
%%                 true;
%%           {error, closed} -> ?SendToDiagnostic(connDown),
%%                              ?SendToDiagnostic({sendA, -1}),
%%                              ?SendToDiagnostic(#callback_event{callback = Disconnect,
%%                                                                event_object = {leave, IWorld}}),
%%                              false;
%%           {error, Reason} -> ?Put("Message failed to send!\nMessage:\n~p\nError: ~p", [Message, Reason]),
%%                              true
%%         end;
%%       {tcp, _, <<"disconnect">>} -> ?SendToDiagnostic(connDown),
%%                                     ?Send(?PARENT_PROCESS,     #callback_event{callback = Disconnect,
%%                                                                                event_object = {leave, IWorld}}),
%%                                     false;
%%       {tcp, _, Data}             -> ?SendToDiagnostic(recv),
%%                                     ?Send(?PARENT_PROCESS, #callback_event{callback = Receive,
%%                                                                            event_object = {message, IWorld,
%%                                                                                            ie_utils_net:decode(Data)}}),
%%                                     true
%%     end,
%%   loop(State#state{stay_alive = StayAlive andalso ?IsParentAlive});

World:
%%TODO cleanup after verification that it works
%% loop(#state{socket = Socket, stay_alive = true} = State) ->
%%   StayAlive =
%%     receive
%%       {send, Message} ->
%%         case gen_tcp:send(Socket, ie_utils_net:encode(Message)) of
%%           ok              -> ?SendToDiagnostic(sendB);
%%           {error, closed} -> ?Send(?PARENT_PROCESS, {close, "TCP Socket closed"});
%%           {error, Reason} -> io:format("Message failed to send! ~nMessage:~n~p~nError: ~p~n", [Message, Reason])
%%         end;
%%       {tcp, _, <<"disconnect">>} -> ?Send(?PARENT_PROCESS, {close, "Universe disconnected"});
%%       {tcp_closed, _}            -> ?Send(?PARENT_PROCESS, {close, "Universe disconnected"});
%%       {tcp, _, Data}             -> Got = ie_utils_net:decode(Data),
%%                                     ?SendToDiagnostic(recv),
%%                                     ?Send(?PARENT_PROCESS, #callback_event{event_object =  Got,
%%                                                                            callback = Callback});
%%       Other                      -> io:format("An unknown message was received by the message process-\n~p\n", [Other])
%%     end,
%%   loop(State#state{stay_alive = ?IsParentAlive});