-module(idraw).

-include_lib("wx/include/wx.hrl").
-include("idraw.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([newCanvas/2]). %Canvas related
-export([draw/2]). %Generic drawing related
-export([curDir/0, imageWidth/1, imageHeight/1, textWidth/1, textHeight/1]). %Utility related

-define(NoMask, no_mask).
-define(Bitmap(X, Y, Mask), {wxBitmap, X, Y, Mask}).
-define(Bitmap, ?Bitmap(1, 1, ?NoMask)).

-define(P2T, fun posnToTuple/1).
-define(C2T, fun colorToTuple/1).

newCanvas(Width, Height) ->
  Frame = wxFrame:new(wx:new(), -1, "Canvas", [{size, {Width, Height}}]),
  wxWindow:setClientSize(Frame, Width, Height),
  Canvas = wxPanel:new(Frame, 0, 0, Width, Height),
  DC = wxWindowDC:new(Canvas),
  wxFrame:show(Frame),
  DC.

draw(DC, #line{point1 = Point1, point2 = Point2, 
               color = Color, thickness = Thickness}) ->
  ?Composite(Fill, _Line) = ensureComposite(Color),
  Pen = wxDC:getPen(DC),
  wxPen:setColour(Pen, ?C2T(Fill)),
  wxPen:setWidth(Pen, Thickness),
  wxDC:drawLine(DC,?P2T(Point1), ?P2T(Point2)),
  wxPen:setWidth(Pen, 1);

draw(DC, #circle{center = Center, radius = Radius, color = Color}) ->
  setColors(Color, DC),
  wxDC:drawCircle(DC, ?P2T(Center), Radius);

draw(DC, #rectangle{topLeft = ?Posn(X, Y), width = Width, height = Height, color = Color}) ->
  setColors(Color, DC),
  wxDC:drawRectangle(DC, {trunc(X), trunc(Y), Width, Height});

draw(DC, #text{topLeft = TopLeft, text = Text, color = Color}) ->
  ?Composite(Fill, _Line) = ensureComposite(Color),
  wxDC:setTextForeground(DC, ?C2T(Fill)),
  wxDC:drawText(DC,Text, ?P2T(TopLeft));

draw(DC, #image{topLeft = TopLeft, path = Path, options = Options}) ->
  {XScale, YScale} = case lists:keyfind(scale, 1, Options) of
                       {scale, Xs, Ys} -> {Xs, Ys};
                       false           -> {1, 1}
                     end,
  {Mask, Use} = case lists:keyfind(mask, 1, Options) of
                  {mask, Color} -> {Color, true};
                  false         -> {?NoMask, false}
                end, 
  Bitmap = get(?Bitmap(XScale, YScale, Mask), Path),
  wxDC:drawBitmap(DC, Bitmap, ?P2T(TopLeft), [{useMask, Use}]).

%Returns the image width dimension.
imageWidth(Path) ->
  Bitmap = get(?Bitmap, Path),
  wxBitmap:getWidth(Bitmap).

%Returns the image height dimension.
imageHeight(Path) ->
  Bitmap = get(?Bitmap, Path),
  wxBitmap:getHeight(Bitmap).

%Returns the width of the string.
textWidth(Text) ->
  DC = get(canvas),
  {Width, _Height} = wxDC:getTextExtent(DC, Text),
  Width.

%Returns the height of the string.
textHeight(Text) ->
  DC = get(canvas),
  {_Width, Height} = wxDC:getTextExtent(DC, Text),
  Height.

%Returns the directory of the .beam file being executed
curDir() ->
  [Return | _] = code:get_path(), 
  Return.

%% ====================================================================
%% Internal functions
%% ====================================================================

colorToTuple(?COLOR(R, G, B)) ->
  {R, G, B}.

posnToTuple(?Posn(X, Y)) ->
  {trunc(X), trunc(Y)}.

ensureComposite(#composite{} = Comp) -> Comp;
ensureComposite(#color{} = Color) -> ?Composite(Color).

setColors(Color, DC) ->
  ?Composite(Fill, Line) = ensureComposite(Color),
  Brush = wxDC:getBrush(DC),
  Pen   = wxDC:getPen(DC),
  true = wxBrush:isOk(Brush),
  wxBrush:setColour(Brush, ?C2T(Fill)),
  wxPen:setColour(Pen,     ?C2T(Line)).

make(?Bitmap(X, Y, M), Path) ->
  Image = wxImage:new(Path),
  ScaledImage = wxImage:scale(Image, round(wxImage:getWidth(Image)*X), round(wxImage:getHeight(Image)*Y)),
  Bitmap = wxBitmap:new(ScaledImage),
  wxImage:destroy(ScaledImage),
  case M of
    ?NoMask -> ok;
    Color   -> Mask = wxMask:new(Bitmap, colorToTuple(Color)),
               wxBitmap:setMask(Bitmap, Mask)
  end,
  put({?Bitmap(X, Y, M), Path}, Bitmap),
  Bitmap.

get(Type, Key) ->
  case get({Type, Key}) of
    undefined -> make(Type, Key);
    Item      -> Item
  end.
