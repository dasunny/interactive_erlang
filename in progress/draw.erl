%% @author etibdan
%% @doc @todo Add description to draw.


-module(draw).

-include("interactive_erlang.hrl").
-include_lib("wx/include/wx.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([init/1, event/2, startProcess/1]).

-record(state, {parent,
                diagnostic,
                stayAlive = true,
                doDraw = true,
                children = [],
                canvases,
                callback,
                refreshRate,
                wx_env}).

-record(childState, {parent,
                     wx_env,
                     width,
                     height,
                     x,
                     y}).

init(#iState{callbacks = #callbacks{draw = undefined}} = IState) -> IState;
init(#iState{callbacks = #callbacks{pane = Panes,
                                    tickRate = Rate,
                                    draw = Draw},
             processes = #processes{parent = Parent,
                                    diagnostic = Diagnostic} = Processes,
             window = Parent,
             wx_env = Env} = IState) ->
  Canvases = createCanvases(Parent, Panes),
  Pid = spawn(?MODULE, startProcesses, #state{parent = Parent, diagnostic = Diagnostic, canvases = Canvases, callback = Draw, 
                                              refreshRate = trunc(1000/Rate), wx_env = Env}),
  IState#iState{canvases = Canvases, processes = Processes#processes{draw = Pid}}.

event(#iState{callbacks = #callbacks{draw = undefined}} = IWorldState, _EventRecord) -> IWorldState; 
event(#iState{state = State,
              processes = #processes{draw = Draw}} = IState, 
      _EventRecord) ->
  common:safeSend(Draw, {draw, State, nowMilli()}),
  IState.

%% ====================================================================
%% Internal functions
%% ====================================================================

startProcess(#state{wx_env = Env} = State) ->
  wx:set_env(Env),
  loop(State).

loop(#state{stayAlive = true, doDraw = true, parent = Parent, diagnostic = Diagnostic} = State) ->
  UpdatedState = receive
                   switch -> common:safeSend(Diagnostic, switchDraw),
                             State#state{stayAlive = is_process_alive(Parent),
                                         doDraw = false};
                   {draw, WorldState, Then} -> draw(State, WorldState, nowMilli() - Then)
                 end,
  loop(UpdatedState#state{stayAlive = is_process_alive(Parent)});

loop(#state{stayAlive = true, doDraw = false, parent = Parent, diagnostic = Diagnostic} = State) ->
  DoDraw = receive
             switch -> common:safeSend(Diagnostic, switchDraw), true;
             _ -> false
           after
               500 -> false
           end,
  loop(State#state{stayAlive = is_process_alive(Parent), doDraw = DoDraw});

loop(#state{stayAlive = false}) ->
  ok.

createCanvases(Parent, []) ->
  {Width, Height} = wxWindow:getSize(Parent),
  [{undefined, Width, Height, 0, 0}];

createCanvases(Parent, Panes) ->
  Fun = fun({Name, Width, Height, X, Y}) ->
                Panel = wxPanel:new(Parent, X, Y, Width, Height),
                Bitmap = wxBitmap:new(Width,Height),
                MemoryDC = wxMemoryDC:new(Bitmap),
                ets:insert(permcanvastable, {Name, MemoryDC}),
                wxBitmap:destroy(Bitmap),
                #canvas{name = Name, memoryDC = MemoryDC, panel = Panel} end,
  [Fun(Pane) || Pane <- Panes].

nowMilli() -> 
  {Mega, Seconds, Micro} = erlang:now(),
   ((Mega*1000000 + Seconds)*1000000 + Micro) div 1000.

draw(#state{parent = Parent, refreshRate = Rate} = State, _WorldState, Diff) when Diff >= Rate ->
  State;

draw(#state{callback = Draw, parent = Parent} = State, WorldState, _Diff) ->
  ok.
       
getDraws(WorldState, {Module, Function}, #canvas{name = undefined}) ->
  Module:Function(WorldState, []);
getDraws(WorldState, {Module, Function}, #canvas{name = Name}) ->
  Module:Function(WorldState, [], Name).










drawEvent(_, _, undefined) -> ok;
drawEvent(World, Canvases, Draw) ->
case length(Canvases) == 1 of
false -> drawEvent2(World, Canvases, Draw);
true -> drawEvent3(World, lists:nth(1, Canvases), Draw, false)
end.
drawEvent2(_, [], _) ->
ok;
drawEvent2(World, [H|T], Draw) ->
drawEvent3(World, H, Draw, true),
drawEvent2(World, T, Draw).
drawEvent3(World, Pane, Draw, Bool) ->
{Name, MemDC, Canvas} = Pane,
{M,F} = Draw,
{X,Y} = wxWindow:getSize(Canvas),
Bitmap = wxBitmap:new(X,Y),
TempDC = wxMemoryDC:new(Bitmap),
CopyDC = wxWindowDC:new(Canvas),
wxDC:blit(TempDC, %destination canvas
{0,0},  %destination point
{X, Y}, %width/height of copy area
MemDC, %source canvas
{0,0}), %source point
case Bool of
true -> M:F(World, TempDC, Name);
false -> M:F(World, TempDC)
end,
wxDC:blit(CopyDC, %destination canvas
{0,0},  %destination point
{X, Y}, %width/height of copy area
TempDC, %source canvas
{0,0}), %source point
wxBitmap:destroy(Bitmap),
wxWindowDC:destroy(CopyDC),
wxMemoryDC:destroy(TempDC).

