-module(iworld).

%% ====================================================================
%% API functions
%% ====================================================================

-export([new_scene/3, draw_scene/1]). %For the user

-include("interactive_erlang.hrl").

%Creates a world. Function options are: 
%draw: Function to call on draw events. 
%		Arguments passed are: (WorldState(), 
%							   Canvas(),
%							   [name = Atom()])
%		The <name> argument is not used if there is only one Canvas() in the window. (See Pane, below)
%		Must return: Canvas()
%
%tick: Function to call on tick events. 
%		Arguments passed are: (WorldState(), 
%							   WindowObject())
%		Must return: WorldState()
%
%mouse: Function to call on mouse events. 
%		Arguments passed are: (WorldState(),
%							   Mouse-x coordinate = integer(), 
%							   Mouse-y coordinate = integer(), 
%							   [left_up | left_down | right_up | right_down | motion] = atom())
%		Must return: WorldState()
%
%key: Function to call on key events.
%		Arguments passed are: (WorldState(),
%							   key = atom(),
%							   type = [key_up | key_down] = atom())
%		Must return: WorldState()
%
%menu: Function to call on the menu building event.
%		Arguments passed are: (WindowObject())
%		Must return: null()
%
%menuEvent: Function to call on menu events.
%		Arguments passed are: (WorldState(),
%							   WindowObject(), 
%							   Menu_item_ID = integer())
%		Must return: WorldState()
%
%alive_for: How long to keep the World going before terminating. (integer())
%
%tickRate: Defines how often to tick the WorldState, in Ticks-per-second. (integer())
%
%print: Function to call on print events.
%		Arguments passed are: (WorldState())
%		Must return: String().
%
%matchDebug: Defines whether or not to print successful function matches to the console. (any())
%
%pane: Adds a separate drawing canvas to the window, to prevent drawing overlap between drawing areas. If this
%	   parameter is not used, a single canvas is created with the dimensions provided in the creation of the
%	   Window() object.
%	   If this option is used, you must specify every canvas you wish to create along with a name (atom) that it will be
%	   identified as in 'draw' function calls (See draw, above).
%	   Multiple panes may be created per window by calling this option multiple times.
%	   ({name = Atom(), width = integer(), height = integer(), x-offset = integer(), y-offset = integer()})
%
%resize: Function to call on window resize events. If this option is used, it is the responsibility of the programmer
%   to keep track of the current window size and adjust 'draw' events accordingly.
%   Arguments passed are: (WorldState(),
%                         {New Width = integer(), 
%                          New Height = integer()})
%   Must return: WorldState()
%
%resizePane: If the 'panes' option is also used, this function is called to supply new dimensions and offsets to the
%   panes. This function is called once for every pane, each time calling with the pane's name (the one supplied in the 
%   'pane' option) 
%   with a 
%TODO finish this, reorder to alphabetically
%callOnce: big_bang will call this function once before it starts running. It calls with arity 0 and any value returned
%   will be ignored. Intended use is to perform one-time setups that the worldstate might use, but big_bang does not
%   have access to, such as ets tables, or to initialize background canvas drawings using the draw[shape]Perm functions.

%% big_bang(InitialState, Options) ->
%%   register(?PARENT_PROCESS, self()),
%%   IState = #iState{state   = InitialState,
%%                    options = Options},
%%   
%%   ie_common:init(IState).

%Creates a new Scene to draw on.
new_scene(Title, Width, Height) ->
  Frame = wxFrame:new(wx:new(), 1, Title, [{size, {Width, Height}}]),
  wxFrame:setClientSize(Frame, Width, Height),
  Parent = self(),
  CB = fun(_,_) -> ?Send(Parent, {close, "Window closed"}) end,
  wxEvtHandler:connect(Frame, close_window, [{callback, CB}]),
  Frame.

%Draws the supplied window.
draw_scene(Scene) ->
  wxFrame:show(Scene).

%% %Terminates the program.
%% quit({error, _Reason} = Error) ->
%%   ie_common:flush(),
%%   ?Send(self(), {close, Error});
%% 
%% quit(World) ->
%%   ie_common:flush(),
%%   ?Send(self(), {close, "iworld:quit/1 called"}),
%%   World.

%% ====================================================================
%% Internal functions
%% ====================================================================
