-module(ie_comp_draw).

-include("interactive_erlang.hrl").

-behaviour(ie_component).

%% ====================================================================
%% API functions
%% ====================================================================
-export([component_info/0, describe/1, init/1, event/2]).
-export([start_process/1, draw_permanent/1, reset_canvas/1]).

-define(HORIZONTAL, horizontal).
-define(VERTICAL,   vertical).
-define(GENERATIONLIMIT, 0).
-define(OVERTIMELIMIT, 25).
-define(sqrt, fun math:sqrt/1).

-record(state, {parent, %%leave parent because the subcanvases don't rely on ?PARENT_PROCESS
                stay_alive = true,
                do_draw = true,
                canvas,
                callback,
                refresh_rate,
                wx_env,
                region,
                children = [],
                generation = 0,
                overtimed = 0,
                last_frame_time = 0}).

component_info() -> #component_info{iworld_init_info    = [ie_comp_diagnostic],
                                    iuniverse_init_info = undefined,
                                    iworld_runtime_spec = ?RUNTIME_LAST}.

describe(Options) ->
  Draw = case ?OptionGetValue(draw, Options) of
           undefined -> undefined;
           Callback  -> {"draw", Callback}
         end,
  Panes = case ?OptionGetValue(panes, Options) of
            undefined -> undefined;
            P         -> {"panes", "~p panes found.", [length(P)]}
          end,
  Title = case ?OptionGetValue(title, Options) of
            undefined -> undefined;
            T         -> {"title", "This window will be called ~p.", [T]}
          end,
  Size = case ?OptionGetValue(size, Options) of
            undefined -> undefined;
            {W, H}    -> {"size", "This window will have a size of ~px~p.", [W,H]}
          end,
  Icon = case ?OptionGetValue(icon, Options) of
            undefined -> undefined;
            Path      -> {"icon", "This window will use the icon located at ~s.", [Path]}
          end,
  [Draw, Panes, Title, Size, Icon].

init(#iState{options = Options} = IState) ->
  init(IState, ?OptionGetValue(draw, Options)).

init(IState, undefined) -> {ok, IState};
init(#iState{options = Options,
             extra_data = #{wx := Wx,
                            wx_env := Env} = ExtraData} = IState, Callback) ->
  Size  = ?OptionGetValue3(size, Options, {500,500}),
  Title = ?OptionGetValue3(title, Options, "Interactive Erlang IWorld"),
%%   Icon = ?OptionGetValue(icon, Options) %%TODO implement see bottom
  ParentWindow = create_window(Wx, Size, Title),
  Fun = fun(Elem) -> Pid = spawn(?MODULE, start_process,
                                 [#state{parent     = whereis(?PARENT_PROCESS), 
                                         canvas     = Elem,
                                         callback   = Callback, 
                                         region = #region{top_left = #posn{x = 0, y = 0},
                                                          width = Elem#canvas.width,
                                                          height = Elem#canvas.height},
                                         refresh_rate = trunc(1000/?OptionGetValue3(tick_rate, Options, 10)),
                                         wx_env = Env}]),
                     Elem#canvas{pid = Pid} end,
  
  Canvases0 = create_canvases(ParentWindow, ?OptionGetValue3(panes, Options, [])),
  Canvases1 = [Fun(Canvas) || Canvas <- Canvases0],
  {ok, IState#iState{extra_data = ExtraData#{canvases => Canvases1,
                                             window   => ParentWindow}}}.

event(#iState{options = Options} = IState, EventObject) ->
  event(IState, EventObject, ?OptionGetValue(draw, Options)).

event(IState, _EventRecord, undefined) -> IState; 
event(#iState{state = WorldState,
              extra_data = #{canvases := Canvases}} = IState, 
      _EventRecord, Callback) ->
  Now = now_milli(),
  Fun = fun(#canvas{name = Name, pid = Pid}) -> Draws = lists:reverse(get_draws(WorldState, Callback, Name)),
                                                ?Send(Pid, {draw, Draws, Now}) end,
  lists:foreach(Fun, Canvases),
  IState.

draw_permanent(Draws) ->
  {canvas, Name} = lists:last(Draws),
  TrimmedDraws = lists:sublist(Draws, 1, length(Draws -1)),
  ?Send(Name, {perm, TrimmedDraws}).

reset_canvas(Name) ->
  ?Send(Name, reset).

%% ====================================================================
%% Internal functions
%% ====================================================================

start_process(#state{wx_env = Env, canvas = #canvas{memoryDC1 = MemoryDC1,
                                                    name2 = Name}} = State) ->
  RegName = list_to_atom(Name),
  put(canvas, MemoryDC1),
  try
    unregister(RegName)
  catch _:_ -> ok end,
  register(RegName, self()),
  ?Send(?DIAGNOSTIC_PROCESS, {register, Name}),
  wx:set_env(Env),
  loop(State).

loop(#state{stay_alive = true, overtimed = ?OVERTIMELIMIT, parent = Parent, 
            generation = Generation, canvas = #canvas{name = Name}} = State) 
  when Generation < ?GENERATIONLIMIT ->
  io:format("Overtime Limit reached. Splitting Canvas ~p\n", [Name]),
  UpdatedState = split(State),
  loop(UpdatedState#state{stay_alive = is_process_alive(Parent)});

loop(#state{stay_alive = true, do_draw = true, parent = Parent, children = Children,
            canvas = #canvas{memoryDC1 = PermCanvas, name2 = Name, panel = Panel} = Canvas, 
            region = Region} = State) ->
  UpdatedState = receive
                   switch -> ?Send(?DIAGNOSTIC_PROCESS, {time, Name, -1}),
                             delegate(State#state{do_draw = false}, switch);
                   {draw, Draws, Then} when Children == [] -> NewState = draw(State, Draws, now_milli() - Then),
                                                              Time = NewState#state.last_frame_time,
                                                              ?Send(?DIAGNOSTIC_PROCESS, {time, Name, Time}),
                                                              NewState;
                   {draw, _Draws, _Then} = Got -> delegate(State, Got);
                   {perm, PermDraws}           -> draw_permanent(PermCanvas, PermDraws), State;
                   reset                       -> wxDC:clear(PermCanvas), State;
                   close                       -> exit(normal); %%TODO properly close children windows
                   {newDim, Width, Height}     -> close_children(Children),
                                                  wxPanel:setSize(Panel, Width, Height),
                                                  State#state{children = [],
                                                              canvas = Canvas#canvas{width = Width, height = Height},
                                                              region = Region#region{width = Width, height = Height}}
                 end,
  loop(UpdatedState#state{stay_alive = is_process_alive(Parent)});

loop(#state{stay_alive = true, do_draw = false, parent = Parent} = State) ->
  DoDraw = receive
             switch -> true;
             _      -> false
           after
               500 -> false
           end,
  loop(State#state{stay_alive = is_process_alive(Parent), do_draw = DoDraw});

loop(#state{stay_alive = false}) ->
  ok.

create_window(Wx, {Width, Height}, Title) ->
  Frame = wxFrame:new(Wx, 1, Title, [{size, {Width, Height}}]),
  wxFrame:setClientSize(Frame, Width, Height),
  Parent = self(),
  CB = fun(_,_) -> ?Send(Parent, {close, "Window closed"}) end,
  wxEvtHandler:connect(Frame, close_window, [{callback, CB}]),
  wxFrame:show(Frame),
  Frame.

create_canvases(Parent, []) ->
  {Width, Height} = wxWindow:getClientSize(Parent),
  create_canvases(Parent, [{?DEFAULT_CANVAS, Width, Height, 0, 0}]);

create_canvases(Parent, Panes) ->
  CB = fun(_,_) -> wxWindow:setFocus(Parent) end,
  Fun = fun({Name, Width, Height, X, Y}) ->
                Panel = wxPanel:new(Parent, X, Y, Width, Height),
                wxEvtHandler:connect(Panel, child_focus, [{callback, CB}]),
                Bitmap = wxBitmap:new(Width,Height),
                MemoryDC1 = wxMemoryDC:new(Bitmap),
                MemoryDC2 = wxMemoryDC:new(Bitmap),
                WindowDC = wxWindowDC:new(Panel),
                wxBitmap:destroy(Bitmap),
                put(canvas, MemoryDC1),
                #canvas{name = Name, name2 = atom_to_list(Name), width = Width, height = Height,
                        memoryDC1 = MemoryDC1, memoryDC2 = MemoryDC2, windowDC = WindowDC, panel = Panel} end,
  [Fun(Pane) || Pane <- Panes].

now_milli() -> 
  {Mega, Seconds, Micro} = erlang:now(),
  ((Mega*1000000 + Seconds)*1000000 + Micro) div 1000.

to_tuple(?Posn(X, Y)) -> {X, Y}.

delegate(#state{children = Children} = State, switch) ->
  lists:foreach(fun(Elem) -> ?Send(Elem, switch) end, Children),
  State;
delegate(#state{children = Children, region = Region} = State, Got) ->
  {draw, Draws, Then} = Got,
  Pred = fun(Elem) -> within(Region, Elem) end,
  FilteredDraws = lists:filter(Pred, Draws),
  Fun = fun(Elem) -> ?Send(Elem, {draw, FilteredDraws, Then}) end,
  lists:foreach(Fun, Children),
  State.

draw(#state{refresh_rate = Rate} = State, _Draws, Diff) when Diff >= Rate ->
  % io:format("Skipped Draw. Diff: ~p. Required: ~p\n", [Diff, Rate]),
  State;

draw(#state{refresh_rate = Rate, overtimed = OverTime,
            canvas = #canvas{memoryDC1 = DC1, memoryDC2 = DC2, windowDC = DC3}, 
            region = #region{top_left = TopLeft, width = Width, height = Height} = Region} = State, 
     Draws, _Diff) ->
  Pred = fun(Elem) -> within(Region, Elem) end,
  Start = now_milli(),
  FilteredDraws = lists:filter(Pred, Draws),
  wxDC:blit(DC2,                %destination canvas
            to_tuple(TopLeft),  %destination point
            {Width, Height},    %width/height of copy area
            DC1,                %source canvas
            to_tuple(TopLeft)), %source point
  lists:foreach(fun(Elem) -> idraw:draw(DC2, Elem) end, FilteredDraws),
  wxDC:blit(DC3,                %destination canvas
            to_tuple(TopLeft),  %destination point
            {Width, Height},    %width/height of copy area
            DC2,                %source canvas
            to_tuple(TopLeft)), %source point
  Stop = now_milli(),
  Time = Stop - Start,
  OTAdd = case Time > Rate of
            true  -> %io:format("~p overtimed by ~pms. Now at ~p\n", [Name, Time-Rate, OverTime]), 
              1;
            false -> -1
          end,
  NewOT = min(max(0, OverTime + OTAdd), ?OVERTIMELIMIT),
  wxDC:clear(DC2),
  State#state{overtimed = NewOT, last_frame_time = Time}.

draw_permanent(PermCanvas, PermDraws) ->
  lists:foreach(fun(Elem) -> idraw:draw(PermCanvas, Elem) end, PermDraws).

get_draws(WorldState, Callback, ?DEFAULT_CANVAS) ->
  Callback(WorldState, []);
get_draws(WorldState, Callback, Name) ->
  Callback(WorldState, [], Name).

split(#state{region = Region, 
             generation = Generation, 
             canvas = #canvas{memoryDC2 = MemoryDC} = Canvas} = State) ->
  #region{top_left = TopLeft, width = Width,height = Height} = Region,
  Direction = split_direction(Region),
  {Region1, Region2} = case Direction of
                         ?HORIZONTAL -> {Region#region{height = Height div 2},
                                         Region#region{top_left = ?Translate(TopLeft, 0, Height div 2),
                                                       height = (Height div 2) + (Height rem 2)}};
                         ?VERTICAL   -> {Region#region{width = Width div 2},
                                         Region#region{top_left = ?Translate(TopLeft, Width div 2, 0),
                                                       width = (Width div 2) + (Width rem 2)}}
                       end,
  Name = Canvas#canvas.name2,
  {Name1, Name2} = case Direction of
                     ?HORIZONTAL -> {Name ++ "T", Name ++ "B"};
                     ?VERTICAL   -> {Name ++ "L", Name ++ "R"}
                   end,
  {DCWidth, DCHeight} = wxDC:getSize(MemoryDC),
  Bitmap = wxBitmap:new(DCWidth, DCHeight),
  NewMemoryDC = wxMemoryDC:new(Bitmap), 
  wxBitmap:destroy(Bitmap),
  Canvas1 = Canvas#canvas{name2 = Name1},
  Canvas2 = Canvas#canvas{name2 = Name2, memoryDC2 = NewMemoryDC},
  ChildState = State#state{generation = Generation + 1, overtimed = 0, last_frame_time = 0, parent = self()},
  ChildState1 = ChildState#state{region = Region1, canvas = Canvas1},
  ChildState2 = ChildState#state{region = Region2, canvas = Canvas2},
  Pid1 = spawn(?MODULE, start_process, [ChildState1]),
  Pid2 = spawn(?MODULE, start_process, [ChildState2]),
  State#state{children = [Pid1, Pid2], overtimed = -1}.

split_direction(#region{width = Width, height = Height}) ->
  case Width < Height of
    true -> ?HORIZONTAL;
    false -> ?VERTICAL
  end.

close_children(Children) ->
  [?Send(To, close) || To <- Children].

%line,circle,rectangle,text,image
within(Region, #line{point1 = P1, point2 = P2}) ->
  #posn{x = Dx, y = Dy} = ?Difference(P2, P1),
  within(Region, #rectangle{top_left = P1, width = Dx, height = Dy});
within(Region, #circle{center = Center, radius = Radius}) -> 
  within(Region, #rectangle{top_left = ?Translate(Center, -Radius, -Radius), width = Radius*2, height = Radius*2});
within(Region, #rectangle{top_left = RectTopLeft, width = RectWidth, height = RectHeight}) ->
  {#posn{x = P1x, y = P1y}, _, _, #posn{x = P2x, y = P2y}} = corners(Region),
  {#posn{x = P3x, y = P3y}, _, _, #posn{x = P4x, y = P4y}} = corners(RectTopLeft, RectWidth, RectHeight),
  not ((P2y < P3y) or (P1y > P4y) or (P2x < P3x) or (P1x > P4x));
within(Region, #text{top_left = TopLeft, text = Text}) ->
  Width = idraw:text_width(Text),
  Height = idraw:text_height(Text),
  within(Region, #rectangle{top_left = TopLeft, width = Width, height = Height});
within(Region, #image{top_left = TopLeft, path = Path, options = Options}) ->
  {XScale, YScale} = case lists:keyfind(scale, 1, Options) of
                       {scale, X, Y} -> {X, Y};
                       false         -> {1, 1}
                     end,
  Width  = round(idraw:image_width(Path)  * XScale),
  Height = round(idraw:image_height(Path) * YScale),
  within(Region, #rectangle{top_left = TopLeft, width = Width, height = Height}).

corners(#region{top_left = TopLeft, width = Width, height = Height}) ->
  corners(TopLeft, Width, Height).
corners(TopLeft, Width, Height) ->
  TopRight = ?Translate(TopLeft, Width, 0),
  BottomLeft = ?Translate(TopLeft, 0, Height),
  BottomRight = ?Translate(TopLeft, Width, Height),
  {TopLeft, TopRight, BottomLeft, BottomRight}.

%  case Icon of
%    undefined -> wxTopLevelWindow:setIcon(Scene, wxIcon:new(idraw:curDir()++"/icons/world.bmp"));
%    _       -> wxTopLevelWindow:setIcon(Scene, wxIcon:new(Icon))
%  end,
