-module(cheesy).

-include("idraw.hrl").
-include("iworld.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([start/0, on_draw/2, on_key/3, on_tick/1, on_resize/2, on_diag/1]).

-define(WIDTH, 1000).
-define(HEIGHT, 500).
-define(SPEED, 5).
-define(RANGE, 30).
-define(CHEESEDENSITY, 150000).
-define(CHEESECOUNT, 3).
-define(FPS, 20).
-define(CHEESERAINCOUNT, 50).

-define(BIGCHEESETTL, 10).
-define(SPEEDCHEESETTL, 10).
-define(RAINCHEESETTL, 7).
-define(COLORCHEESETTL, 15).
-define(BADCHEESETTL, 25).

-define(KS1, {up, down, left, right}).
-define(KS2, {"w", "s", "a", "d"}).
-define(KS3, {"i", "k", "j", "l"}).
-define(KS4, {num8, num5, num4, num6}).

%1 = EASY, bots can not cross or see over edges.
%2 = MEDIUM, bots can cross over edges but can not see pass them. 
%3 = HARD, bots can both cross and see over edges.
-define(BOTDIFFICULTY, 1).
-define(ESTIMATIONNOISE, 75).

-define(BIGCHEESECHANCE, 5). %Percent chance that a big cheese will spawn.
-define(SPEEDCHEESECHANCE, 5). %Percent chance that a speed boost cheese will spawn.
-define(CHEESERAINCHANCE, 1). %Percent chance that a "rain of cheese" cheese will occur.
-define(BADCHEESECHANCE, 0). %Percent chance that a bad cheese will spawn.


-record(game, {loc = [], 
               state = login, 
               playing = {1, 1, 1, 1}, %1 = not playing, 2 = playing, 3 = bot/computer controller
               p1 = undefined,
               p2 = undefined,
               p3 = undefined,
               p4 = undefined,
               size = {?WIDTH, ?HEIGHT},
               version
              }).

-record(keyset, {up = false,
                 down = false, 
                 left = false, 
                 right = false
                }).

-record(player, {x, 
                 y, 
                 score = 0,
                 keySet = #keyset{}, 
                 dir,
                 color,
                 atomColor,
                 bot = false,
                 focus,
                 speed = 0,
                 perks = []
                }).

-record(cheese, {x, 
                 y,
                 ref,
                 path,
                 value = 0,
                 weight = 1,
                 perk = undefined,
                 color = all,
                 ttl = 120*?FPS
                }).

-record(perkSet, {speed = 0,
                  rain = false
                 }).


start() ->
  ?IWorld(newGame(), [{draw,       fun ?MODULE:on_draw/2},
                      {key,        fun ?MODULE:on_key/3},
                      {tick,       fun ?MODULE:on_tick/1},
                      {resize,     fun ?MODULE:on_resize/2},
                      {diagnostic, fun ?MODULE:on_diag/1},
                      {clock_rate, ?FPS},
                      {title,      "Cheesy!"},
                      {size,       {?WIDTH, ?HEIGHT}}
                     ]).

on_draw(World, Canvas) ->
  {W, H} = World#game.size,
  drawInt(World, ?DrawRectangle(Canvas, ?Posn(0,0), W, H, ?WHITE)).

on_key(#game{state = login} = World, Key, Type) ->
  keyLogin(World, Key, Type, {element(1, ?KS1),
                              element(1, ?KS2),
                              element(1, ?KS3),
                              element(1, ?KS4)});
on_key(World, Key, Type) ->
  keyInt(World, Key, Type).

on_tick(World) ->
  tickInt(World).

on_resize(World, NewSize) ->
  World#game{size = NewSize}.

on_diag(World) ->
  diag_internal(World).

%% ====================================================================
%% Internal functions
%% ====================================================================

newGame() ->
  #game{loc = makeCheeses(max(1,getMinCheeseCount({?WIDTH, ?HEIGHT})), {?WIDTH, ?HEIGHT})}.

updatePlayerDir(undefined, _, _) ->
  undefined;
updatePlayerDir(#player{bot = true, focus = Focus} = Player, LoC, WH) ->
  botMovement(Player, getCheese(LoC, Focus), WH);
updatePlayerDir(#player{dir = Dir, keySet = #keyset{up = U, down = D, left = L, right = R}}, _, _) ->
  case {U, D, L, R} of
    {true,  false, false, false} -> 0;
    {true,  false, false, true}  -> 1;
    {false, false, false, true}  -> 2;
    {false, true,  false, true}  -> 3;
    {false, true,  false, false} -> 4;
    {false, true,  true,  false} -> 5;
    {false, false, true,  false} -> 6;
    {true,  false, true,  false} -> 7;
    _                            -> Dir
  end.

updatePlayer(undefined, _, _) ->
  {undefined, false};
updatePlayer(#player{x = X, y = Y, dir = Dir, perks = Perks} = Player, Loc, {W, H} = WH) ->
  #perkSet{speed = PSSpeed, rain = Rain} = perkPlayer(Perks),
  Speed = ?SPEED + PSSpeed,
  {Newx, Newy} = case Dir of
                   0 -> {X,           Y - Speed*2};
                   1 -> {X + Speed,   Y - Speed};
                   2 -> {X + Speed*2, Y};
                   3 -> {X + Speed,   Y + Speed};
                   4 -> {X,           Y + Speed*2};
                   5 -> {X - Speed,   Y + Speed};
                   6 -> {X - Speed*2, Y};
                   7 -> {X - Speed,   Y - Speed}
                 end,
  
  Focus = updateFocus(Player, Loc, WH),
  
  {Player#player{x = playerMoveSub(Newx, W), 
                 y = playerMoveSub(Newy, H), 
                 dir = updatePlayerDir(Player#player{focus = Focus}, Loc, WH),
                 focus = Focus, 
                 perks = updatePerks(Perks)
                },
   Rain}.

playerMoveSub(N, Border) when N < 0 -> N + Border;
playerMoveSub(N, Border) when N > Border -> N - Border;
playerMoveSub(N, _Border) -> N.

perkPlayer(Lop) ->
  perkPlayer(Lop, #perkSet{}).

perkPlayer([], PerkSet) -> PerkSet;
perkPlayer([{Type, Param}|T], PerkSet) ->
  if
    Type == speed, 
    Param > 0        -> perkPlayer(T, PerkSet#perkSet{speed = ?SPEED + PerkSet#perkSet.speed});
    
    Type == rain, 
    Param > 0, 
    Param rem 4 == 0 -> perkPlayer(T, PerkSet#perkSet{rain = true});
    
    true             -> perkPlayer(T, PerkSet)
  end.

updatePerks([]) -> [];
updatePerks([{speed, Param} | T]) when Param > 0 -> [{speed, Param - 1} | updatePerks(T)];
updatePerks([{speed, _Param} | T]) -> updatePerks(T);
updatePerks([{rain, Param}|T]) when Param > 0 -> [{rain, Param - 1} | updatePerks(T)];
updatePerks([{rain, _Param} = H|T]) -> [H | updatePerks(T)];
updatePerks([_ | T]) ->  updatePerks(T).

newCheese(Color, {W, H}) when is_atom(Color) ->
  #cheese{x = random:uniform(W - 20) + 10,
          y = random:uniform(H - 10) + 10,
          ref = make_ref(),
          path = getColor(Color) ++ "cheese.png",
          value = 1,
          weight = 3,
          color = Color,
          ttl = ?COLORCHEESETTL*?FPS
         };

newCheese(N, {W, H}) when N > (100 - ?BIGCHEESECHANCE) ->
  #cheese{x = random:uniform(W - 20) + 10,
          y = random:uniform(H - 10) + 10,
          ref = make_ref(),
          path = "bigcheese.png",
          value = 10,
          weight = 10,
          ttl = ?BIGCHEESETTL*?FPS
         };

newCheese(N, {W, H}) when N > (100 - ?BIGCHEESECHANCE - ?SPEEDCHEESECHANCE) ->
  #cheese{x = random:uniform(W - 20) + 10,
          y = random:uniform(H - 10) + 10,
          ref = make_ref(),
          path = "speedcheese.png",
          perk = speed,
          weight = 2,
          ttl = ?SPEEDCHEESETTL*?FPS
         };

newCheese(N, {W, H}) when N > (100 - ?BIGCHEESECHANCE - ?SPEEDCHEESECHANCE - ?CHEESERAINCHANCE) ->
  #cheese{x = random:uniform(W - 20) + 10,
          y = random:uniform(H - 10) + 10,
          ref = make_ref(),
          path = "raincheese.png",
          value = 0,
          weight = 5,
          perk = rain,
          ttl = ?RAINCHEESETTL*?FPS
         };

newCheese(N, {W, H}) when N > (100 - ?BIGCHEESECHANCE - ?SPEEDCHEESECHANCE - ?CHEESERAINCHANCE - ?BADCHEESECHANCE) ->
  #cheese{x = random:uniform(W - 20) + 10,
          y = random:uniform(H - 10) + 10,
          ref = make_ref(),
          path = "badcheese.png",
          value = -15,
          ttl = ?BADCHEESETTL*?FPS
         };

newCheese(N, {W, H}) when N =< (100 - ?BIGCHEESECHANCE - ?SPEEDCHEESECHANCE - ?CHEESERAINCHANCE - ?BADCHEESECHANCE)->
  #cheese{x = random:uniform(W - 20) + 10,
          y = random:uniform(H - 10) + 10,
          ref = make_ref(),
          path = "cheese.png",
          value = 1
         }.

updateCheeses(LoC, WH) ->
  {W, H} = WH,
  Pred1 = fun(#cheese{color = Color}) -> Color == all end,
  Pred2 = fun(#cheese{x = X, y = Y, ttl = Ttl} = Elem, Acc) -> if Ttl < 1; 
                                                                  X > W; 
                                                                  Y > H -> Acc; 
                                                                  true -> [Elem] ++ Acc 
                                                               end 
          end,
  UpdateTTL = fun(Elem) -> Elem#cheese{ttl = Elem#cheese.ttl - 1} end,
  Loc2 = lists:foldr(Pred2, [], lists:map(UpdateTTL, LoC)),
  case length(lists:filter(Pred1, Loc2)) < max(1,getMinCheeseCount(WH)) of
    true -> Loc2 ++ [newCheese(random:uniform(100), WH)];
    false -> Loc2
  end.

makeCheeses(N, WH) ->
  makeCheeses(N, [], WH).
makeCheeses(0, Loc, _) ->
  Loc;
makeCheeses(N, Loc, WH) ->
  makeCheeses(N-1, [newCheese(random:uniform(100), WH)] ++ Loc, WH).

makeGridCheeses({W, H}) -> makeGridCheeses(0, 0, W, H, []).

makeGridCheeses(X, Y, W, H, Acc) when X > W, Y > H -> Acc;
makeGridCheeses(X, Y, W, H, Acc) when X > W -> makeGridCheeses(0, Y + 50, W, H, Acc);
makeGridCheeses(X, Y, W, H, Acc) -> 
  Cheese = newCheese(random:uniform(100), {W, H}),
  NewAcc = [Cheese#cheese{x = X, y = Y} | Acc],
  makeGridCheeses(X + 50, Y, W, H, NewAcc).

cheeseGen(_, [], _) -> [];
cheeseGen(Loc, [{Bool, Color}|T], WH) ->
  case Bool of
    true  -> [newCheese(Color, WH)] ++ cheeseGen(Loc, T, WH);
    false -> cheeseGen(Loc, T, WH)
  end.

switchPlayer(World, Id) ->
  {P1,P2,P3,P4} = World#game.playing,
  case Id of
    1 -> World#game{playing = {next(P1), P2,       P3,       P4}};
    2 -> World#game{playing = {P1,       next(P2), P3,       P4}};
    3 -> World#game{playing = {P1,       P2,       next(P3), P4}};
    4 -> World#game{playing = {P1,       P2,       P3,       next(P4)}}
  end.

next(1) -> 2;
next(2) -> 3;
next(3) -> 1.

getColor(blue) -> "blue";
getColor(red) -> "red";
getColor(green) -> "green";
getColor(yellow) -> "yellow";
getColor(all) -> "".

checkCheese(World) ->
  LOC = World#game.loc,
  {P1, LC1} = checkaCheese(LOC, World#game.p1),
  {P2, LC2} = checkaCheese(LC1, World#game.p2),
  {P3, LC3} = checkaCheese(LC2, World#game.p3),
  {P4, LC4} = checkaCheese(LC3, World#game.p4),
  World#game{loc = LC4, p1 = P1, p2 = P2, p3 = P3, p4 = P4}.

checkaCheese(Loc, undefined, _) ->
  {undefined, Loc}; 

checkaCheese([], Player, Loc) ->
  {Player, Loc};

checkaCheese([H|T], Player, Loc) ->
  if
    abs(Player#player.x-H#cheese.x) =< ?RANGE, 
    abs(Player#player.y-H#cheese.y) =< ?RANGE ->
      checkaCheese(T, Player#player{score = max(0, Player#player.score + getCheeseValue(H, Player)), 
                                    perks = setPerks(H, Player)}, Loc);
    true ->
      checkaCheese(T, Player, Loc ++ [H])
  end.

checkaCheese(Loc, Player) ->
  lists:foldr(fun checkaCheeseSub/2, {Player, []}, Loc).

checkaCheeseSub(#cheese{x = Cx, y = Cy} = Cheese, {#player{x = Px, y = Py, score = Score} = Player, Loc}) 
  when abs(Px-Cx) =< ?RANGE, abs(Py-Cy) =< ?RANGE ->
  {Player#player{score = max(0, Score + getCheeseValue(Cheese, Player)),
                 perks = setPerks(Cheese, Player)},
   Loc};

checkaCheeseSub(Cheese, {Player, Loc}) ->
  {Player, [Cheese | Loc]}.
  
getCheeseValue(#cheese{color = Color, value = Value}, #player{atomColor = PColor}) ->
  case Color of
    all    -> Value;
    PColor -> Value;
    _      -> -1
  end.

getScores(#game{p1 = P1, p2 = P2, p3 = P3, p4 = P4}) ->
  collateScores([P1, P2, P3, P4]).

collateScores([]) -> [];
collateScores([undefined | T]) -> collateScores(T);
collateScores([#player{score = Score, color = Color} | T]) ->
  [{Color ++ " score", integer_to_list(Score)} | collateScores(T)].

setPerks(Cheese, Player) ->
  Perks = Player#player.perks,
  case Cheese#cheese.perk of
    undefined -> Perks;
    speed     -> Perks ++ [{speed, ?FPS*10}];
    rain      -> Perks ++ [{rain, ?CHEESERAINCOUNT*4}]
  end.

getCheese([], _) -> undefined;
getCheese([#cheese{ref = Ref} = H | _T], Ref) -> H; 
getCheese([_H|T], Ref) ->
  getCheese(T, Ref).

getMinCheeseCount(WH) ->
  {W, H} = WH,
  max(1, W*H div ?CHEESEDENSITY).

findNearest(Player, Loc, {Width, Height} = WH) ->
  {_, Cheese, _, _} = lists:foldr(fun findNearestSub/2, {Player, undefined, Width + Height, WH}, Loc),
  Cheese.

findNearestSub(#cheese{weight = Weight} = Cheese, {#player{x = Px, y = Py, color = PColor} = Player, CC, CurLow, WH}) ->
  Cx = Cheese#cheese.x + random:uniform(?ESTIMATIONNOISE*2) - ?ESTIMATIONNOISE,
  Cy = Cheese#cheese.y + random:uniform(?ESTIMATIONNOISE*2) - ?ESTIMATIONNOISE,
  {Cx2, Cy2} = cheeseLoc(Px, Py, Cx, Cy, 2, WH),
  X = erlang:abs(Px - Cx2),
  Y = erlang:abs(Py - Cy2),
  Distance = X + Y,
  AdjustedDistance = Distance div Weight,
  CColor = Cheese#cheese.color,
  SameColor = (CColor == all) or (getColor(CColor) == PColor),
  case (AdjustedDistance < CurLow) and SameColor of
    true -> {Player, Cheese, AdjustedDistance, WH};
    false -> {Player, CC, CurLow, WH}
  end.


updateFocus(#player{bot = false}, _, _) -> undefined;
updateFocus(#player{focus = undefined} = Player, Loc, WH) ->
  getNewFocus(Player, Loc, WH);
updateFocus(#player{focus = Focus} = Player, Loc, WH) ->
  case getCheese(Loc, Focus) of
    #cheese{ref = Ref} -> Ref;
    undefined -> getNewFocus(Player, Loc, WH)
  end.

getNewFocus(Player, Loc, WH) ->
  #cheese{ref = Ref} = findNearest(Player, Loc, WH),
  Ref.

botMovement(Player, Cheese, WH) ->
  Cx = Cheese#cheese.x,
  Cy = Cheese#cheese.y,
  Px = Player#player.x,
  Py = Player#player.y,
  {Cx2, Cy2} = cheeseLoc(Px, Py, Cx, Cy, 1, WH),
  Dxa = Px =< Cx2, %player to left of cheese
  Dya = Py =< Cy2, %player above cheese
  Dxb = erlang:abs(Px - Cx2) =< ?RANGE, %within range/x
  Dyb = erlang:abs(Py - Cy2) =< ?RANGE, %within range/y
  case {Dxa, Dya, Dxb, Dyb} of
    {false, false, false, false} -> 7;
    {false, false, false, true}  -> 0;
    {false, false, true,  false} -> 6;
    {false, false, true,  true}  -> 7;
    {false, true,  false, false} -> 5;
    {false, true,  false, true}  -> 6;
    {false, true,  true,  false} -> 4;
    {false, true,  true,  true}  -> 5;
    {true,  false, false, false} -> 1;
    {true,  false, false, true}  -> 2;
    {true,  false, true,  false} -> 0;
    {true,  false, true,  true}  -> 1;
    {true,  true,  false, false} -> 3;
    {true,  true,  false, true}  -> 2;
    {true,  true,  true,  false} -> 4;
    {true,  true,  true,  true}  -> 3
  end.

cheeseLoc(Px, Py, Cx, Cy, BL, {Width, Height}) ->
  Dx = Px - Cx,
  Dy = Py - Cy,
  if
    ?BOTDIFFICULTY > BL, Dx >= (Width div 2)    -> Cx2 = Cx + Width;
    ?BOTDIFFICULTY > BL, Dx*-1 >= (Width div 2) -> Cx2 = Cx - Width;
    true                                      -> Cx2 = Cx
  end,
  if
    ?BOTDIFFICULTY > BL, Dy >= (Height div 2)    -> Cy2 = Cy + Height;
    ?BOTDIFFICULTY > BL, Dy*-1 >= (Height div 2) -> Cy2 = Cy - Height;
    true                                       -> Cy2 = Cy
  end,
  {Cx2, Cy2}.

startGame(World) ->
  {P1,P2,P3,P4} = World#game.playing,
  {W, H} = World#game.size,
  case P1 of 
    1 -> NewWorld1 = World;
    2 -> NewWorld1 = World#game{p1 = #player{x=100, y=100, dir=3, color="blue", atomColor=blue}};
    3 -> NewWorld1 = World#game{p1 = #player{x=100, y=100, dir=3, color="blue", atomColor=blue, 
                                             bot=true, focus = undefined}}
  end,
  case P2 of 
    1 -> NewWorld2 = NewWorld1;
    2 -> NewWorld2 = NewWorld1#game{p2 = #player{x=W-100, y=H-100, dir=7, color="red", atomColor=red}};
    3 -> NewWorld2 = NewWorld1#game{p2 = #player{x=W-100, y=H-100, dir=7, color="red", atomColor=red, 
                                                 bot=true, focus = undefined}}
  end,
  case P3 of 
    1 -> NewWorld3 = NewWorld2;
    2 -> NewWorld3 = NewWorld2#game{p3 = #player{x=W-100, y=100, dir=5, color="green", atomColor=green}};
    3 -> NewWorld3 = NewWorld2#game{p3 = #player{x=W-100, y=100, dir=5, color="green", atomColor=green, 
                                                 bot=true, focus = undefined}}
  end,
  case P4 of 
    1 -> NewWorld4 = NewWorld3;
    2 -> NewWorld4 = NewWorld3#game{p4 = #player{x=100, y=H-100, dir=1, color="yellow", atomColor=yellow}};
    3 -> NewWorld4 = NewWorld3#game{p4 = #player{x=100, y=H-100, dir=1, color="yellow", atomColor=yellow, 
                                                 bot=true, focus = undefined}}
  end,
  NewWorld4#game{state = playing}.

drawInt(World, Canvas) ->
  case World#game.state of
    login -> drawLogin(World, Canvas);
    playing -> drawPlaying(World, Canvas);
    pause -> drawPause(World#game.size, Canvas)
  end.

drawPlaying(World, Canvas) ->
  C1 = drawCheese(World#game.loc, Canvas),
  C2 = drawPlayer(C1, World#game.p1),
  C3 = drawPlayer(C2, World#game.p2),
  C4 = drawPlayer(C3, World#game.p3),
  C5 = drawPlayer(C4, World#game.p4), 
  C5.

drawCheese([], Canvas) ->
  Canvas;
drawCheese([H|T], Canvas) ->
  Path = ?CurrentDirectory++"/cheeseold/" ++ H#cheese.path,
  Width = ?ImageWidth(Path),
  Height = ?ImageHeight(Path),
  ?DrawImage(drawCheese(T,Canvas), 
             ?Posn(H#cheese.x-(Width div 2), H#cheese.y-(Height div 2)),
             Path,
             [{mask, ?WHITE}]).

drawPlayer(Canvas, undefined) ->
  Canvas;
drawPlayer(Canvas, #player{x = X, y = Y, score = Score, dir = Dir, color = Color}) ->    
  case Dir of
    0 -> drawMouse(X-28,Y-32, Canvas, Color++"/up.png", Score);
    1 -> drawMouse(X-35,Y-28, Canvas, Color++"/upright.png", Score);
    2 -> drawMouse(X-43,Y-29, Canvas, Color++"/right.png", Score);
    3 -> drawMouse(X-53,Y-35, Canvas, Color++"/downright.png", Score);
    4 -> drawMouse(X-41,Y-44, Canvas, Color++"/down.png", Score);
    5 -> drawMouse(X-24,Y-56, Canvas, Color++"/downleft.png", Score);
    6 -> drawMouse(X-31,Y-41, Canvas, Color++"/left.png", Score);
    7 -> drawMouse(X-27,Y-26, Canvas, Color++"/upleft.png", Score)
  end.

drawMouse(X, Y, Canvas, Path, Score) ->
  ?DrawImage(?DrawText(Canvas, ?Posn(X,Y-20), integer_to_list(Score)), 
             ?Posn(X,Y), 
             ?CurrentDirectory++"/cheeseold/"++Path,
             [{mask, ?WHITE}]).

drawLogin(World, Canvas) ->
  {P1,P2,P3,P4} = World#game.playing,
  {W, H} = World#game.size,
  LoginWidth = 142,
  LoginHeight= 150,
  Current = ?CurrentDirectory,
  case P1 of
    1 -> C1 = ?DrawImage(Canvas, ?Posn(W div 2 -5 -LoginWidth, H div 2 -5 -LoginHeight), Current++"/cheeseold/login.png", [{mask, ?WHITE}]);
    2 -> C1 = ?DrawImage(Canvas, ?Posn(W div 2 -5 -LoginWidth, H div 2 -5 -LoginHeight), Current++"/cheeseold/blue/login.png", [{mask, ?WHITE}]);
    3 -> C1 = ?DrawImage(Canvas, ?Posn(W div 2 -5 -LoginWidth, H div 2 -5 -LoginHeight), Current++"/cheeseold/loginbot.png", [{mask, ?WHITE}])
  end,
  case P2 of
    1 -> C2 = ?DrawImage(C1, ?Posn(W div 2 +5, H div 2 -5 -LoginHeight), Current++"/cheeseold/login.png", [{mask, ?WHITE}]);
    2 -> C2 = ?DrawImage(C1, ?Posn(W div 2 +5, H div 2 -5 -LoginHeight), Current++"/cheeseold/red/login.png", [{mask, ?WHITE}]);
    3 -> C2 = ?DrawImage(C1, ?Posn(W div 2 +5, H div 2 -5 -LoginHeight), Current++"/cheeseold/loginbot.png", [{mask, ?WHITE}])
  end,
  case P3 of
    1 -> C3 = ?DrawImage(C2, ?Posn(W div 2 -5 -LoginWidth, H div 2 +5), Current++"/cheeseold/login.png", [{mask, ?WHITE}]);
    2 -> C3 = ?DrawImage(C2, ?Posn(W div 2 -5 -LoginWidth, H div 2 +5), Current++"/cheeseold/green/login.png", [{mask, ?WHITE}]);
    3 -> C3 = ?DrawImage(C2, ?Posn(W div 2 -5 -LoginWidth, H div 2 +5), Current++"/cheeseold/loginbot.png", [{mask, ?WHITE}])
  end,
  case P4 of
    1 -> C4 = ?DrawImage(C3, ?Posn(W div 2 +5, H div 2 +5), Current++"/cheeseold/login.png", [{mask, ?WHITE}]);
    2 -> C4 = ?DrawImage(C3, ?Posn(W div 2 +5, H div 2 +5), Current++"/cheeseold/yellow/login.png", [{mask, ?WHITE}]);
    3 -> C4 = ?DrawImage(C3, ?Posn(W div 2 +5, H div 2 +5), Current++"/cheeseold/loginbot.png", [{mask, ?WHITE}])
  end,
  C4.

drawPause({W, H}, Canvas) ->
  ?DrawText(Canvas, ?Posn((W div 2) -10, (H div 2) -10), "PAUSE").

keyInt(World, spacebar, key_down) ->
  if
    World#game.state == playing -> newGame();
    true -> World
  end;

keyInt(World, escape, key_down) ->
  worldDT:quit(World);

keyInt(#game{loc = Loc, size = WH} = World, f1, key_down) ->
  World#game{loc = Loc ++ makeCheeses(50, WH)};

keyInt(#game{loc = Loc, size = WH} = World, f3, key_down) ->
  World#game{loc = Loc ++ makeGridCheeses(WH)};

keyInt(World, enter, key_down) ->
  case World#game.state of
    playing -> World#game{state = pause};
    pause -> World#game{state = playing}
  end;

keyInt(World, Key, Type) ->
  World#game{p1 = keyPlaying(World#game.p1, Key, Type, ?KS1),
             p2 = keyPlaying(World#game.p2, Key, Type, ?KS2),
             p3 = keyPlaying(World#game.p3, Key, Type, ?KS3),
             p4 = keyPlaying(World#game.p4, Key, Type, ?KS4)}.

keyLogin(World, Key, Type, {P1,P2,P3,P4}) ->
  if
    Key == " ", Type == key_down -> startGame(World);
    Key == escape, Type == key_down -> worldDT:quit(World);
    Key == P1, Type == key_down -> switchPlayer(World, 1);
    Key == P2, Type == key_down -> switchPlayer(World, 2);
    Key == P3, Type == key_down -> switchPlayer(World, 3);
    Key == P4, Type == key_down -> switchPlayer(World, 4);
    true -> World
  end.

keyPlaying(undefined, _, _, _) ->
  undefined;

keyPlaying(Player, Key, key_up, {Up, Down, Left, Right}) ->
  if
    Key == Up    -> Newset = Player#player.keySet#keyset{up = false};
    Key == Down  -> Newset = Player#player.keySet#keyset{down = false};
    Key == Left  -> Newset = Player#player.keySet#keyset{left = false};
    Key == Right -> Newset = Player#player.keySet#keyset{right = false};
    true         -> Newset = Player#player.keySet#keyset{}
  end,
  Player#player{keySet = Newset};

keyPlaying(Player, Key, key_down, {Up, Down, Left, Right}) ->
  if
    Key == Up    -> Newset = Player#player.keySet#keyset{up = true, down = false};
    Key == Down  -> Newset = Player#player.keySet#keyset{down = true, up = false};
    Key == Left  -> Newset = Player#player.keySet#keyset{left = true, right = false};
    Key == Right -> Newset = Player#player.keySet#keyset{right = true, left = false};
    true         -> Newset = Player#player.keySet#keyset{}
  end,
  Player#player{keySet = Newset}.

tickInt(World) ->
  case World#game.state of
    login -> World;
    pause -> World;
    playing ->tickPlaying(World)
  end.

tickPlaying(World) ->
  WH = World#game.size,
  Loc = updateCheeses(World#game.loc, WH),
  {P1, B1} = updatePlayer(World#game.p1, Loc, WH),
  {P2, B2} = updatePlayer(World#game.p2, Loc, WH),
  {P3, B3} = updatePlayer(World#game.p3, Loc, WH),
  {P4, B4} = updatePlayer(World#game.p4, Loc, WH),
  NewLoc = Loc ++ cheeseGen(Loc, [{B1, blue}, 
                                  {B2, red}, 
                                  {B3, green}, 
                                  {B4, yellow}],
                            WH),
  checkCheese(World#game{loc = NewLoc,
                         p1 = P1,
                         p2 = P2,
                         p3 = P3,
                         p4 = P4
                        }).

diag_internal(World) ->
  {W, H} = World#game.size,
  [{"Width/Height", integer_to_list(W) ++ ", " ++ integer_to_list(H)},
   {"# Cheeses", integer_to_list(length(World#game.loc))}] ++
      getScores(World).