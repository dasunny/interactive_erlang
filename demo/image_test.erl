-module(image_test).

-include("idraw.hrl").
-include("iworld.hrl").

-export([start/0, on_tick/1, on_draw/2]).

start() ->
  ?IWorld(0, [{tick, fun ?MODULE:on_tick/1},
              {draw, fun ?MODULE:on_draw/2},
              {clock_rate, 1},
              {title, "rotate_test"},
              {size, {700, 300}}]).

on_tick(Angle) ->
  (Angle + 30) rem 360.

on_draw(Angle, Canvas) ->
  C1 = ?DrawImage(Canvas,
                  ?Posn(150, 150),
                  "C:/Users/etibdan/Documents/workspace/interactive_erlang/images/2.png",
                  [{rotate, Angle, ?Posn(39,54)}]),
  ?DrawImage(C1,
             ?Posn(550, 150),
             "C:/Users/etibdan/Documents/workspace/interactive_erlang/images/2.png",
             [{rotate, Angle, ?Posn(20,20)}]).
