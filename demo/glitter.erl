-module(glitter).

-include("iworld.hrl").
-include("idraw.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([start/0, on_tick/1, on_draw/2, on_mouse/4]).

-record(glitter, {position, momentum, color}).

start() ->
  ?IWorld([], [{tick,  fun ?MODULE:on_tick/1},
               {draw,  fun ?MODULE:on_draw/2},
               {mouse, fun ?MODULE:on_mouse/4},
               {clock_rate, 20},
               {title, "Glitter"},
               {size, {500, 500}}]).

on_tick(World) ->
  [update(Glitter) || #glitter{position = ?Posn(_, Y)} = Glitter <- World, Y =< 500].

on_draw(World, Canvas) ->
  Fun = fun(Elem, Acc) -> draw(Elem, Acc) end,
  lists:foldr(Fun, ?DrawRectangle(Canvas, ?Posn(0,0), 500, 500, ?WHITE), World).

on_mouse(World, X, Y, motion) ->
  [new(X, Y) | World];
on_mouse(World, _X, _Y, _) ->
  World.

%% ====================================================================
%% Internal functions
%% ====================================================================

random(A, B) ->
  random:uniform(B-A) + A.

new(X, Y) ->
  #glitter{position = ?Posn(X, Y), momentum = ?Posn(random(-3, 3), -5), color = ?COLOR(random(0, 255),
                                                                                       random(0, 255),
                                                                                       random(0, 255))}.

update(#glitter{position = Position, momentum = Momentum} = Glitter) ->
  Glitter#glitter{position = ?Translate(Position, Momentum), momentum = ?Translate(Momentum, 0, 1)}.

draw(#glitter{position = Position, color = Color}, Canvas) ->
  ?DrawRectangle(Canvas, Position, 15, 15, Color).
