-module(image_test2).

-include("idraw.hrl").
-include("iworld.hrl").

-export([start/0, on_tick/1, on_draw/2]).

start() ->
  ?IWorld(0, [{tick, fun ?MODULE:on_tick/1},
              {draw, fun ?MODULE:on_draw/2},
              {clock_rate, 1},
              {title, "composite_test"},
              {size, {400, 400}}]).

on_tick(Pos) ->
  (Pos + 1) rem 4.

on_draw(Pos, Canvas) ->
  CompositeCanvas1 = ?CompositeCanvas(100,100),
  CompositeCanvas2 = ?CompositeCanvas(100,100),
  CompositeCanvas3 = ?CompositeCanvas(100,100),
  CompositeCanvas4 = ?CompositeCanvas(100,100),
  CompositeCanvas5 = ?CompositeCanvas(100,100),

  Color = fun(N) -> case (Pos + N) rem 4 of
                      0 -> ?BLUE;
                      1 -> ?RED;
                      2 -> ?GREEN;
                      3 -> ?PURPLE
                    end end,

  CC11 = ?DrawCircle(CompositeCanvas1, ?Posn(50,50), 50, Color(0)),
  CC21 = ?DrawCircle(CompositeCanvas2, ?Posn(50,50), 50, Color(1)),
  CC31 = ?DrawCircle(CompositeCanvas3, ?Posn(50,50), 50, Color(2)),
  CC41 = ?DrawCircle(CompositeCanvas4, ?Posn(50,50), 50, Color(3)),
  CC51 = ?DrawImage(CompositeCanvas5, ?Posn(0,0), "images/square.png", [{mask, ?COLOR(195,195,195)}]),

  CC12 = ?DrawCircle(CC11, ?Posn(10,20), 50, ?RED),
  CC22 = ?DrawCircle(CC21, ?Posn(10,20), 50, ?RED),
  CC32 = ?DrawCircle(CC31, ?Posn(10,20), 50, ?RED),
  CC42 = ?DrawCircle(CC41, ?Posn(10,20), 50, ?RED),
  CC52 = ?DrawImage(CC51, ?Posn(10,10), "images/WD.bmp", [{mask, ?WHITE}]),

  C0 = ?DrawRectangle(Canvas, ?Posn(0,0), 400,400, ?MAGENTA),
  C1 = ?DrawComposite(C0, ?Posn(50,50), CC12, [{mask, ?RED}]),
  C2 = ?DrawComposite(C1, ?Posn(250,50), CC22, [{mask, ?RED}]),
  C3 = ?DrawComposite(C2, ?Posn(250,250), CC32, [{mask, ?RED}]),
  C4 = ?DrawComposite(C3, ?Posn(50,250), CC42, [{mask, ?RED}]),
  C5 = ?DrawComposite(C4, ?Posn(200,200), CC52, [{rotate, 45 + Pos*90},
                                                 {mask, ?WHITE}]),
  C5.
