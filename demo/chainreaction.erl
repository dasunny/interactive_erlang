-module(chainreaction).

-include("idraw.hrl").
-include("iworld.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([start/0, on_tick/1, on_draw/2, on_mouse/4, on_key/3, on_diag/1, do_not_remove/1, within_bounds/2, expired/1]).

-define(WIDTH, 1000).
-define(HEIGHT, 800).
-define(FPS, 20).

-define(BOMBMAXRADIUS, 50).
-define(BOMBDURATION, .5).
-define(BOMBEXPANDRATE, 10).
-define(HOLDTIME, trunc(?BOMBDURATION*?FPS)).

-define(SPAWNFREQ, 2).
-define(MAXBOMBCOUNT, 10000).

-define(COLORPATTERN, {?BLACK, ?BLUE, ?GREEN, ?CYAN, ?RED, ?MAGENTA, ?YELLOW, ?WHITE}).

-record(game, {lob = [],
               loe = [],
               score = 0,
               phase = waiting,
               mouse = {?WIDTH div 2, ?HEIGHT div 2},
               freq = 1, %?SPAWNFREQ,
               mode = 1}).

-record(bomb, {x,
               y,
               xDir,
               yDir,
               phase = a,
               color,
               time = 10,
               level = 5}).

start() ->
  ?IWorld(new_game(), [debug,
                       {draw,       fun ?MODULE:on_draw/2},
                       {tick,       fun ?MODULE:on_tick/1},
                       {key,        fun ?MODULE:on_key/3},
                       {mouse,      fun ?MODULE:on_mouse/4},
                       {diagnostic, fun ?MODULE:on_diag/1},
                       {clock_rate, ?FPS},
                       {size, {?WIDTH, ?HEIGHT}},
                       {title, "Chain Reaction"}
                      ]).

new_game() ->
  #game{}.

on_draw(World, Canvas) ->
  draw_internal(World, Canvas).

on_tick(World) ->
  tick_internal(World).

on_mouse(World, X, Y, Type) ->
  mouse_internal(World, X, Y, Type).

on_key(World, Key, Type) ->
  key_internal(World, Key, Type).

on_diag(World) ->
  diag_internal(World).
%% ====================================================================
%% Internal functions
%% ====================================================================

%Returns true if the bomb has either expired or is out of bounds.
do_not_remove(Bomb) ->
  within_bounds(Bomb, 5) and (not expired(Bomb)).

do_not_remove2(Bomb) ->
  within_bounds(Bomb, 15) and (not expired(Bomb)).

within_bounds(Bomb, Dist) ->
  X = Bomb#bomb.x,
  Y = Bomb#bomb.y,
  (X < ?WIDTH + Dist)      and 
      (X > -Dist)          and 
      (Y < ?HEIGHT + Dist) and
      (Y > -Dist).

expired(Bomb) ->
  (Bomb#bomb.phase == d) and (Bomb#bomb.level =< 0).


%Updates a bomb based on tick events.
update_bomb(#bomb{phase = a} = Bomb) ->
  Bomb#bomb{x = Bomb#bomb.x + Bomb#bomb.xDir,
            y = Bomb#bomb.y + Bomb#bomb.yDir};
update_bomb(#bomb{phase = b} = Bomb) ->
  case Bomb#bomb.level < ?BOMBMAXRADIUS of
    true  -> Bomb#bomb{level = Bomb#bomb.level + ?BOMBEXPANDRATE};
    false -> Bomb#bomb{phase = c, level = ?BOMBMAXRADIUS}
  end;
update_bomb(#bomb{phase = c} = Bomb) ->
  case Bomb#bomb.time > 0 of
    true  -> Bomb#bomb{time = Bomb#bomb.time -1};
    false -> Bomb#bomb{phase = d}
  end;
update_bomb(#bomb{phase = d} = Bomb) ->
  Bomb#bomb{level = Bomb#bomb.level - ?BOMBEXPANDRATE}.


update_bomb2(#bomb{x = X, y = Y, xDir = XD, yDir = YD, phase = a} = Bomb) ->
  if
    (X < 0) or (X > ?WIDTH) ->  Bomb#bomb{x = X - 2*XD, y = Y + YD, xDir = -XD};
    (Y < 0) or (Y > ?HEIGHT) -> Bomb#bomb{x = X + XD, y = Y - 2*YD, yDir = -YD};
    true ->                     Bomb#bomb{x = X + XD,
                                          y = Y + YD}
  end;

update_bomb2(#bomb{phase = b} = Bomb) ->
  case Bomb#bomb.level < ?BOMBMAXRADIUS of
    true  -> Bomb#bomb{level = Bomb#bomb.level + ?BOMBEXPANDRATE};
    false -> Bomb#bomb{phase = c, level = ?BOMBMAXRADIUS}
  end;
update_bomb2(#bomb{phase = c} = Bomb) ->
  case Bomb#bomb.time > 0 of
    true  -> Bomb#bomb{time = Bomb#bomb.time -1};
    false -> Bomb#bomb{phase = d}
  end;
update_bomb2(#bomb{phase = d} = Bomb) ->
  Bomb#bomb{level = Bomb#bomb.level - ?BOMBEXPANDRATE}.

%Updates a list of bombs.
update_bombs(Lob, Loe, 1) ->
  FilteredBombs = lists:filter(fun do_not_remove/1, Lob),
  UpdatedBombs = lists:map(fun update_bomb/1, FilteredBombs),
  FilteredExps = lists:filter(fun do_not_remove/1, Loe),
  UpdatedExps = lists:map(fun update_bomb/1, FilteredExps),
  compare_bombs(UpdatedBombs, UpdatedExps, [], []);

update_bombs(Lob, Loe, 2) ->
  FilteredBombs = lists:filter(fun do_not_remove2/1, Lob),
  UpdatedBombs = lists:map(fun update_bomb2/1, FilteredBombs),
  FilteredExps = lists:filter(fun do_not_remove2/1, Loe),
  UpdatedExps = lists:map(fun update_bomb2/1, FilteredExps),
  compare_bombs(UpdatedBombs, UpdatedExps, [], []).

%Updates the count of total exploded bombs.
score_tally1(Score, Loe) ->
  Score + length(lists:filter(fun score_tally2/1, Loe)).
score_tally2(Bomb) ->
  (Bomb#bomb.phase == b) and
      (Bomb#bomb.level == 5).

%Switches a bomb to explode.
explode(Bomb, C) ->
  Fun = fun(C1, C2) -> max(0, min(255, C1-1+(C2 div 255)*2)) end,
  ?COLOR(R1, G1, B1) = C,
  ?COLOR(R2, G2, B2) = Bomb#bomb.color,
  Bomb#bomb{phase = b, color = ?COLOR(Fun(R1, R2), Fun(G1, G2), Fun(B1, B2))}.

random(N) ->
  random:uniform(N).

%Spawns more bombs.
spawn_bombs(0) -> [];
spawn_bombs(N) ->
  [spawn_bomb(random(8)) | spawn_bombs(N-1)].

%Spawns an individual bomb.
spawn_bomb(1) ->
  #bomb{x = random(?WIDTH div 2) + (?WIDTH div 2),
        y = 0,
        xDir = random(20) -10,
        yDir = random(10),
        color = element(1, ?COLORPATTERN)};
spawn_bomb(2) ->
  #bomb{x = ?WIDTH,
        y = random(?HEIGHT div 2),
        xDir = -random(10),
        yDir = random(20) -10,
        color = element(2, ?COLORPATTERN)};
spawn_bomb(3) ->
  #bomb{x = ?WIDTH,
        y = random(?HEIGHT div 2) + (?HEIGHT div 2),
        xDir = -random(10),
        yDir = random(20) -10,
        color = element(3, ?COLORPATTERN)};
spawn_bomb(4) ->
  #bomb{x = random(?WIDTH div 2) + ?WIDTH div 2,
        y = ?HEIGHT,
        xDir = random(20) -10,
        yDir = -random(10),
        color = element(4, ?COLORPATTERN)};
spawn_bomb(5) ->
  #bomb{x = random(?WIDTH div 2),
        y = ?HEIGHT,
        xDir = random(20) -10,
        yDir = - random(10),
        color = element(5, ?COLORPATTERN)};
spawn_bomb(6) ->
  #bomb{x = 0,
        y = random(?HEIGHT div 2) + ?HEIGHT div 2,
        xDir = random(10),
        yDir = random(20) -10,
        color = element(6, ?COLORPATTERN)};
spawn_bomb(7) ->
  #bomb{x = 0,
        y = random(?HEIGHT div 2),
        xDir = random(10),
        yDir = random(20) -10,
        color = element(7, ?COLORPATTERN)};
spawn_bomb(8) ->
  #bomb{x = random(?WIDTH div 2),
        y = 0,
        xDir = random(20) -10,
        yDir = random(10),
        color = element(8, ?COLORPATTERN)}.

%Adds an exploding bomb to the list of bombs
add_trigger(Loe, {X, Y}) ->
  Bomb = #bomb{x = X,
               y = Y,
               xDir = 0,
               yDir = 0,
               phase = b,
               color = ?BLACK},
  [Bomb | Loe].

%Returns true if two bombs are within range of each other.
within_range(B1, B2) ->
  X1 = B1#bomb.x,   Y1 = B1#bomb.y,
  X2 = B2#bomb.x,   Y2 = B2#bomb.y,
  Dx = X1 - X2,     Dy = Y1 - Y2,
  Range = B1#bomb.level + B2#bomb.level,
  math:sqrt(Dx*Dx + Dy*Dy) =< Range.

compare_bombs([], Loie, Loab, Loae) -> {Loab, Loae ++ Loie};
compare_bombs([H|T] = _Loib, Loie, Loab, Loae) ->
  {NewBomb, NewExp} = compare_bomb(H, Loie),
  compare_bombs(T, Loie, 
                NewBomb ++ Loab, 
                NewExp ++ Loae).

compare_bomb(Bomb, []) -> {[Bomb], []}; 
compare_bomb(Bomb, [H|T]) ->
  C = H#bomb.color,
  case within_range(Bomb, H) of
    true  -> {[], [explode(Bomb, C)]};
    false -> compare_bomb(Bomb, T)
  end.

%Internal Draw event handler.
draw_internal(World, Canvas) ->
  C0 = ?DrawRectangle(Canvas, ?Posn(0,0), ?WIDTH, ?HEIGHT, ?WHITE),
  C1 = draw_bombs(C0, World#game.lob ++ World#game.loe),
  C2 = draw_debug(C1, World),
  C3 = draw_score(C2, World#game.score),
  draw_border(C3).

%Draws some debug info.
draw_debug(Canvas, World) ->
  {X, Y} = World#game.mouse,
  ?DrawText(Canvas, ?Posn(10, 10), integer_to_list(X) ++ ", " ++ integer_to_list(Y)).

%Draws the player score.
draw_score(Canvas, Score) ->
  ?DrawText(Canvas, ?Posn(10, ?HEIGHT-30), "Score: " ++ integer_to_list(Score)).

%Draws the color border around the screen.
draw_border(C0) ->
  C1 = ?DrawRectangle(C0, ?Posn(0,            0),             ?WIDTH div 2, 4,             element(8, ?COLORPATTERN)),
  C2 = ?DrawRectangle(C1, ?Posn(0,            0),             4,            ?HEIGHT div 2, element(7, ?COLORPATTERN)),
  C3 = ?DrawRectangle(C2, ?Posn(0,            ?HEIGHT div 2), 4,            ?HEIGHT div 2, element(6, ?COLORPATTERN)),
  C4 = ?DrawRectangle(C3, ?Posn(0,            ?HEIGHT -4),    ?WIDTH div 2, 4,             element(5, ?COLORPATTERN)),
  C5 = ?DrawRectangle(C4, ?Posn(?WIDTH div 2, ?HEIGHT -4),    ?WIDTH div 2, 4,             element(4, ?COLORPATTERN)),
  C6 = ?DrawRectangle(C5, ?Posn(?WIDTH -4,    ?HEIGHT div 2), 4,            ?HEIGHT div 2, element(3, ?COLORPATTERN)),
  C7 = ?DrawRectangle(C6, ?Posn(?WIDTH -4,    0),             4,            ?HEIGHT div 2, element(2, ?COLORPATTERN)),
  C8 = ?DrawRectangle(C7, ?Posn(?WIDTH div 2, 0),             ?WIDTH div 2, 4,             element(1, ?COLORPATTERN)),
  C8.

draw_bombs(Canvas, Bombs) ->
  lists:foldr(fun draw_bomb/2, Canvas, Bombs).
%Draws a bomb.
draw_bomb(#bomb{color = ?COLOR(R, G, B), x = X, y = Y, level = Level}, Canvas) ->
  NC = ?COLOR(trunc(R), trunc(G), trunc(B)),
  ?DrawCircle(Canvas, ?Posn(X, Y), Level, NC).

%Internal Tick event handler.
tick_internal(World) ->
  {Nlob, Nloe} = update_bombs(World#game.lob, World#game.loe, World#game.mode),
  NumToSpawn = min(World#game.freq, ?MAXBOMBCOUNT - length(Nlob)),
  World#game{lob = spawn_bombs(NumToSpawn) ++ Nlob,
             loe = Nloe,
             score = score_tally1(World#game.score, World#game.loe)}.

%Internal Key event handler.
key_internal(World, Key, Type) ->
  key_int2(World, Key, Type, World#game.phase).

key_int2(_, " ", key_down, running) -> new_game();
key_int2(World, " ", key_down, waiting) -> World#game{lob = add_trigger(World#game.lob, World#game.mouse),
                                                           phase = running};
key_int2(World, escape, key_down, _) -> interactive_erlang:quit(World);
key_int2(World, down, key_down, _) -> World#game{freq = max(0, World#game.freq - 1)};
key_int2(World, up, key_down, _) -> World#game{freq = min(10, World#game.freq + 1)};

key_int2(World, "1", key_down, _) -> World#game{mode = 1};
key_int2(World, "2", key_down, _) -> World#game{mode = 2};

key_int2(World, _, _, _) -> World.

%Internal Mouse event handler.
mouse_internal(World, X, Y, Type) ->
  mouse_int2(World, X, Y, Type, World#game.phase).

%Secondary, per case, mouse event handlers.
mouse_int2(World, X, Y, motion, _) ->
  World#game{mouse = {X, Y}};
mouse_int2(World, X, Y, left_down, waiting) ->
  World#game{loe = add_trigger(World#game.loe, {X, Y})};
mouse_int2(World, _, _, _, _) -> World.

diag_internal(World) ->
  {Mx, My} = World#game.mouse,
  [{"# Bombs", integer_to_list(length(World#game.lob))},
   {"# Exps", integer_to_list(length(World#game.loe))},
   {"Spawn Freq (bombs/sec)", integer_to_list(World#game.freq*20)},
   {"Score", integer_to_list(World#game.score)},
   {"Mouse", integer_to_list(Mx)++", "++integer_to_list(My)},
   {"Mode", integer_to_list(World#game.mode)}].
