-module(fireplane).

-include("idraw.hrl").
-include("iworld.hrl").
-include("imenu.hrl").

-record(mouse, {x, y, state}).
-record(flame, {x, str, mode, lop}).
-record(wdrop, {pos, mom, mode, str, range}).
-record(puff, {pos, sideMomA, sideMomB, vertMom}).
-record(heli, {pos, mom, dir, waterA, waterB, phase}).
-record(game, {lof, lod, heli, key, drawPuffs, waterMode, mouse}).

-define(HEIGHT, 500).
-define(WIDTH, 1000).
-define(HELIHEIGHT, 100).
-define(SMOKELIFE, 300).
-define(CURDIR, ?CurrentDirectory ++ "/..").
-define(GRAVITY, 5).
-define(TOLERANCE, 10).
-define(NUMFLAMES, 10).
-define(VARIANCE, 5).
-define(MAXAMMO, 15).
-define(SUBAMMO, 5).
-define(HORZDRAFTDISTANCE, 175).
-define(VERTDRAFTDISTANCE, 200).
-define(MAXHELIMOM, 15).
-define(PUFFRISERATE, 3).
-define(REFILLMIN, 150).
-define(REFILLMAX, 250).
-define(FIREGROWTHRATE, 1).


%% ====================================================================
%% API functions
%% ====================================================================

-export([start/0, on_draw/2, on_tick/1, on_key/3, on_menu/0, on_mouse/4,
         on_menuEvent/2, new_game/0]).

%Starts a new game.
start() ->
  ?IWorld(new_game(), [{draw, fun ?MODULE:on_draw/2},
                       {tick, fun ?MODULE:on_tick/1},
                       {key,  fun ?MODULE:on_key/3},
                       {menu, fun ?MODULE:on_menu/0},
                       %{mouse, on_mouse},
                       debug,
                       {clock_rate, 20},
                       {menu_event, fun ?MODULE:on_menuEvent/2},
                       {title, "Fire Plane!"},
                       {size, {?WIDTH, ?HEIGHT}}
                      ]).	

on_draw(World, Canvas) ->
  draw_internal(World, ?DrawRectangle(Canvas, ?Posn(0,0), ?WIDTH, ?HEIGHT, ?WHITE)).

on_tick(World) ->
  tick_internal(World).

on_key(World, Key, Type) ->
  key_internal(World,Key,Type).

on_menu() ->
  build_menu().

on_menuEvent(World, Id) ->
  menuEvent(World, Id).

on_mouse(World, X, Y, Type) ->
  mouse_internal(World, X, Y, Type).

%% ====================================================================
%% Internal functions
%% ====================================================================
displace(#posn{x = X, y = Y}, Dx, Dy) ->
  #posn{x = X + Dx,
        y = Y + Dy}.

absdiv(0) -> 0;
absdiv(X) -> abs(X) div X.

% Updates a single fire. 
update_fire(#flame{mode = fire, str = Str} = Flame, Mod) ->
  Flame#flame{str = Str + Mod};
update_fire(#flame{mode = smoke} = Flame, _Mod) ->
  Flame.

update_fire(#flame{mode = fire, str = Str} = Flame, _Mod, _Heli) when Str =< 0->
  Flame#flame{mode = smoke, str = ?SMOKELIFE};
update_fire(#flame{mode = fire, str = Str} = Flame, Mod, _Heli) ->
  Flame#flame{str = Str + Mod};
update_fire(#flame{str = 0, mode = smoke, lop = []} = Flame, _Mod, _Heli) ->
  Flame#flame{mode = done};
update_fire(#flame{str = 0, mode = smoke, lop = Lop} = Flame, _Mod, Heli) ->
  Flame#flame{lop = draft_puffs(update_puffs(Lop), Heli)};
update_fire(#flame{x = X, mode = smoke, str = Str, lop = Lop} = Flame, _Mod, Heli) ->
  Flame#flame{str = Str - 1, lop = draft_puffs(update_puffs(add_puff(Lop, X)), Heli)};
update_fire(Flame, _Mod, _Heli) ->
  Flame.

% Updates a single water drop.
update_drop(#wdrop{pos = #posn{y = ?HEIGHT}, mode = drop} = Drop) ->
  Drop#wdrop{mode = splash, str = 11};
update_drop(#wdrop{pos = #posn{x = X} = Pos, mom = Mom, mode = drop} = Drop) when X < 0->
  Drop#wdrop{pos = displace(Pos, Mom + ?WIDTH, ?GRAVITY)};
update_drop(#wdrop{pos = #posn{x = X} = Pos, mom = Mom, mode = drop} = Drop) when X > ?WIDTH ->
  Drop#wdrop{pos = displace(Pos, Mom - ?WIDTH, ?GRAVITY)};
update_drop(#wdrop{pos = Pos, mom = Mom, mode = drop} = Drop) ->
  Drop#wdrop{pos = displace(Pos, Mom, ?GRAVITY)};
update_drop(#wdrop{mode = splash, str = Str} = Drop) when Str =< 0 ->
  Drop#wdrop{mode = done};
update_drop(#wdrop{mode = splash, str = Str} = Drop) ->
  Drop#wdrop{str = Str - 2}.

%Updates a single smoke puff.
update_puff(#puff{pos = Pos, sideMomA = 0, sideMomB = 0, vertMom = 0} = Puff) -> 
  Puff#puff{pos = displace(Pos, 0, -?PUFFRISERATE)};
update_puff(#puff{pos = Pos, sideMomA = MomA, sideMomB = MomB, vertMom = VMom} = Puff) ->
  N    = 10,
  Rand = random:uniform(N),
  case Rand of
    N -> MomAdd = MomA - absdiv(MomA);
    _ -> MomAdd = MomA
  end,
  Puff#puff{pos = displace(Pos, MomA + MomB, VMom - ?PUFFRISERATE),
            sideMomA = MomAdd,
            sideMomB = MomB div 2,
            vertMom = VMom - absdiv(VMom)}.

% Updates the list of fires in the game.
update_fires(Lof, Heli) ->
  [update_fire(F, ?FIREGROWTHRATE, Heli) || F <- remove_flame(Lof)].

% Updates the list of drops in the game.
update_drops(Lod) ->
  lists:map(fun update_drop/1, remove_wdrop(Lod)).

% Updates the list of puffs in the game.
update_puffs(Lop) ->
  lists:map(fun update_puff/1, remove_puff(Lop)).

%Compares all droplets to all fires to check if they are within range to have an effect.
compare_fires_to_drops(Lof, Lod) ->
  [compare_fire_to_drops(Fire, Lod) || Fire <- Lof].

%Compares a single wdrop against a list of fires (Lof). If they are within range of eachother, the fire is diminished.
compare_fire_to_drops(#flame{mode = fire} = Fire, Lod) -> 
  lists:foldl(fun compare_fire_to_drop/2, Fire, Lod);
compare_fire_to_drops(Fire, _Lod) -> Fire.

%Checks a wdrop's x position against a flame's x position. Returns true if they are within 10 units of eachother.
compare_fire_to_drop(#wdrop{str = Str} = Drop, Fire) ->
  case can_affect(Fire, Drop) of
    true  -> update_fire(Fire, -Str);
    false -> Fire
  end.

%Checks to see if a given wdrop is at ground level.
can_affect(#flame{mode = Mode}, _Drop) when Mode == smoke; Mode == done -> false;
can_affect(#flame{x = Fx, str = Fstr} = Flame, #wdrop{pos = #posn{x = Dx, y = Dy}, range = Range}) ->
  Fun = fun(TFx) -> (Dy >= (?HEIGHT-5)) and (abs(Dx - TFx) =< Fstr + Range) end,
  Fun(Fx) orelse (case flame_overlap(Flame) of
                    false -> false;
                    NFx     -> Fun(NFx)
                  end).

flame_overlap(#flame{x = Fx, str = Fstr}) when (Fx =< (?WIDTH div 2)) and (Fstr > Fx) ->
  Fx + ?WIDTH;
flame_overlap(#flame{x = Fx, str = Fstr}) when (Fx > (?WIDTH div 2)) and ((Fstr + Fx) > ?WIDTH) ->
  Fx - ?WIDTH;
flame_overlap(_Flame) ->
  false.

%Removes wdrops from the list if they are below the screen.
remove_wdrop(Lod) ->
  Fun = fun(Elem) -> not (Elem#wdrop.mode == done) end,
  lists:filter(Fun, Lod).

%Removes flames from the list if their str field is less or equal to 0.
remove_flame(Lof) ->
  Fun = fun(Elem) -> not (Elem#flame.mode == done) end,
  lists:filter(Fun, Lof).

%Removes puffs from the list if they are above the screen.
remove_puff(Lop) ->
  Fun = fun(#puff{pos = #posn{y = Y}}) -> Y > -25 end,
  lists:filter(Fun, Lop).

%Spawns a water drop with the same momentum as the heli.
spawn_wdrop(Lod, Mom, Pos) ->
  [#wdrop{pos = #posn{x = Pos, y = ?HELIHEIGHT},
          mom = trunc(Mom/3),
          mode = drop,
          str = 20,
          range = 10} | Lod].

%Adds a smoke puff to the list of smoke puffs.
add_puff(Lop, Pos) ->
  R = 3,
  N = random:uniform(R),
  case N of
    R -> [#puff{pos = #posn{x = Pos, y = ?HEIGHT}, 
                sideMomA = (random:uniform(5) - 3),
                sideMomB = 0,
                vertMom = 0}| Lop];
    _ -> Lop
  end.

get_draft(HeliX, PuffX, PuffY) when abs(HeliX - PuffX) > ?HORZDRAFTDISTANCE;
                                    (PuffY - ?HELIHEIGHT) > ?VERTDRAFTDISTANCE -> {0, 0}; 
get_draft(HeliX, PuffX, PuffY) ->
  Dx = get_draft_sub(HeliX, PuffX, 25, ?HORZDRAFTDISTANCE),
  Dy = get_draft_sub(?HELIHEIGHT, PuffY, 100, (?HEIGHT - ?HELIHEIGHT)),
  {Dx, Dy}.

get_draft_sub(HeliV, PuffV, Div, Max) ->
  Dv = PuffV - HeliV,
  D1 = (Dv div Div) + absdiv(Dv),
  absdiv(D1)*(Max div Div) - D1.

%Adds momentum to a puff
draft_puff(#puff{pos = #posn{x = PuffX, y = PuffY}, sideMomB = SMom, vertMom = VMom} = Puff, HeliX) ->
  {Dx, Dy} = get_draft(HeliX, PuffX, PuffY),
  Puff#puff{sideMomB = SMom + Dx, vertMom = VMom + Dy}.

draft_puffs(Lop, #heli{pos = HeliX}) ->
  [draft_puff(P, HeliX) || P <- Lop].

%Decreases the heli's drop count by 1.
use_drop(#heli{waterA = WA} = Heli) ->
  Heli#heli{waterA = WA - 1}.

%Returns true if the heli still has some water left.
has_drop(Heli) ->
  Heli#heli.waterA > 0.

all_out(Lof) ->
  Fun = fun(#flame{mode = Mode}, Acc) -> Acc or (Mode == fire) end,
  not (lists:foldl(Fun, false, Lof)).

%Spawns the initial starting fires.
spawn_flames() ->
  spawn_flames(random:uniform(?NUMFLAMES) + ?VARIANCE).

spawn_flames(0) ->
  [];
spawn_flames(N) ->
  [spawn_flame() | spawn_flames(N - 1)].

%Generates a random flame.
spawn_flame() ->
  #flame{x = random:uniform(?WIDTH), 
         str = random:uniform(45) + 5,
         mode = fire,
         lop = []}.

%Refills the heli.
refill(#heli{waterA = ?MAXAMMO} = Heli) -> Heli;
refill(#heli{waterA = WaterA, waterB = ?SUBAMMO} = Heli) -> Heli#heli{waterA = WaterA + 1, waterB = 0};
refill(#heli{waterB = WaterB} = Heli) -> Heli#heli{waterB = WaterB + 1}.

%Checks the heli's water count, spawns a wdrop and depletes the heli's water count if it can.
drop_water(Game) ->
  case has_drop(Game#game.heli) of
    true ->
      Game#game{lod = spawn_wdrop(Game#game.lod, Game#game.heli#heli.mom, Game#game.heli#heli.pos),
                heli = use_drop(Game#game.heli)
               };
    false -> Game
  end.

%Checks if the heli is within the refill zone.
near_refill(#heli{pos = Pos}) ->
  (Pos =< ?REFILLMAX) and (Pos >= ?REFILLMIN).

%Updates the heli's position.
update_heliPos(#heli{pos = Pos, mom = Mom}) when Pos < 0, Mom =< 0 -> ?WIDTH; 
update_heliPos(#heli{pos = Pos, mom = Mom}) when Pos > ?WIDTH, Mom >= 0 -> 0;
update_heliPos(#heli{pos = Pos, mom = Mom}) -> Pos + Mom.

%Updates the heli's momentum.
update_heliMom(#heli{mom = 0}, #game{key = o}) -> 0;
update_heliMom(#heli{mom = Mom}, #game{key = o}) ->
  MomAdd = absdiv(Mom),
  Mom - MomAdd;
update_heliMom(#heli{mom = ?MAXHELIMOM}, #game{key = f}) -> ?MAXHELIMOM;
update_heliMom(#heli{mom = Mom}, #game{key = f}) -> Mom + 1;
update_heliMom(#heli{mom = -?MAXHELIMOM}, #game{key = r}) -> -?MAXHELIMOM;
update_heliMom(#heli{mom = Mom}, #game{key = r}) -> Mom - 1.

%Updates heli image.
update_heliPhase(#heli{dir = f, phase = fa}) -> fb;
update_heliPhase(#heli{dir = f, phase = fb}) -> fa;
update_heliPhase(#heli{dir = f, phase = _})  -> fa;
update_heliPhase(#heli{dir = r, phase = ra}) -> rb;
update_heliPhase(#heli{dir = r, phase = rb}) -> ra;
update_heliPhase(#heli{dir = r, phase = _})  -> ra.

%Updates heli direction.
update_heliDir(_Heli, #game{key = f}) -> f;
update_heliDir(_Heli, #game{key = r}) -> r;
update_heliDir(#heli{dir = Dir}, #game{key = o}) -> Dir.

%Updates the heli.
update_heli(Heli, Game) ->
  NewHeli = Heli#heli{pos = update_heliPos(Heli),
                      mom = update_heliMom(Heli, Game),
                      dir = update_heliDir(Heli, Game),
                      phase = update_heliPhase(Heli)},
  case near_refill(NewHeli) of
    true -> refill(NewHeli);
    false -> NewHeli
  end.
make_statusBarText(N) ->
  integer_to_list(N) ++ " Drops (" ++ integer_to_list(trunc((N / ?MAXAMMO)* 100)) ++ "%)".

%Updates the game every tick.
tick_internal(#game{heli = Heli, lof = Lof, lod = Lod} = Game) ->
  ?RevertStatusBar,
  ?ChangeStatusBar("Water tank level: " ++ make_statusBarText(Heli#heli.waterA)),
  Game#game{
            lof = compare_fires_to_drops(update_fires(Lof, Heli), Lod),
            lod = update_drops(Lod),
            heli = update_heli(Heli, Game)
           }.

%Creates a new game.
new_game() ->
  #game{
        lof = spawn_flames(), 
        lod = [],
        heli = #heli{pos = 200, mom = 0, dir = f, waterA = ?MAXAMMO, waterB = 0, phase = fa},
        key = o,
        drawPuffs = true,
        waterMode = drop,
        mouse = #mouse{x = ?WIDTH div 2, y = ?HEIGHT div 2, state = up}}.

%Handles all menu events.
menuEvent(_World, 11) -> new_game();
menuEvent(World, 12) -> interactive_erlang:quit(World);
menuEvent(World, 21) -> ?SpawnDialogBox("Written by Daniel Tibbetts for practice in Erlang. "
                                        "Use the arrow keys to maneuver your helicopter from side to "
                                        "side and spacebar to drop water bombs."),
                                World.

%Builds the menu of the game.
build_menu() ->
  ?AddStatusBar("Water tank level:"),
  ?ChangeStatusBar("Water tank level: | | | | | | | | | | | | | | |"),
  ?MenuBar([?Menu("&File", [?MenuButtonItem("&New Game", 11),
                            ?MenuSeparatorItem,
                            ?MenuButtonItem("&Quit", 12)]),
            ?Menu("&Help", [?MenuButtonItem("&About", 21)])]).

key_internal(Game, left,     key_down) -> Game#game{key = r};
key_internal(Game, left,     key_up)   -> Game#game{key = o};
key_internal(Game, right,    key_down) -> Game#game{key = f};
key_internal(Game, right,    key_up)   -> Game#game{key = o};
key_internal(#game{drawPuffs = DP} = Game, f1, key_down) -> Game#game{drawPuffs = not DP};
key_internal(Game, " ", key_down) -> drop_water(Game);
key_internal(Game, _Key, _Type) -> Game.

mouse_internal(#game{mouse = Mouse} = Game, X, Y, left_down) -> 
  Game#game{mouse = Mouse#mouse{x = X, y = Y, state = true}};
mouse_internal(#game{mouse = Mouse} = Game, X, Y, left_up) -> 
  Game#game{mouse = Mouse#mouse{x = X, y = Y, state = false}};
mouse_internal(#game{mouse = Mouse} = Game, X, Y, motion) -> 
  Game#game{mouse = Mouse#mouse{x = X, y = Y}}.

draw_a_drop(#wdrop{mode = drop, pos = ?Posn(X, Y)}, Canvas) ->
  ?DrawImage(Canvas, 
                  ?Posn(X - 14, Y),
                  ?CURDIR ++ "/images/WD.bmp", 
                  [{mask, ?WHITE}]);
draw_a_drop(#wdrop{mode = splash, pos = ?Posn(X, _Y), str = Str}, Canvas) ->
  ?DrawCircle(Canvas, ?Posn(X, ?HEIGHT), Str*4, ?BLUE);
draw_a_drop(#wdrop{mode = done}, Canvas) ->
  Canvas.

draw_drops(Canvas, Lod) -> 
  lists:foldr(fun draw_a_drop/2, Canvas, Lod).

draw_a_puff(#puff{pos = #posn{x = X}}, Canvas) when X < -60; X > ?WIDTH + 60 ->
  Canvas;
draw_a_puff(#puff{pos = #posn{x = X, y = Y}}, Canvas) ->
  G = 255 - (max(60, Y) div 2),
  ?DrawCircle(Canvas, ?Posn(X, Y), (60- Y div 8) + 5, ?COLOR(G,G,G)).

draw_puffs(Canvas, Lop) -> 
  lists:foldr(fun draw_a_puff/2, Canvas, Lop).

draw_a_fire(#flame{x = X, str = Str, mode = fire} = Flame, Canvas, _) ->
  Fun = fun(Fx, C) -> ?DrawCircle(C, ?Posn(Fx, ?HEIGHT), Str, ?ORANGE) end,
  C2 = case flame_overlap(Flame) of
         false -> Canvas;
         NFx     -> Fun(NFx, Canvas)
       end,
  Fun(X, C2);
draw_a_fire(#flame{mode = smoke, lop = Lop}, Canvas, true) ->
  draw_puffs(Canvas, Lop);
draw_a_fire(#flame{mode = smoke}, Canvas, false) ->
  Canvas;
draw_a_fire(#flame{mode = done}, Canvas, _) ->
  Canvas.

draw_fire(Canvas, Lof, DrawPuff) ->
  Fun = fun(Elem, Acc) -> draw_a_fire(Elem, Acc, DrawPuff) end,
  lists:foldr(Fun, Canvas, Lof).

draw_heli(Canvas, #heli{pos = X, phase = Phase}) ->
  case Phase of
    ra -> ?DrawImage(Canvas, ?Posn(X-44, ?HELIHEIGHT),  ?CURDIR ++ "/images/helilb.bmp", [{mask, ?WHITE}]);
    rb -> ?DrawImage(Canvas, ?Posn(X-97, ?HELIHEIGHT),  ?CURDIR ++ "/images/helilf.bmp", [{mask, ?WHITE}]);
    fa -> ?DrawImage(Canvas, ?Posn(X-111, ?HELIHEIGHT), ?CURDIR ++ "/images/helirb.bmp", [{mask, ?WHITE}]);
    fb -> ?DrawImage(Canvas, ?Posn(X-111, ?HELIHEIGHT), ?CURDIR ++ "/images/helirf.bmp", [{mask, ?WHITE}])
  end.

draw_refill(Canvas) ->
  ?DrawRectangle(
    ?DrawText(Canvas, ?Posn(?REFILLMIN,?HELIHEIGHT-30), "Refill Station"), 
    ?Posn(?REFILLMIN,?HELIHEIGHT-10), 
    (?REFILLMAX-?REFILLMIN), 10, ?BLUE).

drawBorder(Canvas) ->
  C1 = ?DrawLine(Canvas, ?Posn(0,0),            ?Posn(?WIDTH,0),       ?BLACK, 1),
  C2 = ?DrawLine(C1,     ?Posn(?WIDTH,0),       ?Posn(?WIDTH,?HEIGHT), ?BLACK, 1),
  C3 = ?DrawLine(C2,     ?Posn(?WIDTH,?HEIGHT), ?Posn(0,?HEIGHT),      ?BLACK, 1),
  ?DrawLine(C3,          ?Posn(0,?HEIGHT),      ?Posn(0,0),            ?BLACK, 1).

draw_win(Canvas) ->
  C1 = ?DrawText(Canvas, ?Posn(300, 200), "The fires are out!"),
  C2 = ?DrawText(C1,     ?Posn(300, 250), "You win!"),
  C3 = ?DrawText(C2,     ?Posn(300, 300), "You can start a new game from the menu"),
  C3.

draw_internal(#game{heli = Heli, lof = Lof, lod = Lod, drawPuffs = DP}, Canvas) ->
  C1 = case all_out(Lof) of 
         true -> draw_win(Canvas); 
         _ -> Canvas
       end,
  C2 = draw_fire(  C1, Lof, DP),
  C3 = draw_drops( C2, Lod),
  C4 = draw_heli(  C3, Heli),
  C5 = draw_refill(C4),
  drawBorder(C5).
