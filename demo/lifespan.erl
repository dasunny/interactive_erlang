-module(lifespan).

-include("idraw.hrl").
-include("iworld.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([start/0, on_tick/1, on_draw/2]).

-define(WIDTH, 1500).
-define(HEIGHT, 800).
-define(FPS, 20).
-define(RADIUS, 10).
-define(COOLDOWN, 2*?FPS).
-define(LIFE, 15*?FPS).

-record(life, {pos,
               dir,
               color,
               cooldown = ?FPS,
               me = make_ref(),
               parent1,
               parent2,
               siblings,
               life = ?LIFE}).

start() ->
  random:seed(now()),
  ?IWorld(new_game(), [debug,
                       {draw,       fun ?MODULE:on_draw/2},
                       {tick,       fun ?MODULE:on_tick/1},
                       {clock_rate, ?FPS},
                       {size, {?WIDTH, ?HEIGHT}},
                       {title, "LifeSpan"}
                      ]).

new_game() ->
  [begin
     New = spawn_life(Color),
     New#life{life = ?LIFE*4, siblings = make_ref(), parent1 = make_ref(), parent2 = make_ref()}
   end || Color <- [?BLACK, ?WHITE, ?RED, ?ORANGE, ?YELLOW, ?GREEN, ?BLUE, ?MAGENTA]].

on_draw(World, Canvas) ->
  draw_internal(World, Canvas).

on_tick(World) ->
  tick_internal(World).

%% ====================================================================
%% Internal functions
%% ====================================================================

spawn_life(Color) ->
  #life{pos = ?Posn(random(?WIDTH), random(?HEIGHT)),
        dir = ?Posn(random(20) - 10, random(20) - 10),
        color = Color}.

spawn_children(#life{parent1 = Parent11, parent2 = Parent12, siblings = S1},
               #life{parent1 = Parent21, parent2 = Parent22, siblings = S2}) 
  when Parent11 == Parent21 orelse Parent11 == Parent22 orelse
         Parent12 == Parent21 orelse Parent12 == Parent22 orelse S1 == S2->
  [];
spawn_children(#life{color = ?COLOR(R1,G1,B1), me = Parent1, pos = Pos},
               #life{color = ?COLOR(R2,G2,B2), me = Parent2}) ->
  N = case random(67) of
        1 -> 2; %% twins!
        _ -> 1
      end,
  Color = ?COLOR((R1+R2) div 2,
                 (G1+G2) div 2,
                 (B1+B2) div 2),
  Sibling = make_ref(),
  [begin
     New = spawn_life(Color),
     New#life{pos = Pos,
              siblings = Sibling,
              parent1 = Parent1,
              parent2 = Parent2}
   end || _ <- lists:seq(1, N)].

clear_dead(List) ->
  [X || #life{life = L} = X <- List, L > 0].

move(List) ->
  Fun = fun(#life{pos = ?Posn(X,Y) = Pos,
                  dir = ?Posn(Xd, Yd) = Dir} = Life) ->
                if
                  (X < 0) or (X > ?WIDTH) ->  Life#life{pos = ?Translate(Pos, -2*Xd, Yd), dir = ?Posn(-Xd, Yd)};
                  (Y < 0) or (Y > ?HEIGHT) -> Life#life{pos = ?Translate(Pos, Xd, -2*Yd), dir = ?Posn(Xd, -Yd)};
                  true ->                     Life#life{pos = ?Translate(Pos, Dir)}
                end end,
  [Fun(X#life{life = X#life.life - 1, cooldown = min(?COOLDOWN, X#life.cooldown+1)}) || X <- List].

random(N) ->
  random:uniform(N).

%Returns true if two bombs are within range of each other.
is_near(#life{pos = Pos1}, #life{pos = Pos2}) ->
  ?Distance(Pos1, Pos2) =< 20.

check_near(List) -> check_near([], List).
check_near(Acc, []) -> Acc;
check_near(Acc, [#life{cooldown = ?COOLDOWN} = First | Rest]) ->
  case first(First, Rest, []) of
    undefined      -> check_near([First | Acc], Rest);
    {Near, Others} -> check_near([First#life{cooldown = 0},
                                  Near#life{cooldown = 0} | spawn_children(First, Near)] ++ Acc, Others)
  end;
check_near(Acc, [First | Rest]) ->
  check_near([First | Acc], Rest).

first(_Focus, [], _Acc) -> undefined;
first(Focus, [First | Rest], Acc) ->
  case is_near(Focus, First) of
    true  -> {First, Acc ++ Rest};
    false -> first(Focus, Rest, [First | Acc])
  end.

%Internal Draw event handler.
draw_internal(List, Canvas) ->
  C0 = ?DrawRectangle(Canvas, ?Posn(0,0), ?WIDTH, ?HEIGHT, ?WHITE),
  draw_list(C0, List).

draw_list(Canvas, List) ->
  lists:foldr(fun draw_one/2, Canvas, List).
%Draws a bomb.
draw_one(#life{pos = Pos, color = Color}, Canvas) ->
  ?DrawCircle(Canvas, Pos, ?RADIUS, Color).

%Internal Tick event handler.
tick_internal(List) ->
  List == [] andalso interactive_erlang:quit("Done!"),
  length(List) >= max(?WIDTH, ?HEIGHT) andalso interactive_erlang:quit("Too many!"),
  Alive = clear_dead(List),
  Moved = move(Alive),
  check_near(Moved).
