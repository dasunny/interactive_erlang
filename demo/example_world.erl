%% Execute 'erl -noshell -run example_world -run erlang halt' in your terminal to run this program

%% -noshell           -> Tells the terminal not to open a shell, but instead to just run the program.
%% -run example_world -> Tells the terminal to immediately run the function 'example_world:start()' when it starts.
%%                       Even though it's not explicitly stated to use the 'start/0' function, start/0 is the 
%%                        default understood starting function in Erlang (like main(String[] args) in Java)
%% -run erlang halt   -> Tells the terminal to run the function erlang:halt() after example_world:start() has finished.
%%                       erlang:halt() stops the erlang virtual machine process

-module(example_world).

-include("idraw.hrl").  %% Imports all of the drawing macros
-include("iworld.hrl"). %% Imports all of the world macros

%% ====================================================================
%% API functions
%% ====================================================================
-export([start/0]).                         %% Start function
-export([on_tick/1, on_draw/2, on_key/3]).  %% World callback functions

-record(world, {direction, %% Keep track of what direction you're moving
                position,  %% Keep track of your current position
                color}).   %% Keep track of what color you are

%% This is how fast the circle will travel.
-define(MOVESPEED, 5).

%% All the colors that the circle can be
-define(COLORS, {?WHITE, ?RED, ?BLUE, ?GREEN, ?CYAN, ?MAGENTA, ?YELLOW, ?BLACK}).

%% A quick macro function to get a color, based on a number.
-define(GETCOLOR(N), element(N, ?COLORS)).

%% This world will draw a colorful circle that moves across the screen. It's direction can be changed with the arrow keys
%% Pressing numbers 1-8 will change the circle's color.
start() ->
  %% Starts the world on the provided scene and with world_0 as the initial state.
  ?IWorld(world_0(), [{title, "Title"},
                      {size, {500,500}},
                      {clock_rate, 15},              %% Create 'tick' events 15 times/second
                      {tick, fun ?MODULE:on_tick/1}, %% Call this function every time there is a 'tick' event
                      {key,  fun ?MODULE:on_key/3},  %% Call this function for every 'key' event
                      {draw, fun ?MODULE:on_draw/2}, %% Call this function for every 'draw' event
                      debug]).                       %% Useful for seeing which options have been paired with which functions

%% The starting world
world_0() ->
  #world{direction = right,
         position = ?Posn(50, 250), %% A Posn (short for position) contains only an X and a Y component. 
                                    %% Every drawing macro uses a Posn as it's position (Topleft corner for rectangles, center for circles) 
         color = ?BLACK}.

%% This function will call with 2 arguments, World_n and Window. You can ignore the window.
%% This world will update every tick by moving the circle in the direction it's travelling, but not off of the screen.
%% This function must return an updated world
on_tick(World) ->
  %% Get the X and Y component of the circle through pattern matching
  ?Posn(X, Y) = World#world.position,
  %% Pattern match Dx and Dy to the changes in the X and Y coordinates
  {Dx, Dy} = case World#world.direction of
               up    -> {0, -?MOVESPEED};
               down  -> {0,  ?MOVESPEED};
               left  -> {-?MOVESPEED,  0};
               right -> {?MOVESPEED,  0}
             end,
  %% Calculate the new position. The current X or Y coordinate, plus the Dx or Dy, then make sure it's 
  %% greater than 0, but less than 500 (to keep it from leaving the screen).
  NewPosition = ?Posn(max(0, min(500, X + Dx)),
                      max(0, min(500, Y + Dy))),
  World#world{position = NewPosition}.

%% This function draws the circle, in it's selected color, on a white background (a rectangle)
%% The function calls with 2 arguments, The current world, and a canvas on which to draw objects.
%% This function must return a Canvas
on_draw(World, Canvas0) ->
  %% ?DrawRectangle's arguments are:
  %%  A Canvas (canvas_0), The rectangle's top left corner's position, width, height, and it's color.
  %% It returns canvas_1, ie, the canvas with the rectangle drawn on it
  Canvas1 = ?DrawRectangle(Canvas0, ?Posn(0, 0), 500, 500, ?WHITE),
  
  %% ?DrawCircle's arguments are:
  %%  A Canvas (canvas_0), the circle's center position, it's radius, and it's color
  %% It returns a canvas (canvas_1) with a circle drawn on it
  Canvas2 = ?DrawCircle(Canvas1, World#world.position, 10, World#world.color),
  %% Return Canvas2
  Canvas2.

%% This function is called when you press a key on your keyboard.
%% KeyName is an atom of the key pressed ('a', 'b', 'up', 'down', '1', '2', and so on)
%% KeyType is either key_down (when you press a key) or key_up (when you release it)
%% This function must return an updated world
on_key(World, KeyName, key_down = _KeyType) ->
  on_key_internal(World, KeyName);
%% We only care about key_down events, so we'll ignore key_up events and return the same world we were given
on_key(World, _KeyName, _key_up) ->
  World.
  
%% ====================================================================
%% Internal functions
%% ====================================================================

%% Use pattern matching on the key to change the color of the circle, and return the updated world record
on_key_internal(World, "1") ->
  World#world{color = ?GETCOLOR(1)};
on_key_internal(World, "2") ->
  World#world{color = ?GETCOLOR(2)};
on_key_internal(World, "3") ->
  World#world{color = ?GETCOLOR(3)};
on_key_internal(World, "4") ->
  World#world{color = ?GETCOLOR(4)};
on_key_internal(World, "5") ->
  World#world{color = ?GETCOLOR(5)};
on_key_internal(World, "6") ->
  World#world{color = ?GETCOLOR(6)};
on_key_internal(World, "7") ->
  World#world{color = ?GETCOLOR(7)};
on_key_internal(World, "8") ->
  World#world{color = ?GETCOLOR(8)};

%% Pressing the up/down/left/right keys will change the direction direction stored in the world record
on_key_internal(World, up) ->
  World#world{direction = up};
on_key_internal(World, down) ->
  World#world{direction = down};
on_key_internal(World, left) ->
  World#world{direction = left};
on_key_internal(World, right) ->
  World#world{direction = right};
%% In case we get some other key we don't want, we'll return the same world we were given
on_key_internal(World, _Other) ->
  World.
