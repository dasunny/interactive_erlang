-module(unpacker).

-export([start/0]).

start() ->
  Name = fun(Name) -> list_to_atom(filename:basename(Name, ".beam")) end,
  Fun = fun(N, _I, B, ok) -> case code:load_binary(Name(N), N, B()) of
  	                           {module, Module} -> io:format("successfully loaded ~p\n", [Module]), ok;
  	                           {error, What}    -> io:format("Failed to load ~p. Error: ~p\n", [N, What]), error
  	                         end;
  	       (N, _I, _B, error) -> io:format("Skipping ~p\n", [N]) end,
  case zip:foldl(Fun, ok, "zip_test.zip") of
    {ok, ok} -> zip_test:test();
    Other -> io:format("~p\n", [Other])
  end.