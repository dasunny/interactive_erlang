-define(IE_DEVELOPER, true).

-define(PARENT_PROCESS, interactive_erlang).

-define(IWORLD,    iworld).
-define(IUNIVERSE, iuniverse).

-define(INIT_FIRST, ie_init_first).
-define(INIT_LAST,  ie_init_last).

-define(EXEC_FIRST, ie_exec_first).
-define(EXEC_LAST,  ie_exec_last).

-define(MISSING_CALLBACK, missing_required_callback).

-define(Send, fun ie_utils_common:safe_send/2).
-define(SendCallbackEvent(CallbackEvent), ?Send(?PARENT_PROCESS, {?PARENT_PROCESS, CallbackEvent})).
-define(SendClose(Reason), ?Send(?PARENT_PROCESS, {?PARENT_PROCESS, {close, Reason}})).
-define(IsParentAlive, ie_utils_common:is_parent_alive()).

-define(OptionExists,    fun proplists:is_defined/2).
-define(OptionGetValue,  fun proplists:get_value/2).
-define(OptionGetValue3, fun proplists:get_value/3).

-define(Unregister(Name), catch unregister(Name)).

-define(Dispatch(Dispatches), ie_utils_common:dispatch_callback_results(Dispatches)).
-define(IE_DISPATCH, ie_dispatch).

-record(iState, {state              :: term(),
                 options            :: list({atom(), any()} | atom()),
                 ie_components = [] :: list(module()),
                 exec_first         :: list(module()),
                 exec_last          :: list(module()),
                 type               :: 'iworld' | 'iuniverse',
                 extra_data = #{}   :: map(),
                 stay_alive = true  :: boolean()}).

-type init_info() :: 'undefined' | [IEComponent :: module()] | ?INIT_FIRST | ?INIT_LAST.
-type exec_info() :: 'undefined' | [ExecutionTime :: ?EXEC_FIRST | ?EXEC_LAST].

-record(type_info, {module,
                    init_info = []        :: init_info(),
                    exec_info = undefined :: exec_info()}).

-record(component_info,
        {iworld    = #type_info{} :: #type_info{},
         iuniverse = #type_info{} :: #type_info{}}).

-record(callback_event, {component = ?MODULE :: module(),
                         callback :: function(),
                         event_object :: any()}).
