-ifdef(IE_DEVELOPER).

-record(canvas, {name,
                 pid,
                 width,
                 height,
                 bitmap,
                 memoryDC1,
                 memoryDC2,
                 clientDC,
                 panel,
                 simple_canvas}).

-include_lib("wx/include/wx.hrl").

-endif.

-record(color, {r, g, b}).

-record(saved_image, {path, width, height, data}).

-define(COLOR(R, G, B), #color{r = R, g = G, b = B}).

-define(BLACK,     #color{r = 0,   g = 0,   b = 0}).
-define(BLUE,      #color{r = 0,   g = 0,   b = 255}).
-define(GREEN,     #color{r = 0,   g = 255, b = 0}).
-define(CYAN,      #color{r = 0,   g = 255, b = 255}).
-define(RED,       #color{r = 255, g = 0,   b = 0}).
-define(MAGENTA,   #color{r = 255, g = 0,   b = 255}).
-define(YELLOW,    #color{r = 255, g = 255, b = 0}).
-define(WHITE,     #color{r = 255, g = 255, b = 255}).

-define(DARKRED,   #color{r = 128, g = 0,   b = 0}).
-define(DARKGREEN, #color{r = 0,   g = 128, b = 0}).
-define(DARKBLUE,  #color{r = 0,   g = 0,   b = 128}).

-define(ORANGE,    #color{r = 255, g = 165, b = 0}).
-define(PURPLE,    #color{r = 155, g = 40,  b = 255}).
-define(PINK,      #color{r = 255, g = 192, b = 203}).

-define(LIGHTGRAY, #color{r = 192, g = 192, b = 192}).
-define(GRAY,      #color{r = 128, g = 128, b = 128}).
-define(DARKGRAY,  #color{r = 64,  g = 64,  b = 64}).
%-define(, #color{r = , g = , b = }).

-define(Inverse(Color), ?COLOR(255-Color#color.r, 255-Color#color.g, 255-Color#color.b)).

-record(composite, {fill = ?WHITE, line = ?BLACK}).
-define(Composite(Fill, Line), #composite{fill = Fill, line = Line}).
-define(Composite(Fill), ?Composite(Fill, ?BLACK)).

-record(line,             {point1,   point2, color,  thickness}).
-record(circle,           {center,   radius, color}).
-record(rectangle,        {top_left, width,  height, color}).
-record(text,             {top_left, text,   color}).
-record(image,            {top_left, path,   options}).
-record(composite_canvas, {top_left, canvas, options}).

-define(DEFAULT_CANVAS, 'Canvas').

-record(simple_canvas, {name,
                        width,
                        height}).

-define(Canvas,             [ie_comp_draw:get_simple_canvas(?DEFAULT_CANVAS)]).
-define(Canvas(CanvasName), [ie_comp_draw:get_simple_canvas(CanvasName)]).
-define(CompositeCanvas(Width, Height), [{composite_size, Width, Height}]).
-define(DrawPerm(Draws),         ie_comp_draw:draw_permanent(Draws)).
-define(ResetCanvas(),           ie_comp_draw:reset_canvas('Canvas')).
-define(ResetCanvas(CanvasName), ie_comp_draw:reset_canvas(CanvasName)).

-define(DrawLine(Canvas, Point1, Point2, Color, Thickness), [#line{point1 = Point1, point2 = Point2, color = Color, thickness = Thickness} | Canvas]).
-define(DrawCircle(Canvas, Point, Radius, Color),           [#circle{center = Point, radius = Radius, color = Color} | Canvas]).
-define(DrawRectangle(Canvas, Point, Width, Height, Color), [#rectangle{top_left = Point, width = Width, height = Height, color = Color} | Canvas]).
-define(DrawText(Canvas, Point, Text, Color),               [#text{top_left = Point, text = Text, color = Color} | Canvas]).
-define(DrawImage(Canvas, Point, Path, Options),            [#image{top_left = Point, path = Path, options = Options} | Canvas]).
-define(DrawComposite(Canvas, Point, CompositeCanvas, Options), [#composite_canvas{top_left = Point, canvas = CompositeCanvas, options = Options} | Canvas]).

-define(DrawLine(Canvas, Point1, Point2, Color),       ?DrawLine(Canvas, Point1, Point2, Color, 1)).
-define(DrawText(Canvas, Point, Text),                 ?DrawText(Canvas, Point, Text, ?BLACK)).
-define(DrawImage(Canvas, Point, Path),                ?DrawImage(Canvas, Point, Path, [])).
-define(DrawComposite(Canvas, Point, CompositeCanvas), ?DrawComposite(Canvas, Point, CompositeCanvas, [])).


-define(PreLoadImage(Path), ie_utils_draw:preload_image(Path)).

-define(GetSavedImage(Name, Image), ie_utils_draw:make_saved_image(Name, Image)).
-define(GetSavedImage(Path),        ie_utils_draw:make_saved_image(Path)).

-define(CanvasWidth(Canvas), (lists:last(Canvas))#simple_canvas.width).
-define(CanvasHeight(Canvas), (lists:last(Canvas))#simple_canvas.height).

-define(ImageWidth(Path),          ie_utils_draw:image_width(Path)).
-define(ImageHeight(Path),         ie_utils_draw:image_height(Path)).
-define(ImageWidth(Canvas, Text),  ie_utils_draw:text_width(Canvas, Text)).
-define(ImageHeight(Canvas, Text), ie_utils_draw:text_height(Canvas, Text)).

-define(TextWidth(Text),  ie_utils_draw:text_width(Text)).
-define(TextHeight(Text), ie_utils_draw:text_height(Text)).

-define(CurrentDirectory, ie_utils_draw:current_directory()).

-record(posn, {x, y}).

-define(Posn(X, Y), #posn{x = X, y = Y}).
-define(Translate(Posn1, Posn2), Posn1#posn{x = Posn1#posn.x + Posn2#posn.x, y = Posn1#posn.y + Posn2#posn.y}).
-define(Translate(Posn, X, Y), Posn#posn{x = Posn#posn.x + X, y = Posn#posn.y + Y}).
-define(Difference(Posn1, Posn2), #posn{x = Posn1#posn.x - Posn2#posn.x, y = Posn1#posn.y - Posn2#posn.y}).
-define(Distance(Posn1, Posn2),  math:sqrt((Posn1#posn.x-Posn2#posn.x)*(Posn1#posn.x-Posn2#posn.x) +
                                             (Posn1#posn.y-Posn2#posn.y)*(Posn1#posn.y-Posn2#posn.y))).
