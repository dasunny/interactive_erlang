-record(http_client, {pid,
                      address}).

-define(HTML_TYPE, <<"text/html">>).
-define(TEXT_TYPE, <<"text">>).

-define(HTTPResponse(Message, Type, Redirect), {http_response, Message, Type, Redirect}).
-define(HTTPResponse(Message, Redirect), ?HTTPResponse(Message, ?TEXT_TYPE, Redirect)).

-define(MakeHTTPResponse(State, Message, Type), {http_response, State, Message, Type}).
-define(MakeHTTPResponse(State, Message), {http_response, State, Message, ?TEXT_TYPE}).
