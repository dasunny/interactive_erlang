-define(AddStatusBar(Text),    ie_utils_menu:add_status_bar(Text)).
-define(ChangeStatusBar(Text), ie_utils_menu:change_status_bar(Text)).
-define(RevertStatusBar,       ie_utils_menu:revert_status_bar()).

-define(SpawnDialogBox(Text),  ie_utils_menu:spawn_dialog_box(Text)).

-define(NoID, no_id).

-define(MenuBar(Menus), {menu_bar, Menus}).
-define(Menu(Title, Items), {menu, Title, Items}).

-define(MenuSubMenuItem(Title, ID, Items), {sub_menu, Title, ID, Items}).
-define(MenuButtonItem(Title, ID), {menu_button, Title, ID}).
-define(MenuRadioItem(Title, ID), {menu_radio, Title, ID}).
-define(MenuCheckItem(Title, ID), {menu_check, Title, ID}).
-define(MenuSeparatorItem, menu_separator).
