-define(IWorld, fun interactive_erlang:iworld/2).

-define(MakePackage(State, Message), ie_utils_net:make_package(State, Message)).

-define(ServerChooser, ie_utils_net:server_chooser()).
