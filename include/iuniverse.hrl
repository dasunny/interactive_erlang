-record(iworld, {socket,
                 name}).

-record(mail, {to,
               message}).

-define(IUniverse, fun interactive_erlang:iuniverse/2).

-define(MakeBundle(State, Mails, ToDisconnect), ie_utils_net:make_bundle(State, Mails, ToDisconnect)).
-define(MakeBundle(State, Mails),               ie_utils_net:make_bundle(State, Mails, [])).
-define(MakeBundle(State),                      ie_utils_net:make_bundle(State, [],    [])).

-define(MakeMail(To, Message), #mail{to = To, message = Message}).
-define(MakeMails(To, Messages), [?MakeMail(To, Message) || Message <- Messages]).

-define(IWorldsEqual(IWorld1, IWorld2), IWorld1#iworld.socket == IWorld2#iworld.socket).
