-module(ie_component).

-include("interactive_erlang.hrl").

-type description() :: 'undefined' |
                       {Key :: string()} |
                       {Key :: string(), Callback :: fun()} |
                       {Key :: string(), Format :: string(), Args :: list()}.

-callback component_info() -> #component_info{}.

-callback describe(Options :: list()) -> description() | [description()].

-callback init(IState :: #iState{}) -> 'ok' |
                                       {'ok', UpdatedIState :: #iState{}} |
                                       'undefined' |
                                       {'error', {Component :: module(), Reason :: term()}}.

-callback stop(IState :: #iState{}) -> UpdatedIState :: #iState{}.

-callback event(IState :: #iState{}, CallbackEvent :: #callback_event{}) -> UpdatedIState :: #iState{}.

-export([find_components/0]).

find_components() ->
  AllComponents =
    lists:map(fun(Path) -> Matches = filelib:wildcard("ie_comp_*.beam", Path),
                      [list_to_atom(filename:rootname(File)) || File <- Matches] end,
              code:get_path()),
  Flattened = lists:flatten(AllComponents),
  lists:filter(fun is_ie_component/1, Flattened).

is_ie_component(Module) ->
  Attrs = Module:module_info(attributes),
  case lists:keyfind(behaviour, 1, Attrs) of
    false                   -> false;
    {behaviour, Behaviours} -> lists:any(fun(ie_component) -> true;
                                            (_)            -> false end, Behaviours)
  end.
