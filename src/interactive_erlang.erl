-module(interactive_erlang).

-include("interactive_erlang.hrl").

%% ====================================================================
%% API functions
%% ====================================================================
-export([iworld/2, iuniverse/2, quit/1]).

iworld(InitialState, Options) ->
  init(InitialState, Options, ?IWORLD).

iuniverse(InitialState, Options) ->
  init(InitialState, Options, ?IUNIVERSE).

quit(World) ->
  ie_utils_common:flush(),
  ?SendClose("interactive_erlang:quit/1 called"),
  World.

%% ====================================================================
%% Internal functions
%% ====================================================================

%Initializes all of the callback components.
init(InitialState, Options, Type) ->
  register(?MODULE, self()),
  UninitializedIState = #iState{state   = InitialState,
                                options = Options,
                                type    = Type},
  Components = my_components(Type),
  Components == [] andalso throw({error, no_components_found}),
  OrderedDeps = init_order(Components),
  Fun = fun(Elem, #iState{ie_components = InitializedComponents} = AccIState) ->
                Module = Elem#type_info.module,
                case Module:init(AccIState) of
                  ok                  -> AccIState#iState{ie_components = [Elem | InitializedComponents]};
                  {ok, UpdatedIState} -> UpdatedIState#iState{ie_components = [Elem | InitializedComponents]};
                  undefined           -> AccIState;
                  {error, Error}      -> io:format("Initialization of component ~p failed. Error: ~p\n", [Elem, Error]),
                                         throw({error, Error})
                end end,
  InitializedIState = lists:foldl(Fun, UninitializedIState, OrderedDeps), %% ie_components is reverse of OrderedDeps
  {ExecFirsts, ExecLasts} = lists:foldl(fun execution_order/2, {[],[]}, InitializedIState#iState.ie_components),
  loop(InitializedIState#iState{exec_first = lists:sort(ExecFirsts),
                                exec_last  = lists:sort(ExecLasts)}).

loop(#iState{exec_first = ExecFirsts,
             exec_last  = ExecLasts,
             stay_alive    = true} = IState) ->
  CallbackEvent = receive {?PARENT_PROCESS, Message} -> Message
                  after 1000 -> undefined end,

  Fun = fun(Elem, Acc) -> Elem:event(Acc, CallbackEvent) end,
  IState2 = lists:foldl(Fun, IState, ExecFirsts),

  IState3 = case CallbackEvent of
              #callback_event{component = Component} -> Component:event(IState2, CallbackEvent);
              {close, {error, Reason}}               -> io:format("INTERNAL ERROR: " ++ Reason ++ "\n", []),
                                                        IState2#iState{stay_alive = false};
              {close, Reason}                        -> io:format(Reason ++ "\n", []),
                                                        IState2#iState{stay_alive = false};
              undefined                              -> IState2
            end,

  IState4 = lists:foldl(Fun, IState3, ExecLasts),

  loop(IState4);

loop(#iState{state = State,
             stay_alive = false,
             ie_components = Components} = IState) ->
  lists:foldl(fun(#type_info{module = Module}, AccIState) -> Module:stop(AccIState) end, IState, Components),
  unregister(?MODULE),
  State.

%% ====================================================================
%% Init functions
%% ====================================================================

%% Init order:
%% ?INIT_FIRSTs
%% Components without dependencies
%% Components with dependencies
%% ?INIT_LASTs
%% Accepts list of #type_info records
init_order(ComponentInfos) ->
  ensure_dependencies(ComponentInfos),
  Firsts = lists:sort([Info || #type_info{init_info = ?INIT_FIRST} = Info <- ComponentInfos]),
  NoDeps = lists:sort([Info || #type_info{init_info = []}   = Info <- ComponentInfos]),
  Depped = [Info            || #type_info{init_info = Init} = Info <- ComponentInfos, is_list(Init), length(Init) > 0],
  Lasts  = lists:sort([Info || #type_info{init_info = ?INIT_LAST}  = Info <- ComponentInfos]),
  OrderedDepped = order_deps(get_modules(Firsts++NoDeps), Depped, []),
  Firsts ++ NoDeps ++ OrderedDepped ++ Lasts.

my_components(Mode) ->
  All = [my_mode(Mode, One) || One <- ie_component:find_components()],
  [Comp || #type_info{init_info = Init} = Comp <- All, Init =/= undefined].

my_mode(Mode, Module) ->
  #component_info{iworld    = IWorld,
                  iuniverse = IUniverse} = Module:component_info(),
  case Mode of
    ?IWORLD    -> IWorld#type_info{module = Module};
    ?IUNIVERSE -> IUniverse#type_info{module = Module}
  end.

order_deps(_Firsts, [], Acc) -> lists:reverse(Acc);
order_deps(Firsts, [#type_info{module = Module, init_info = Deps} = First | Rest], Acc) ->
  case lists:all(fun(Dep) -> lists:member(Dep, Firsts) end, Deps) of
    true  -> order_deps([Module | Firsts], Rest, [First | Acc]);
    false -> order_deps(Firsts, Rest ++ [First], Acc)
  end.

ensure_dependencies(ComponentInfos) ->
  Deps = [Dep || #type_info{init_info = Dep} <- ComponentInfos],
  Have = [Module || #type_info{module = Module} <- ComponentInfos],
  Need = [X || X <- Deps, is_list(X)],
  lists:all(fun(Elem) -> case lists:member(Elem, Have) of
                           true  -> true;
                           false -> throw({error, {missing_dependency, Elem}})
                         end
            end, lists:flatten(Need)).

get_modules(ComponentInfos) ->
  [Info#type_info.module || Info <- ComponentInfos].

execution_order(#type_info{module = Module, exec_info = ?EXEC_FIRST}, {AccF, AccL}) -> {[Module | AccF], AccL};
execution_order(#type_info{module = Module, exec_info = ?EXEC_LAST},  {AccF, AccL}) -> {AccF, [Module | AccL]};
execution_order(#type_info{}, Acc) -> Acc.
